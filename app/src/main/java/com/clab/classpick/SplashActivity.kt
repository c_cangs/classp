package com.clab.classpick

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.preference.PreferenceManager

import com.clab.classpick.NewUser.NewNoUserActivity
import com.clab.classpick.utils.Singletons.mFunction
import com.clab.classpick.utils.Singletons.Singleton
import com.google.gson.JsonElement
import com.google.gson.JsonObject

import java.io.IOException
import java.util.Calendar

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by kyun on 2017. 9. 6..
 */

class SplashActivity : Activity() {

    private var singleton: Singleton? = null
    private var mfunction: mFunction? = null
    private var preferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null

    private var ucode: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = PreferenceManager.getDefaultSharedPreferences(this)
        editor = preferences!!.edit()
        if (mfunction == null) mfunction = mFunction.getInstance(this)
        if (singleton == null) singleton = Singleton.getInstance(this)
        ucode = preferences!!.getInt(resources.getString(R.string.ucode), 0)
        init()
    }

    private fun init() {
        appVmsg()
    }

    private fun appVmsg() {
        singleton!!.getRetroService().appVmsg().enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    val `object` = response.body().asJsonObject
                    val and = `object`.getAsJsonObject("android")
                    val code = and.get("code").asInt
                    val type = and.get("type").asInt
                    val url = and.get("url").asString
                    val title = and.get("title").asString
                    val msg = and.get("msg").asString
                    val sday = preferences!!.getInt(resources.getString(R.string.sday), 0)
                    val c = Calendar.getInstance()
                    val day = c.get(Calendar.DAY_OF_WEEK)
                    val pain = packageManager.getPackageInfo(packageName, 0)
                    if (code > pain.versionCode) {
                        singleton!!.CustomDialog(this@SplashActivity).setTitle("업데이트").setMessage("업데이트가 필요합니다 스토어로 이동합니다.")
                                .setPositiveButton("이동", DialogInterface.OnClickListener { dialogInterface, i ->
                                    val it = Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName))
                                    startActivity(it)
                                }).setOnDismissListener(DialogInterface.OnDismissListener {
                            finish()
                            android.os.Process.killProcess(android.os.Process.myPid())
                        }).show()
                    } else {
                        if (type == 1) {
                            if (day != sday) {
                                if (url == "") {
                                    singleton!!.CustomDialog(this@SplashActivity).setTitle(title).setMessage(msg).setPositiveButton("확인", null).setOnDismissListener(DialogInterface.OnDismissListener {
                                        Pass()
                                        editor!!.putInt(resources.getString(R.string.sday), day)
                                        editor!!.commit()
                                    }).show()
                                } else {
                                    singleton!!.CustomDialog(this@SplashActivity).setTitle(title).setMessage(msg)
                                            .setPositiveButton("이동", DialogInterface.OnClickListener { dialogInterface, i -> startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url))) }).setOnDismissListener(DialogInterface.OnDismissListener {
                                        Pass()
                                        editor!!.putInt(resources.getString(R.string.sday), day)
                                        editor!!.commit()
                                    }).setNegativeButton("확인", null).show()
                                }
                            } else {
                                Pass()
                            }
                        } else if (type == 2) {
                            if (url == "") {
                                singleton!!.CustomDialog(this@SplashActivity).setTitle(title).setMessage(msg).setPositiveButton("확인", null).setOnDismissListener(DialogInterface.OnDismissListener {
                                    Pass()
                                    editor!!.putInt(resources.getString(R.string.sday), day)
                                    editor!!.commit()
                                }).show()
                            } else {
                                singleton!!.CustomDialog(this@SplashActivity).setTitle(title).setMessage(msg)
                                        .setPositiveButton("이동", DialogInterface.OnClickListener { dialogInterface, i -> startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url))) }).setOnDismissListener{ Pass() }.setNegativeButton("확인", null).show()
                            }
                        } else if (type == 3) {
                            singleton!!.CustomDialog(this@SplashActivity).setTitle(title).setMessage(msg)
                                    .setPositiveButton("확인", DialogInterface.OnClickListener { dialogInterface, i -> startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url))) }).setOnDismissListener{ android.os.Process.killProcess(android.os.Process.myPid()) }.show()
                        }
                    }

                } else {
                    singleton!!.Errdialog(this@SplashActivity, true, "000").show()
                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                singleton!!.Errdialog(this@SplashActivity, true, "001").show()
            }
        })
    }

    private fun getdb() {
        val Loading = singleton!!.CustomDialog(this).setTitle("설정중...").setMessage("학교 정보를 업데이트 하는 중입니다.\n최대 10초정도 걸릴수 있습니다 \n잠시만 기다려 주세요...")
                .setCancelable(false).show()
        singleton!!.getRetroService().getdb(ucode).enqueue(object : Callback<JsonElement> {
            override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                if (response.isSuccessful) {
                    object : AsyncTask<JsonObject, Void, Void>() {
                        override fun doInBackground(vararg JsonObjects: JsonObject): Void? {
                            editor!!.putInt(resources.getString(R.string.dbv), mfunction!!.insertDb(JsonObjects[0]))
                            editor!!.commit()
                            return null
                        }

                        override fun onPostExecute(aVoid: Void) {
                            super.onPostExecute(aVoid)
                            mfunction!!.ScheduleSet()
                            Loading.dismiss()
                            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                            finish()
                        }
                    }.execute(response.body().asJsonObject)
                } else {
                    singleton!!.Errdialog(this@SplashActivity, true, "000").show()
                }
            }

            override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                singleton!!.Errdialog(this@SplashActivity, true, "001").show()
            }
        })
    }

    private fun getdbV() {
        singleton!!.getRetroService().getdbV(ucode).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val dbv = preferences!!.getInt(resources.getString(R.string.dbv), 1)
                    var serv = 0
                    try {
                        serv = Integer.parseInt(response.body().string())
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } finally {
                        if (dbv < serv) {
                            mfunction!!.deleteDb()
                            getdb()
                        } else {
                            SplashFinish()
                        }
                    }
                } else {
                    singleton!!.Errdialog(this@SplashActivity, true, "000").show()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                singleton!!.Errdialog(this@SplashActivity, true, "001").show()
            }
        })
    }

    private fun Pass() {
        if (ucode == 0) {
            startActivity(Intent(this@SplashActivity, NewNoUserActivity::class.java))
            finish()
        } else if (ucode == -3) {
            SplashFinish()
        } else {
            getdbV()
        }

    }

    private fun SplashFinish() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()

    }
}
