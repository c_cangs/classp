package com.clab.classpick.NewUser;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clab.classpick.MainActivity;
import com.clab.classpick.R;
import com.clab.classpick.utils.Singletons.mFunction;
import com.clab.classpick.utils.Singletons.Singleton;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kyun on 2017. 9. 6..
 */

public class NewNoUserActivity extends Activity {

    private Singleton singleton;
    private mFunction mfunction;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private int selectUcode,selectP;
    private String[][] univCode;

    private InputMethodManager im;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfo);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();
        if(singleton == null) singleton = Singleton.getInstance(this);
        if(mfunction == null) mfunction = mFunction.getInstance(this);
        im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        View logo = findViewById(R.id.userinfo_logo);
        RelativeLayout.LayoutParams logoparam = (RelativeLayout.LayoutParams) logo.getLayoutParams();
        logoparam.height = singleton.ratiodp(false,0.061);
        logoparam.width = singleton.ratiodp(true,0.072);
        logoparam.topMargin = singleton.ratiodp(false,0.044);
        TextView info = findViewById(R.id.userinfo_top_info);
        info.setTypeface(singleton.getBmjua());
        ((RelativeLayout.LayoutParams)info.getLayoutParams()).topMargin = singleton.ratiodp(false,0.13);

        TextView school = findViewById(R.id.userinfo_school_txt);
        school.setTypeface(singleton.getBmjua());
        RelativeLayout.LayoutParams schoolparam = (RelativeLayout.LayoutParams) school.getLayoutParams();
        schoolparam.leftMargin = singleton.ratiodp(true,0.05);
        schoolparam.rightMargin = schoolparam.leftMargin;
        schoolparam.topMargin = singleton.ratiodp(false,0.039);

        final AutoCompleteTextView univ = findViewById(R.id.userinfo_univ);
        univ.getLayoutParams().height = singleton.ratiodp(false,0.056);
        univ.setPadding(singleton.ratiodp(true,0.024),0,0,0);
        univ.setTypeface(singleton.getNanumb());
        univ.setThreshold(0);
        univ.setImeOptions(EditorInfo.IME_ACTION_DONE);
        univ.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(i == EditorInfo.IME_ACTION_DONE) {
                    univ.dismissDropDown();
                    im.hideSoftInputFromWindow(univ.getWindowToken(),0);
                }
                return false;
            }
        });

        final Button access = findViewById(R.id.userinfo_access);
        access.getLayoutParams().height = singleton.ratiodp(false,0.061);

        findViewById(R.id.userinfo_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                im.hideSoftInputFromWindow(univ.getWindowToken(),0);
            }
        });

        singleton.getRetroService().getunivlist().enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if(response.isSuccessful()) {
                    JsonArray univlist = response.body().getAsJsonArray();
                    univCode = new String[2][univlist.size()+1];
                    univCode[0][0] = "-3";
                    univCode[1][0] = "선택 안함(목록에 없음)";
                    for(int i = 1; i < univlist.size()+1;i++) {
                        JsonObject object = univlist.get(i-1).getAsJsonObject();
                        univCode[0][i] = object.get("ucode").getAsString();
                        univCode[1][i] = object.get("name").getAsString();
                    }

                    selectUcode = -2;
                    selectP = -2;

                    univ.setAdapter(new ArrayAdapter<>(NewNoUserActivity.this,R.layout.custom_drop_down,univCode[1]));
                    univ.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            TextView tx = (TextView) view;
                            for(int j = 0; j < univCode[1].length ;j++) {
                                if(univCode[1][j].equals(tx.getText().toString())) {
                                    selectUcode = Integer.parseInt(univCode[0][j]);
                                    selectP = j;
                                    im.hideSoftInputFromWindow(univ.getWindowToken(),0);
                                }
                            }
                        }
                    });
                    univ.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            String s = charSequence.toString();
                            for(int j = 0 ; j < univCode[1].length ; j++) {
                                if(s.equals(univCode[1][j])) {
                                    selectUcode = Integer.parseInt(univCode[0][j]);
                                    selectP = j;
                                    break;
                                } else {
                                    selectUcode = -2;
                                    selectP = -2;
                                }
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });
                    univ.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            univ.showDropDown();
                        }
                    });

                    access.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (selectP != -2) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(NewNoUserActivity.this);
                                builder.setNegativeButton("아니요", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                }).setPositiveButton("예", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if(selectUcode == -1) {
                                            Toast.makeText(NewNoUserActivity.this, "준비중인 학교입니다 빠른 시간내에 추가하도록 하겠습니다!", Toast.LENGTH_SHORT).show();
                                        }  else if(selectUcode == -3) {
                                            editor.putInt(getResources().getString(R.string.ucode), selectUcode);
                                            editor.commit();
                                            startActivity(new Intent(NewNoUserActivity.this, MainActivity.class));
                                            finish();
                                        } else if (selectUcode != -2) {
                                            singleton.getRetroService().newclasspclient(1, 0, selectUcode).enqueue(new Callback<ResponseBody>() {
                                                @Override
                                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                    if (response.isSuccessful()) {
                                                        try {
                                                            if (response.body().string().equals("suc")) {
                                                                getdb(selectUcode);
                                                            }
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                    } else {
                                                        singleton.Errdialog(NewNoUserActivity.this,true,"000").show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                    singleton.Errdialog(NewNoUserActivity.this,true,"001").show();
                                                }
                                            });
                                        } else {
                                            Toast.makeText(NewNoUserActivity.this, "학교가 제대로 선택되지 않았습니다 다시 학교를 지정해 주세요", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }).setTitle("등록").setMessage(univCode[1][selectP] + " 로 진행 하시겠습니까?").show();
                            } else {
                                Toast.makeText(NewNoUserActivity.this, "학교가 제대로 선택되지 않았습니다 다시 학교를 지정해 주세요", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    singleton.Errdialog(NewNoUserActivity.this,true,"000").show();
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                singleton.Errdialog(NewNoUserActivity.this,true,"001").show();
            }
        });
    }

    private void getdb(int ucode){
        final android.app.AlertDialog Loading = singleton.CustomDialog(this).setTitle("설정중...").setMessage("학교 정보를 설정하는 중입니다.\n최대 10초정도 걸릴수 있습니다 \n잠시만 기다려 주세요...").setCancelable(false).show();
        singleton.getRetroService().getdb(ucode).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if(response.isSuccessful()) {
                    new AsyncTask<JsonObject,Void,Void>() {
                        @Override
                        protected Void doInBackground(JsonObject... JsonObjects) {
                            editor.putInt(getResources().getString(R.string.dbv),mfunction.insertDb(JsonObjects[0]));
                            editor.commit();

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            mfunction.ScheduleSet();
                            editor.putInt(getResources().getString(R.string.ucode), selectUcode);
                            editor.commit();
                            Loading.dismiss();
                            startActivity(new Intent(NewNoUserActivity.this, MainActivity.class));
                            finish();
                        }
                    }.execute(response.body().getAsJsonObject());

                } else {
                    Loading.dismiss();
                    singleton.Errdialog(NewNoUserActivity.this,true,"000").show();
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Loading.dismiss();
                singleton.Errdialog(NewNoUserActivity.this,true,"001").show();
            }
        });
    }
}
