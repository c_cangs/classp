package com.clab.classpick.utils.Observer.AppFrontEnd;

/**
 * Created by kyun on 2017-04-03.
 */

public interface OnAppFrontendObserv {
    void OnAppFrontend();
}
