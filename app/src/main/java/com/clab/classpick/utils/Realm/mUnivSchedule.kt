package com.clab.classpick.utils.Realm

import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by kyun on 2017. 9. 12..
 */
@RealmClass
open class mUnivSchedule : RealmModel {

    var period: Int = 0
        internal set
    var stime: String? = null
        internal set
    var etime: String? = null
        internal set
    var title: String? = null
        internal set


    operator fun set(period: Int, stime: String, etime: String, title: String) {
        this.period = period
        this.stime = stime
        this.etime = etime
        this.title = title
    }

}
