package com.clab.classpick.utils.Singletons;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.clab.classpick.BuildingInfo.BuildinginfoItem;
import com.clab.classpick.Favorite.Building.FavoriteBuildingItem;
import com.clab.classpick.Favorite.Room.FavoriteRoomItem;
import com.clab.classpick.R;
import com.clab.classpick.Schedule.ClassListItem;
import com.clab.classpick.Schedule.TimeTableItem;
import com.clab.classpick.Search.SearchRecycleritem;
import com.clab.classpick.Widget.Widget44Item;
import com.clab.classpick.utils.Observer.Favorite.FavoritePublisher;
import com.clab.classpick.utils.Realm.mBfavorite;
import com.clab.classpick.utils.Realm.mBuilding;
import com.clab.classpick.utils.Realm.mClass;
import com.clab.classpick.utils.Realm.mClassroom;
import com.clab.classpick.utils.Realm.mClasstable;
import com.clab.classpick.utils.Realm.mCustomtable;
import com.clab.classpick.utils.Realm.mMigration;
import com.clab.classpick.utils.Realm.mMyclass;
import com.clab.classpick.utils.Realm.mRfavorite;
import com.clab.classpick.utils.Realm.mUnivSchedule;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by kyun on 2017. 9. 12..
 */

public class mFunction {

    private volatile static mFunction instance;

    public static mFunction getInstance(Context context) {
        if (instance == null) {
            synchronized (mFunction.class) {
                if (instance == null) {
                    instance = new mFunction(context);
                }
            }
        }
        return instance;
    }

    private static Singleton singleton;
    private static String[][] univscheduleS;
    private static String[] univscheduleT;
    private static float[][] univscheduleF;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;

    private static List<ClassListItem> allitem;

    public String[][] getUnivscheduleS() {
        return univscheduleS;
    }

    public String[] getUnivscheduleT() {
        return univscheduleT;
    }

    public float[][] getUnivscheduleF() {
        return univscheduleF;
    }

    public mFunction(Context context) {
        singleton = Singleton.getInstance(context);
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
        try {
            Realm.migrateRealm(singleton.getmConfig(),new mMigration());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ScheduleSet();
    }

    public void ScheduleSet() {
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mUnivSchedule> schedules = realm.where(mUnivSchedule.class).findAll();
        univscheduleS = new String[schedules.size()][2];
        univscheduleF = new float[schedules.size()][2];
        univscheduleT = new String[schedules.size()];
        for (mUnivSchedule m : schedules) {
            int p = m.getPeriod();
            String st = m.getStime();
            String et = m.getEtime();
            univscheduleS[p] = new String[]{st,et};
            univscheduleF[p] = new float[]{singleton.timeTofloat(st),singleton.timeTofloat(et)};
            univscheduleT[p] = m.getTitle();
        }
        getAllItems();
    }

    public void deleteDb() {
        Realm realm = Realm.getInstance(singleton.getmConfig());
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(mClass.class).findAll().deleteAllFromRealm();
                realm.where(mClasstable.class).findAll().deleteAllFromRealm();
                realm.where(mUnivSchedule.class).findAll().deleteAllFromRealm();
                realm.where(mBuilding.class).findAll().deleteAllFromRealm();
                realm.where(mClassroom.class).findAll().deleteAllFromRealm();
            }
        });
    }

    public int insertDb(JsonObject object) {
        JsonObject all = object;
        Gson gson = new Gson();
        JsonArray ver = all.getAsJsonArray("ver");
        JsonArray classes = all.getAsJsonArray("class");
        JsonArray classtable = all.getAsJsonArray("classtable");
        JsonArray schedule = all.getAsJsonArray("schedule");
        JsonArray building = all.getAsJsonArray("building");
        JsonArray classroom = all.getAsJsonArray("classroom");

        int v = ver.get(0).getAsJsonObject().get("ver").getAsInt();

        Type stringStringMap = new TypeToken<Map<String, String>>(){}.getType();
        final List<mClass> mClasses = new ArrayList<>();
        final List<mClasstable> mClasstables = new ArrayList<>();
        final List<mUnivSchedule> mUnivSchedules = new ArrayList<>();
        final List<mBuilding> mBuildings = new ArrayList<>();
        final List<mClassroom> mClassrooms = new ArrayList<>();

        Realm realm = Realm.getInstance(singleton.getmConfig());
        for (int i = 0; i < classes.size(); i++) {
            Map<String,String> map = gson.fromJson(classes.get(i),stringStringMap);
            mClass mClassModel = new mClass();
            mClassModel.set(map.get("cid"),Integer.parseInt(map.get("point")),map.get("type"),map.get("department"),map.get("cname"),map.get("prof"),Integer.parseInt(map.get("grade")));
            mClasses.add(mClassModel);
        }
        for (int i = 0; i < classtable.size(); i++) {
            Map<String,String> map = gson.fromJson(classtable.get(i),stringStringMap);
            mClasstable mClasstableModel = new mClasstable();
            mClasstableModel.set(map.get("bname"),map.get("rid"),map.get("cid"),Integer.parseInt(map.get("cday")),Integer.parseInt(map.get("speriod")), Integer.parseInt(map.get("eperiod")));
            mClasstables.add(mClasstableModel);
        }
        for (int i = 0; i < schedule.size(); i++) {
            Map<String,String> map = gson.fromJson(schedule.get(i),stringStringMap);

            mUnivSchedule mU = new mUnivSchedule();
            mU.set(Integer.parseInt(map.get("period")),map.get("stime"),map.get("etime"),map.get("title"));
            mUnivSchedules.add(mU);
        }
        for(int i = 0 ; i < building.size(); i ++) {
            Map<String,String> map = gson.fromJson(building.get(i),stringStringMap);

            mBuilding b = new mBuilding();
            float lati = 0;
            float longti = 0;
            if(map.get("lati") != null) lati = Float.parseFloat(map.get("lati"));
            if(map.get("longti") != null) lati = Float.parseFloat(map.get("longti"));
            b.set(map.get("bname"),lati,longti);
            mBuildings.add(b);
        }
        for(int i = 0 ; i < classroom.size(); i ++) {
            Map<String,String> map = gson.fromJson(classroom.get(i),stringStringMap);

            mClassroom mC = new mClassroom();
            mC.set(map.get("bname"),map.get("rid"));
            mClassrooms.add(mC);
        }
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(mClasses);
                realm.insert(mClasstables);
                realm.insert(mUnivSchedules);
                realm.insert(mBuildings);
                realm.insert(mClassrooms);
            }
        });

        return v;
    }

    public List<ClassListItem> getAllitem() {
        return allitem;
    }


    public List<ClassListItem> getNewItems(List<String[]> items) {
        List<ClassListItem> result = new ArrayList<>();
        Realm realm = Realm.getInstance(singleton.getmConfig());
        for(int i = 0 ; i < items.size(); i ++) {
            String[] period,rid;
            String cid = items.get(i)[0];
            RealmQuery<mClasstable> query = realm.where(mClasstable.class);
            query.equalTo("cid",cid);
            RealmResults<mClasstable> classTresults = query.findAll();
            classTresults = classTresults.sort("cday");
            period = new String[classTresults.size()];
            rid = new String[classTresults.size()];
            for(int j = 0; j < classTresults.size() ; j++) {
                mClasstable m = classTresults.get(j);
                rid[j] = m.getRid();
                int day =  m.getCday();
                int sp = m.getSperiod();
                int ep = m.getEperiod();
                period[j] = singleton.intToStringDay(day)+univscheduleT[sp]+"~"+univscheduleT[ep];
            }
            result.add(new ClassListItem(cid,items.get(i)[1],items.get(i)[2],items.get(i)[3],items.get(i)[4],items.get(i)[5],items.get(i)[6],period,rid));
        }
        return result;
    }

    private void getAllItems() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<ClassListItem> result = new ArrayList<>();
                Realm realm = Realm.getInstance(singleton.getmConfig());
                RealmResults<mClass> classes = realm.where(mClass.class).findAll();
                for(mClass c : classes) {
                    String[] period,rid;
                    boolean isSelect;
                    String cid = c.getCid();
                    String title = c.getCname();
                    String grade;
                    if(c.getGrade() != 0) grade = c.getGrade() + "학년";
                    else grade = "전체학년";
                    String point = c.getPoint() + "학점";
                    String type = c.getType();
                    String prof = c.getProf();
                    String department = c.getDepartment();
                    RealmQuery<mClasstable> query = realm.where(mClasstable.class);
                    query.equalTo("cid",cid);
                    RealmResults<mClasstable> classTresults = query.findAll();
                    classTresults = classTresults.sort("cday");
                    period = new String[classTresults.size()];
                    rid = new String[classTresults.size()];
                    for(int j = 0; j < classTresults.size() ; j++) {
                        mClasstable m = classTresults.get(j);
                        rid[j] = m.getRid();
                        int day =  m.getCday();
                        int sp = m.getSperiod();
                        int ep = m.getEperiod();
                        period[j] = singleton.intToStringDay(day)+univscheduleT[sp]+"~"+univscheduleT[ep];
                    }
                    result.add(new ClassListItem(cid,title,grade,type,point,prof,department,period,rid));
                }
                allitem = result;
            }
        }).run();

    }

    public List<TimeTableItem> getMyclassItem() {
        List<TimeTableItem> items = new ArrayList<>();
        List<String>[] cids = new ArrayList[6];
        List<String>[] cnames = new ArrayList[6];
        List<String>[] rooms = new ArrayList[6];
        List<String>[] stimes = new ArrayList[6];
        List<String>[] etimes = new ArrayList[6];
        List<Integer>[] colors = new ArrayList[6];
        for(int i = 0 ; i < 6 ;i ++) {
            cids[i] = new ArrayList<>();
            cnames[i] = new ArrayList<>();
            rooms[i] = new ArrayList<>();
            stimes[i] = new ArrayList<>();
            etimes[i] = new ArrayList<>();
            colors[i] = new ArrayList<>();
        }
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mMyclass> my = realm.where(mMyclass.class).findAll();
        for(mMyclass m : my) {
            String cid = m.getCid();
            int color = m.getColor();
            RealmResults<mClasstable> mc = realm.where(mClasstable.class).equalTo("cid",cid).findAll();
            for(mClasstable c : mc) {
                String rid = c.getRid();
                int cday = c.getCday();
                String cname = realm.where(mClass.class).equalTo("cid",cid).findFirst().getCname();
                String stime = univscheduleS[c.getSperiod()][0];
                String etime = univscheduleS[c.getEperiod()][1];
                if(cday != 6) {
                    cids[cday].add(cid);
                    cnames[cday].add(cname);
                    rooms[cday].add(rid);
                    stimes[cday].add(stime);
                    etimes[cday].add(etime);
                    colors[cday].add(color);
                }
            }
            RealmResults<mCustomtable> mt = realm.where(mCustomtable.class).equalTo("cid",cid).findAll();
            for(mCustomtable c : mt) {
                String rname = c.getRname();
                int cday = c.getCday();
                String cname = c.getCname();
                String stime = c.getStime();
                String etime = c.getEtime();
                cids[cday].add(cid);
                cnames[cday].add(cname);
                rooms[cday].add(rname);
                stimes[cday].add(stime);
                etimes[cday].add(etime);
                colors[cday].add(color);
            }
        }
        items.add(null);
        for(int i = 1; i < 6;i++) {
            items.add(new TimeTableItem(
                    cids[i].toArray(new String[cids[i].size()]),cnames[i].toArray(new String[cnames[i].size()]),rooms[i].toArray(new String[rooms[i].size()]),
                    stimes[i].toArray(new String[stimes[i].size()]),etimes[i].toArray(new String[etimes[i].size()]),colors[i].toArray(new Integer[colors[i].size()]),i));
        }
        return items;
    }

    public List<Widget44Item>[] getWidget44Item() {
        List<Widget44Item>[] result = new ArrayList[5];
        for(int i = 0; i < 5; i ++) {
            result[i] = new ArrayList<>();
        }
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mMyclass> my = realm.where(mMyclass.class).findAll();
        for(mMyclass m : my) {
            String cid = m.getCid();
            int color = m.getColor();
            RealmResults<mClasstable> mc = realm.where(mClasstable.class).equalTo("cid",cid).findAll();
            for(mClasstable c : mc) {
                String cname = realm.where(mClass.class).equalTo("cid",cid).findFirst().getCname();
                String rid = c.getRid();
                int day = c.getCday();
                String stime = univscheduleS[c.getSperiod()][0];
                String etime = univscheduleS[c.getEperiod()][1];
                result[day-1].add(new Widget44Item(cname,rid,stime,etime,color));
            }
            RealmResults<mCustomtable> mt = realm.where(mCustomtable.class).equalTo("cid",cid).findAll();
            for(mCustomtable c : mt) {
                String cname = c.getCname();
                String rname = c.getRname();
                int day = c.getCday();
                String stime = c.getStime();
                String etime = c.getEtime();
                result[day-1].add(new Widget44Item(cname,rname,stime,etime,color));
            }
        }
        return result;
    }

    public int AlterMyclass(Context context, String cid) {
        int result = 0;
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mMyclass> my = realm.where(mMyclass.class).equalTo("cid",cid).findAll();
        RealmResults<mMyclass> al = realm.where(mMyclass.class).findAll();
        RealmResults<mClasstable> mc = realm.where(mClasstable.class).equalTo("cid",cid).findAll();//추가할려는 시간표
        String[] bnames = new String[mc.size()];
        String[] rids = new String[mc.size()];
        for(int i = 0 ; i < mc.size() ; i++) {
            bnames[i] = mc.get(i).getBname();
            rids[i] = mc.get(i).getRid();
        }
        if (my.size() == 0) { //추가, 중복체크
            if (al.size() > 0) {
                mcstart : for(mClasstable c : mc) { //db 시간표 겹침 체크
                    int day = c.getCday();
                    float stime = univscheduleF[c.getSperiod()][0];
                    float etime = univscheduleF[c.getEperiod()][1];
                    for(mMyclass y : al) {
                        RealmResults<mClasstable> ch = realm.where(mClasstable.class).equalTo("cid",y.getCid()).findAll();//기존의 시간표
                        for(mClasstable la : ch) {
                            int mday = la.getCday();
                            float mstime = univscheduleF[la.getSperiod()][0];
                            float metime = univscheduleF[la.getEperiod()][1];
                            if (day != 6 && day == mday && ((stime < mstime && etime > mstime) || (etime > metime && stime < metime) || (stime > mstime && etime < metime) || (stime == mstime || etime == metime))) {
                                result = -1;
                                break mcstart;
                            }
                        }
                        RealmResults<mCustomtable> mt = realm.where(mCustomtable.class).equalTo("cid",y.getCid()).findAll();//기존의 커스텀 사간표
                        for(mCustomtable lm : mt) {
                            int mday = lm.getCday();
                            float mstime = singleton.timeTofloat(lm.getStime());
                            float metime = singleton.timeTofloat(lm.getEtime());
                            if (day != 6 && day == mday && ((stime < mstime && etime > mstime) || (etime > metime && stime < metime) || (stime > mstime && etime < metime) || (stime == mstime || etime == metime))) {
                                result = -1;
                                break mcstart;
                            }
                        }
                    }
                }
            }
            if(result != -1) { //추가
                int co = 0;
                boolean[] colors = new boolean[18];
                Arrays.fill(colors, false);
                for(mMyclass m : al) {
                    colors[m.getColor()] = true;
                }
                for(int i = 0; i < 18 ; i++){
                    if(!colors[i]) {
                        co = i;
                        break;
                    }
                    if(i == 17) {
                        co = (int) (Math.random() * 17);
                    }
                }
                //즐겨찾기 추가
                for(int i = 0 ; i < bnames.length ; i++) FavoriteUpdate(context, true, bnames[i], rids[i]);
                realm.beginTransaction();
                mMyclass mclass = realm.createObject(mMyclass.class,cid);
                mclass.setColor(co);
                realm.commitTransaction();
            }
        } else { //제거
            realm.beginTransaction();
            realm.where(mMyclass.class).equalTo("cid",cid).findAll().deleteAllFromRealm();
            realm.where(mCustomtable.class).equalTo("cid",cid).findAll().deleteAllFromRealm();
            realm.commitTransaction();
            result = 1;
            String bn = preferences.getString("beforenoti","");
            if(bn.equals(cid)) editor.putString("beforenoti","").commit();
            for(int i = 0 ; i < bnames.length ; i++) {
                //즐겨찾기 제거
                boolean overlap = false;
                start : for(mMyclass c : al) {
                    RealmResults<mClasstable> mt = realm.where(mClasstable.class).equalTo("cid",c.getCid()).findAll();
                    for(mClasstable m : mt) {
                        if(bnames[i].equals(m.getBname()) && rids[i].equals(m.getRid())) {
                            overlap = true;
                            break start;
                        }
                    }
                }
                if(!overlap) {
                    FavoriteUpdate(context, false, bnames[i], rids[i]);
                }
            }
        }
        return result;
    }

    public boolean insertCustom(String cname, String rname,String Alter, List<Float[]> items) {
        boolean result = true;
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mMyclass> al = realm.where(mMyclass.class).findAll();
        if (al.size() > 0) {
            all : for(mMyclass a : al) {
                RealmResults<mClasstable> mc = realm.where(mClasstable.class).equalTo("cid", a.getCid()).findAll();
                for (mClasstable m : mc) {
                    int day = m.getCday();
                    float stime = univscheduleF[m.getSperiod()][0];
                    float etime = univscheduleF[m.getEperiod()][1];
                    for (Float[] f : items) {
                        int mday = (int) ((float) f[0]);
                        float mstime = f[1];
                        float metime = f[2];
                        if (day != 6 && day == mday && ((stime < mstime && etime > mstime) || (etime > metime && stime < metime) || (stime > mstime && etime < metime) || (stime == mstime || etime == metime))) {
                            result = false;
                            break all;
                        }
                    }
                }
                RealmResults<mCustomtable> mt = realm.where(mCustomtable.class).equalTo("cid", a.getCid()).findAll();
                for (mCustomtable c : mt) {
                    if (!c.getCid().equals(Alter)) {
                        int day = c.getCday();
                        float stime = singleton.timeTofloat(c.getStime());
                        float etime = singleton.timeTofloat(c.getEtime());
                        for (Float[] f : items) {
                            int mday = (int) ((float) f[0]);
                            float mstime = f[1];
                            float metime = f[2];
                            if (day != 6 && day == mday && ((stime < mstime && etime > mstime) || (etime > metime && stime < metime) || (stime > mstime && etime < metime) || (stime == mstime || etime == metime))) {
                                result = false;
                                break all;
                            }
                        }
                    }
                }
            }
            if(result) {
                int ci = preferences.getInt("custom",0);
                String cid = "c"+ci;
                ci += 1;
                int co = 0;
                boolean[] colors = new boolean[18];
                Arrays.fill(colors, false);
                for(mMyclass m : al) {
                    colors[m.getColor()] = true;
                }
                for(int i = 0; i < 18 ; i++){
                    if(!colors[i]) {
                        co = i;
                        break;
                    }
                    if(i == 17) {
                        co = (int) (Math.random() * 17);
                    }
                }
                for(Float[] f : items) {
                    realm.beginTransaction();
                    mCustomtable mt = realm.createObject(mCustomtable.class);
                    mt.set(cid, cname, rname, (int) ((float) f[0]), singleton.floatTotime(f[1]), singleton.floatTotime(f[2]));
                    realm.commitTransaction();
                }
                realm.beginTransaction();
                mMyclass mclass = realm.createObject(mMyclass.class,cid);
                mclass.setColor(co);
                realm.commitTransaction();
                editor.putInt("custom",ci);
                editor.commit();
            }
        } else {
            int ci = preferences.getInt("custom",0);
            String cid = "c"+ci;
            int co = 0;
            boolean[] colors = new boolean[18];
            Arrays.fill(colors, false);
            for(mMyclass m : al) {
                colors[m.getColor()] = true;
            }
            for(int i = 0; i < 18 ; i++){
                if(!colors[i]) {
                    co = i;
                    break;
                }
                if(i == 17) {
                    co = (int) (Math.random() * 17);
                }
            }
            for(Float[] f : items) {
                realm.beginTransaction();
                mCustomtable mt = realm.createObject(mCustomtable.class);
                mt.set(cid, cname, rname, (int) ((float) f[0]), singleton.floatTotime(f[1]), singleton.floatTotime(f[2]));
                realm.commitTransaction();
            }
            realm.beginTransaction();
            mMyclass mclass = realm.createObject(mMyclass.class,cid);
            mclass.setColor(co);
            realm.commitTransaction();
            editor.putInt("custom",ci+1);
            editor.commit();
        }
        return result;
    }


    public String[] getClassInfos(String cid,int day) {
        String[] result = new String[3];
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mClasstable> ms = realm.where(mClasstable.class).equalTo("cid",cid).equalTo("cday",day).findAll();
        if(ms.size() == 1) {
            mClasstable m = ms.get(0);
            String rid = m.getRid();
            String stime = univscheduleS[m.getSperiod()][0];
            String etime = univscheduleS[m.getEperiod()][1];
            mClass c = realm.where(mClass.class).equalTo("cid", cid).findFirst();
            String cname = c.getCname();
            result[0] = stime + " - " + etime;
            result[1] = cname;
            result[2] = rid;
        } else {
            mCustomtable mt = realm.where(mCustomtable.class).equalTo("cid",cid).equalTo("cday",day).findFirst();
            String rame = mt.getRname();
            String stime = mt.getStime();
            String etime = mt.getEtime();
            String cname = mt.getCname();
            result[0] = stime + " - " + etime;
            result[1] = cname;
            result[2] = rame;
        }
        return result;
    }

    public List<FavoriteRoomItem> favoriteRoomSet() {
        List<FavoriteRoomItem> items = new ArrayList<>();
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mRfavorite> rfavrow = realm.where(mRfavorite.class).findAll();
        int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
        float nowtime = singleton.nowTime();
        if(nowtime >= 8) {
            for(mRfavorite m : rfavrow) {
                String bname = m.getBname();
                String rid = m.getRid();
                float nextTime = 24;
                float lastTime = 0;
                RealmResults<mClasstable> mClasstables = realm.where(mClasstable.class).equalTo("bname",bname).equalTo("rid",rid).equalTo("cday",today).findAll();
                if(mClasstables.size() > 0) {
                    boolean isInclass = false;
                    for (mClasstable c : mClasstables) {
                        String cid = c.getCid();
                        float stime = univscheduleF[c.getSperiod()][0];
                        float etime = univscheduleF[c.getEperiod()][1];
                        if(lastTime < etime) lastTime = etime;
                        if(nowtime < stime && nextTime > stime) nextTime = stime;
                        if(nowtime >= stime && nowtime <= etime) {
                            isInclass = true;
                            items.add(new FavoriteRoomItem(false,bname,rid,univscheduleS[c.getEperiod()][1],realm.where(mClass.class).equalTo("cid",cid).findFirst().getCname()));
                        }
                    }
                    if(!isInclass) {
                        if (nowtime < nextTime && nextTime != 24) {
                            items.add(new FavoriteRoomItem(true, bname, rid, singleton.floatTotime(nextTime), null));
                        } else {
                            items.add(new FavoriteRoomItem(true,bname,rid,"00:00",null));
                        }
                    }
                } else {
                    items.add(new FavoriteRoomItem(true,bname,rid,"00:00",null));
                }
            }
        } else {
            for (mRfavorite m : rfavrow) {
                items.add(new FavoriteRoomItem(true,m.getBname(),m.getRid(),"00:00",null));
            }
        }
        return items;
    }

    public int[] countBuildingRoom(String bname) {
        Realm realm = Realm.getInstance(singleton.getmConfig());
        int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
        float nowtime = singleton.nowTime();
        int count[] = new int[2];
        int allRoomCount = (int) realm.where(mClassroom.class).equalTo("bname", bname).count();
        count[0]= 0; count[1] = 0;
        RealmResults<mClasstable> tablerow = realm.where(mClasstable.class).equalTo("bname",bname).equalTo("cday",today).findAll();
        if(tablerow.size() != 0) {
            for(mClasstable m : tablerow) {
                float stime = univscheduleF[m.getSperiod()][0];
                float etime = univscheduleF[m.getEperiod()][1];
                if ((nowtime >= stime) && (nowtime <= etime)) count[1]++;
            }
            count[0] = allRoomCount - count[1];
        } else {
            count[0] = allRoomCount;
        }
        return count;
    }


    public List<FavoriteBuildingItem> favoriteBuildingSet() {
        List<FavoriteBuildingItem> items = new ArrayList<>();
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mBfavorite> bfavrow = realm.where(mBfavorite.class).findAll();
        int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
        float nowtime = singleton.nowTime();
        if(nowtime >= 8) {
            for (mBfavorite m : bfavrow) {
                String bname = m.getBname();
                int inClassCount = 0;
                int allRoomCount = (int) realm.where(mClassroom.class).equalTo("bname", bname).count();
                RealmResults<mClasstable> mClasstables = realm.where(mClasstable.class).equalTo("bname", bname).equalTo("cday", today).findAll();
                if(mClasstables.size() > 0) {
                    for (mClasstable c : mClasstables) {
                        float stime = univscheduleF[c.getSperiod()][0];
                        float etime = univscheduleF[c.getEperiod()][1];
                        if (nowtime >= stime && nowtime <= etime) inClassCount++;
                    }
                    items.add(new FavoriteBuildingItem(bname, allRoomCount - inClassCount));
                } else {
                    items.add(new FavoriteBuildingItem(bname, allRoomCount));
                }
            }
        } else {
            for(mBfavorite m : bfavrow) {
                String bname = m.getBname();
                items.add(new FavoriteBuildingItem(bname,(int) realm.where(mClassroom.class).equalTo("bname", bname).count()));
            }
        }
        return items;
    }


    public void FavoriteUpdate(final Context context, final boolean isadd, final String bname, final String rid) {
        Realm realm = Realm.getInstance(singleton.getmConfig());
        final AlertDialog loading = singleton.Loading(context);
        loading.show();
        if(preferences.getBoolean(context.getResources().getString(R.string.isuser),false)) {
            //웹연동
        } else {
            if(isadd) {
                if (rid == null) {
                    if (realm.where(mBfavorite.class).equalTo("bname", bname).count() == 0) {
                        realm.beginTransaction();
                        mBfavorite m = realm.createObject(mBfavorite.class);
                        m.setBname(bname);
                        realm.commitTransaction();
                        FavoritePublisher.getInstance().changeBuildingNotify();
                        loading.dismiss();
                    } else {
                        loading.dismiss();
                    }
                } else {
                    if (realm.where(mRfavorite.class).equalTo("bname", bname).equalTo("rid", rid).count() == 0) {
                        realm.beginTransaction();
                        mRfavorite m = realm.createObject(mRfavorite.class);
                        m.set(bname, rid);
                        realm.commitTransaction();
                        FavoritePublisher.getInstance().changeRoomNotify();
                        loading.dismiss();
                    } else {
                        loading.dismiss();
                    }
                }
            } else {
                if(rid == null) {
                    realm.beginTransaction();
                    realm.where(mBfavorite.class).equalTo("bname",bname).findAll().deleteAllFromRealm();
                    realm.commitTransaction();
                    FavoritePublisher.getInstance().changeBuildingNotify();
                    loading.dismiss();
                } else {
                    realm.beginTransaction();
                    realm.where(mRfavorite.class).equalTo("bname",bname).equalTo("rid",rid).findAll().deleteAllFromRealm();
                    realm.commitTransaction();
                    FavoritePublisher.getInstance().changeRoomNotify();
                    loading.dismiss();
                }
            }
        }
    }

    public List<BuildinginfoItem> buildingInfoSet(boolean isfreeview, boolean isinclassview, String bname) {
        List<BuildinginfoItem> items = new ArrayList<>();
        Realm realm = Realm.getInstance(singleton.getmConfig());
        int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
        float nowtime = singleton.nowTime();
        RealmResults<mClassroom> rids = realm.where(mClassroom.class).equalTo("bname", bname).findAll();
        for (mClassroom m : rids) {
            String rid = m.getRid();
            boolean isfree = true;
            boolean isfav = false;
            float parentS = 24;
            float parentE = 24;
            String parentC = "";
            RealmResults<mRfavorite> rfav = realm.where(mRfavorite.class).equalTo("bname", bname).findAll();
            RealmResults<mClasstable> tablerow = realm.where(mClasstable.class).equalTo("bname", bname).equalTo("rid", rid).equalTo("cday", today).findAll();
            for (mRfavorite mf : rfav) {
                if (rid.equals(mf.getRid())) {
                    isfav = true;
                    break;
                }
            }
            if (tablerow.size() != 0) {
                for (mClasstable mc : tablerow) {
                    String cid = mc.getCid();
                    float stime = univscheduleF[mc.getSperiod()][0];
                    float etime = univscheduleF[mc.getEperiod()][1];
                    if (nowtime >= stime && nowtime <= etime) {
                        isfree = false;
                        parentS = stime;
                        parentE = etime;
                        parentC = cid;
                        break;
                    } else if(stime - nowtime < parentS - nowtime && nowtime < etime) {
                        parentS = stime;
                        parentE = etime;
                        parentC = cid;
                    }
                }
                float nextTime = 24;
                if (nowtime < parentS && nextTime > parentS) nextTime = parentS;
                if (nowtime != -1) {
                    if (isfreeview) {
                        if (isfree) {
                            if (nextTime != 24)
                                items.add(new BuildinginfoItem(true, isfav, bname, rid, singleton.floatTotime(nextTime), ""));
                            else
                                items.add(new BuildinginfoItem(true, isfav, bname, rid, "00:00", ""));
                        }
                    } else if (isinclassview) {
                        if (!isfree)
                            items.add(new BuildinginfoItem(false, isfav, bname, rid, singleton.floatTotime(parentE), realm.where(mClass.class).equalTo("cid", parentC).findFirst().getCname()));
                    } else {
                        if (isfree) {
                            if (nextTime != 24)
                                items.add(new BuildinginfoItem(true, isfav, bname, rid, singleton.floatTotime(nextTime), ""));
                            else
                                items.add(new BuildinginfoItem(true, isfav, bname, rid, "00:00", ""));
                        } else
                            items.add(new BuildinginfoItem(false, isfav, bname, rid, singleton.floatTotime(parentE), realm.where(mClass.class).equalTo("cid", parentC).findFirst().getCname()));
                    }
                } else {
                    if (!isinclassview && isfree)
                        items.add(new BuildinginfoItem(true, isfav, bname, rid, "00:00", ""));
                }

            } else {
                if (!isinclassview)
                    items.add(new BuildinginfoItem(true, isfav, bname, rid, "00:00", ""));
            }
        }
        return items;
    }

    public String[] searchitemlist() {
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mClass> classes = realm.where(mClass.class).findAll();
        RealmResults<mClassroom> classrooms = realm.where(mClassroom.class).findAll();
        RealmResults<mBuilding> buildings = realm.where(mBuilding.class).findAll();
        List<String> cid = new ArrayList<>();
        List<String> cname = new ArrayList<>();
        List<String> departemnt = new ArrayList<>();
        List<String> prof = new ArrayList<>();
        List<String> rid = new ArrayList<>();
        List<String> bname = new ArrayList<>();
        List<String> last = new ArrayList<>();
        for(mClass m : classes) {
            cid.add(m.getCid());
            if(!cname.contains(m.getCname())) cname.add(m.getCname());
            if(!departemnt.contains(m.getDepartment())) departemnt.add(m.getDepartment());
            if(!prof.contains(m.getProf())) prof.add(m.getProf());
        }
        for(mClassroom m : classrooms) {
            rid.add(m.getRid());
        }
        for(mBuilding m : buildings) {
            bname.add(m.getBname());
        }
        last.addAll(cid);
        last.addAll(cname);
        last.addAll(departemnt);
        last.addAll(prof);
        last.addAll(rid);
        last.addAll(bname);
        return last.toArray(new String[last.size()]);
    }

    public List<SearchRecycleritem> searchitems(String s) {
        List<SearchRecycleritem> items = new ArrayList<>();
        if(!s.equals("")) {
            Realm realm = Realm.getInstance(singleton.getmConfig());
            List<String> buildings = new ArrayList<>();
            List<String[]> rooms = new ArrayList<>();
            RealmResults<mClass> classlist = realm.where(mClass.class).findAll();
            RealmResults<mClassroom> rids = realm.where(mClassroom.class).findAll();
            int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
            float nowtime = singleton.nowTime();
            for (mClassroom m : rids) {
                String bname = m.getBname();
                String rid = m.getRid();
                if (rid.contains(s) || bname.contains(s)) {
                    boolean issameb = false, issamer = false;
                    if (buildings.size() != 0) {
                        for (String r : buildings) {
                            if (r.equals(bname)) issameb = true;
                        }
                        if (!issameb) buildings.add(bname);
                    } else buildings.add(bname);
                    if (rooms.size() != 0) {
                        for (String[] r : rooms) {
                            if (r[0].equals(bname) && r[1].equals(rid)) issamer = true;
                        }
                        if (!issamer) rooms.add(new String[]{bname, rid});
                    } else rooms.add(new String[]{bname, rid});
                }
            }
            for (mClass m : classlist) {
                String cid = m.getCid();
                String department = m.getDepartment();
                String cname = m.getCname();
                String prof = m.getProf();
                if (cid.contains(s) || department.contains(s) || cname.contains(s) || prof.contains(s)) {
                    RealmResults<mClasstable> classtablelist = realm.where(mClasstable.class).equalTo("cid", cid).findAll();
                    for (mClasstable mc : classtablelist) {
                        String bname = mc.getBname();
                        String rid = mc.getRid();
                        boolean issameb = false, issamer = false;
                        if (buildings.size() != 0) {
                            for (String r : buildings) {
                                if (r.equals(bname)) issameb = true;
                            }
                            if (!issameb) buildings.add(bname);
                        } else buildings.add(bname);
                        if (rooms.size() != 0) {
                            for (String[] r : rooms) {
                                if (r[0].equals(bname) && r[1].equals(rid)) issamer = true;
                            }
                            if (!issamer) rooms.add(new String[]{bname, rid});
                        } else rooms.add(new String[]{bname, rid});
                    }
                }
            }
            items.add(new SearchRecycleritem(SearchRecycleritem.navv, "건물"));
            for (String b : buildings) {
                boolean isfav;
                if (realm.where(mBfavorite.class).equalTo("bname", b).count() == 1) isfav = true;
                else isfav = false;
                items.add(new SearchRecycleritem(SearchRecycleritem.buildingv, b, countBuildingRoom(b)[0], isfav));
            }
            items.add(new SearchRecycleritem(SearchRecycleritem.navv, "강의실"));
            for (String[] r : rooms) {
                String bname = r[0];
                String rid = r[1];
                boolean isfav;
                if (realm.where(mRfavorite.class).equalTo("bname", bname).equalTo("rid", rid).count() == 1)
                    isfav = true;
                else isfav = false;
                if (nowtime != -1) {
                    RealmResults<mClasstable> tablerow = realm.where(mClasstable.class).equalTo("bname", bname).equalTo("rid", rid).equalTo("cday", today).findAll();
                    float nextTime = 24;
                    boolean isinclass = false;
                    if (tablerow.size() != 0) {
                        for (mClasstable m : tablerow) {
                            String cid = m.getCid();
                            float stime = univscheduleF[m.getSperiod()][0];
                            float etime = univscheduleF[m.getEperiod()][1];
                            if (nowtime < stime && nextTime > stime) nextTime = stime;
                            if (nowtime >= stime && nowtime <= etime) {
                                String cname = realm.where(mClass.class).equalTo("cid", cid).findFirst().getCname();
                                items.add(new SearchRecycleritem(SearchRecycleritem.roomv, bname, rid, singleton.floatTotime(etime), cname, false, isfav));
                                isinclass = true;
                            }
                        }
                        if (!isinclass) {
                            if (nowtime < nextTime && nextTime != 99) {
                                if(nextTime == 24) nextTime = 0;
                                items.add(new SearchRecycleritem(SearchRecycleritem.roomv, bname, rid, singleton.floatTotime(nextTime), null, true, isfav));
                            }
                            else
                                items.add(new SearchRecycleritem(SearchRecycleritem.roomv, bname, rid, "00:00", null, true, isfav));
                        }
                    } else {
                        items.add(new SearchRecycleritem(SearchRecycleritem.roomv, bname, rid, "00:00", null, true, isfav));
                    }
                } else {
                    items.add(new SearchRecycleritem(SearchRecycleritem.roomv, bname, rid, "00:00", null, true, isfav));
                }
            }
        }
        return items;
    }


    public String[] nextAlarm() {
        Calendar c = Calendar.getInstance();
        int today = c.get(Calendar.DAY_OF_WEEK) - 1;
        long now = c.getTimeInMillis();
        Calendar t = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
        try {
            t.setTime(sf.parse(sf.format(c.getTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long real = Long.MAX_VALUE;
        String name = "";
        String cid = "";
        String bn = preferences.getString("beforenoti","");
        if(today >= 0 && today < 7) {
            if(today == 6) today = 1;
            if(today == 0) today = 2;
            Realm realm = Realm.getInstance(singleton.getmConfig());
            RealmResults<mMyclass> mlist = realm.where(mMyclass.class).findAll();
            for(mMyclass m : mlist) {
                mClasstable mc = realm.where(mClasstable.class).equalTo("cid",m.getCid()).equalTo("cday",today).findFirst();
                if(mc != null) {
                    if(!bn.equals(mc.getCid())) {
                        float stf = univscheduleF[mc.getSperiod()][0];
                        int ho = (int) stf;
                        int mi = (int) (60 * (stf - ho));
                        long st = t.getTimeInMillis() + ho * 3600000 + mi * 60000;
                        if (st > now && st < real) {
                            real = st;
                            cid = mc.getCid();
                            name = realm.where(mClass.class).equalTo("cid", mc.getCid()).findFirst().getCname() + " 수업이 10분후 시작합니다.";
                        }
                    }
                } else {
                    mCustomtable mt = realm.where(mCustomtable.class).equalTo("cid",m.getCid()).equalTo("cday",today).findFirst();
                    if(mt != null) {
                        if (!bn.equals(mt.getCid())) {
                            float stf = singleton.timeTofloat(mt.getStime());
                            int ho = (int) stf;
                            int mi = (int) (60 * (stf - ho));
                            long st = t.getTimeInMillis() + ho * 3600000 + mi * 60000;
                            if (st > now && st < real) {
                                real = st;
                                cid = mt.getCid();
                                name = mt.getCname() + " 수업이 10분후 시작합니다.";
                            }
                        }
                    }
                }
            }
            if(real != Long.MAX_VALUE) return new String[]{name,Long.toString(real-600000),cid};
            else return new String[] {};
        } else {
            return new String[] {};
        }
    }

}
