package com.clab.classpick.utils.Realm

import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.RealmClass

/**
 * Created by kyun on 2017. 9. 12..
 */
@RealmClass
open class mClasstable : RealmModel {

    var bname: String? = null
        internal set
    var rid: String? = null
        internal set
    var cid: String? = null
        internal set
    var cday: Int = 0
        internal set
    var speriod: Int = 0
        internal set
    var eperiod: Int = 0
        internal set

    operator fun set(bname: String, rid: String, cid: String, cday: Int, speriod: Int, eperiod: Int) {
        this.bname = bname
        this.rid = rid
        this.cid = cid
        this.cday = cday
        this.speriod = speriod
        this.eperiod = eperiod

    }
}
