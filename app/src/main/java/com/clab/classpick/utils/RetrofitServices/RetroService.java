package com.clab.classpick.utils.RetrofitServices;

import com.google.gson.JsonElement;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by kyun on 2017-03-04.
 */

public interface RetroService {

    @GET("newclasspclient")
    Call<ResponseBody>newclasspclient(@Query("android") int and,@Query("ios")int ios,@Query("ucode")int ucode);

    @GET("getdb")
    Call<JsonElement>getdb(@Query("ucode")int ucode);

    @GET("getdbV")
    Call<ResponseBody>getdbV(@Query("ucode")int ucode);

    @GET("getunivlist")
    Call<JsonElement> getunivlist();

    @GET("appVmsg")
    Call<JsonElement>appVmsg();

    @GET("feedback")
    Call<ResponseBody>feedback(@Query("contents")String contents,@Query("os")int os,@Query("ver")int ver);


}
