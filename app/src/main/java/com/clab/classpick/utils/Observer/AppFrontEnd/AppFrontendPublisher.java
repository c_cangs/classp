package com.clab.classpick.utils.Observer.AppFrontEnd;

import java.util.ArrayList;

/**
 * Created by kyun on 2017-04-03.
 */

public class AppFrontendPublisher implements OnAppFrontendPulish {

    private volatile static AppFrontendPublisher instance;

    private ArrayList<OnAppFrontendObserv> Observs;

    public AppFrontendPublisher(){
        Observs = new ArrayList<>();
    }

    public static AppFrontendPublisher getInstance (){
        if(instance == null) {
            synchronized (AppFrontendPublisher.class) {
                if(instance == null) {
                    instance = new AppFrontendPublisher();
                }
            }
        }
        return instance;
    }

    @Override
    public void addlistener(OnAppFrontendObserv observ) {
        boolean isoverlap = false;
        for(OnAppFrontendObserv o : Observs) {
            if(o == observ) isoverlap = true;
        }
        if(!isoverlap) Observs.add(observ);

    }

    @Override
    public void notifychange() {
        for(OnAppFrontendObserv observ: Observs)
            observ.OnAppFrontend();
    }

}

interface OnAppFrontendPulish {
    void addlistener(OnAppFrontendObserv observ);
    void notifychange();
}
