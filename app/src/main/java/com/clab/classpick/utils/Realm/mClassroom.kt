package com.clab.classpick.utils.Realm

import io.realm.RealmModel
import io.realm.annotations.RealmClass

/**
 * Created by kyun on 2017. 9. 18..
 */

@RealmClass
open class mClassroom : RealmModel {

    var bname: String? = null
        internal set
    var rid: String? = null
        internal set

    operator fun set(bname: String, rid: String) {
        this.bname = bname
        this.rid = rid
    }
}
