package com.clab.classpick.utils.Singletons;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.WindowManager;

import com.clab.classpick.utils.Realm.mMigration;
import com.clab.classpick.utils.RetrofitServices.AddCookiesInterceptor;
import com.clab.classpick.utils.RetrofitServices.ReceivedCookiesInterceptor;
import com.clab.classpick.utils.RetrofitServices.RetroService;


import org.jetbrains.annotations.Contract;

import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kyun on 2017. 9. 6..
 */

public class Singleton {
    private volatile static Singleton instance;

    private Singleton(Context context) {
        OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(new AddCookiesInterceptor(context)).addInterceptor(new ReceivedCookiesInterceptor(context)).build();
        retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        retroService = retrofit.create(RetroService.class);
        displayMetrics = context.getResources().getDisplayMetrics();
        widthpix = displayMetrics.widthPixels;
        heightpix = displayMetrics.heightPixels;
        density = displayMetrics.density;
        comwidth = widthpix / density;
        comheight = heightpix / density;
        StatusBarHeightpix = (int) (24 * density);
        bmjua = Typeface.createFromAsset(context.getAssets(), "fonts/BMJUA.ttf");
        Nanumr = Typeface.createFromAsset(context.getAssets(), "fonts/NanumSquareR.otf");
        Nanumb = Typeface.createFromAsset(context.getAssets(), "fonts/NanumSquareB.otf");
        Nanumbg = Typeface.createFromAsset(context.getAssets(),"fonts/NanumBarunGothic.ttf");
        Nanumeb = Typeface.createFromAsset(context.getAssets(), "fonts/NanumSquareEB.otf");
        colormain = Color.parseColor("#28baff");
        colorwhite = Color.parseColor("#ffffff");
        colordarkgray = Color.parseColor("#525252");
        Colorfree = Color.parseColor("#F04E77");
        Colorinclass = Color.parseColor("#54416F");
        ColorYello = Color.parseColor("#ffe400");
        tablecolor = new int[18];

        tablecolor[0] = Color.parseColor("#ffd8f0fc");
        tablecolor[1] = Color.parseColor("#fffce8d8");
        tablecolor[2] = Color.parseColor("#759ad9a4");
        tablecolor[3] = Color.parseColor("#54f6abd5");
        tablecolor[4] = Color.parseColor("#c2f4ab75");
        tablecolor[5] = Color.parseColor("#cefff67b");
        tablecolor[6] = Color.parseColor("#8f708eaa");
        tablecolor[7] = Color.parseColor("#ffcce198");
        tablecolor[8] = Color.parseColor("#ffcfa972");
        tablecolor[9] = Color.parseColor("#ff84ccc9");
        tablecolor[10] = Color.parseColor("#b57aacd9");
        tablecolor[11] = Color.parseColor("#69f5969d");
        tablecolor[12] = Color.parseColor("#878789c0");
        tablecolor[13] = Color.parseColor("#ffd1c0a5");
        tablecolor[14] = Color.parseColor("#ffd2d2d2");
        tablecolor[15] = Color.parseColor("#ffc490bf");
        tablecolor[16] = Color.parseColor("#87f4afb7");
        tablecolor[17] = Color.parseColor("#91f4d0e6");
        animaccelerate = android.R.anim.accelerate_decelerate_interpolator;

        Realm.init(context);
        mConfig = new RealmConfiguration.Builder().schemaVersion(2).build();
    }

    public static Singleton getInstance(Context context) {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton(context);
                }
            }
        }
        return instance;
    }

    private static Retrofit retrofit;

    private static RetroService retroService;

    private static RealmConfiguration mConfig;

    private static DisplayMetrics displayMetrics;

    private static final String BASE_URL = "http://1.255.55.179:8010/";

    private static Typeface bmjua, Nanumr, Nanumb, Nanumbg, Nanumeb;

    private static float density, comwidth, comheight;

    private static int widthpix,heightpix,StatusBarHeightpix;

    private static int colormain, colorwhite, colordarkgray, Colorfree, Colorinclass, ColorYello;

    private static int animaccelerate;

    private static int[] tablecolor;

    public RetroService getRetroService() {
        return retroService;
    }

    public RealmConfiguration getmConfig() {
        return mConfig;
    }

    public DisplayMetrics getDisplayMetrics() {
        return displayMetrics;
    }

    public Typeface getBmjua() {
        return bmjua;
    }

    public Typeface getNanumr() {
        return Nanumr;
    }

    public Typeface getNanumb() {
        return Nanumb;
    }

    public Typeface getNanumbg() {
        return Nanumbg;
    }

    public Typeface getNanumeb() {
        return Nanumeb;
    }

    public float getDensity() {
        return density;
    }

    public int getWidthpix() {
        return widthpix;
    }

    public int getHeightpix() {
        return heightpix;
    }

    public int getStatusBarHeightpix() {
        return StatusBarHeightpix;
    }

    public int getColorMain() {
        return colormain;
    }

    public int getColorWhite() {
        return colorwhite;
    }

    public int getColorDarkGray() {
        return colordarkgray;
    }

    public int getColorFree() {
        return Colorfree;
    }

    public int getColorinclass() {
        return Colorinclass;
    }

    public int getColorYello() {
        return ColorYello;
    }

    public int getAnimaccelerate() {
        return animaccelerate;
    }

    public int getTableColor(int p) {
        return tablecolor[p];
    }


    public AlertDialog Loading(Context context) {
        AlertDialog.Builder dialogb = new AlertDialog.Builder(context);
        dialogb.setMessage("로딩중").setCancelable(false);
        AlertDialog dialog = dialogb.create();
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    public AlertDialog Errdialog(final Context context, final boolean finish, String code) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("에러코드 " + code).setMessage("오류가 발생하였습니다 관리자에게 문의해주세요.").setPositiveButton("확인", null)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if(finish) {
                    ((Activity)context).finish();
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            }
        });
        return dialog.create();
    }

    public AlertDialog.Builder CustomDialog(Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        //다이얼로그 ui 작업
        return dialog;
    }

    public int ratiodp(boolean isWidth, double ratio) {
        if (isWidth)
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) (comwidth * ratio), displayMetrics);
        else
            return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) (comheight * ratio), displayMetrics);
    }

    public int dpTopx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) dp, displayMetrics);
    }

    public int weightdp(boolean isWidth, double maxWeight, double Weight) {
        if(isWidth)
            return (int) (widthpix / (maxWeight*(1/Weight)));
        else
            return (int) ((heightpix - StatusBarHeightpix) / (maxWeight*(1/Weight)));
    }

    public int Cweightdp(int pix, double maxWeight, double Weight) {
        return (int) (pix / (maxWeight*(1/Weight)));
    }

    public float timeTofloat(String t) {
        return Float.parseFloat(t.substring(0,t.indexOf(":"))) + Float.parseFloat(t.substring(t.indexOf(":")+1,t.length()))/60;
    }

    public float nowTime() {
        Calendar c = Calendar.getInstance();
        float hour = c.get(Calendar.HOUR_OF_DAY);
        float min = (float)c.get(Calendar.MINUTE)/60;
        return  hour + min;
    }

    public String floatTotime(float f) {
        int h = (int) f;
        int m = (int)(60*(f - h));
        String hs,ms;
        if(h < 10) hs = "0"+h;
        else hs = Integer.toString(h);
        if(m < 10) ms = "0" + m;
        else ms = Integer.toString(m);
        return hs + ":" + ms;
    }

    public float intTimeTofloat(int h, int m) {
        return h + (float)(m/60);
    }

    public String intToStringDay(int d) {
        switch (d) {
            case 0:
                return "일";
            case 1:
                return "월";
            case 2:
                return "화";
            case 3:
                return "수";
            case 4:
                return "목";
            case 5:
                return "금";
            case 6:
                return "토";
            default:
                return "unknown";
        }
    }

    public int StringToIntDay(String s) {
        if(s.equals("일")) return 0;
        else if(s.equals("월")) return 1;
        else if(s.equals("화")) return 2;
        else if(s.equals("수")) return 3;
        else if(s.equals("목")) return 4;
        else if(s.equals("금")) return 5;
        else if(s.equals("토")) return 6;
        else return -1;
    }

}
