package com.clab.classpick.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.preference.PreferenceManager
import android.util.Log
import com.clab.classpick.utils.Singletons.mFunction
import java.util.*

/**
 * Created by kyun on 2017. 9. 29..
 */
class BootCastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, p1: Intent?) {
        if(p1?.action == Intent.ACTION_BOOT_COMPLETED || p1?.action == Intent.ACTION_DATE_CHANGED) {
            val e = PreferenceManager.getDefaultSharedPreferences(context).edit()
//            e.putBoolean("setnoti",false)
            e.putString("beforenoti", "")
            e.commit()
            Log.i("bc","bcon")
            val mfunction = mFunction.getInstance(context)

            val setting = mfunction.nextAlarm()
            val manager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pi = PendingIntent.getBroadcast(context, 0, Intent(context, AlarmReceiver::class.java), PendingIntent.FLAG_CANCEL_CURRENT)
            manager.cancel(pi)
            pi.cancel()
            if (setting.isNotEmpty()) {
                val name = setting[0]
                val time = setting[1].toLong()
                val cid = setting[2]

                val c = Calendar.getInstance()
                c.timeInMillis = time
                val t : String = "" + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE)

                if(Build.VERSION.SDK_INT > 23) {
                    Log.i("newalarmBroad","n - " + name + " t - " + t + " time - " + time)
                    manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(context, 0,
                            Intent(context, AlarmReceiver::class.java).putExtra("name", name).putExtra("cid", cid).putExtra("noti", true), 0))
                } else {
                    manager.setExact(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(context, 0,
                            Intent(context, AlarmReceiver::class.java).putExtra("name", name).putExtra("cid", cid).putExtra("noti", true), 0))
                }
            }
        }
    }
}