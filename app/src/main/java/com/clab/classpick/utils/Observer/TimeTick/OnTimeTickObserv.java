package com.clab.classpick.utils.Observer.TimeTick;

/**
 * Created by kyun on 2017-04-09.
 */

public interface OnTimeTickObserv {
    void OnTimeTick(boolean isDateChange);
}
