package com.clab.classpick.utils.Realm

import io.realm.RealmModel
import io.realm.annotations.RealmClass

/**
 * Created by kyun on 2017. 9. 18..
 */

@RealmClass
open class mBuilding : RealmModel {

    var bname: String? = null
        internal set
    var lati: Float = 0.toFloat()
        internal set
    var longti: Float = 0.toFloat()
        internal set

    operator fun set(bname: String, lati: Float, longti: Float) {
        this.bname = bname
        this.lati = lati
        this.longti = longti
    }
}
