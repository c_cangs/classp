package com.clab.classpick.utils.Observer.Favorite;

/**
 * Created by kyun on 2017-03-23.
 */

public interface OnFavoriteChangeBuildingObserv {
    void FavoriteBuildingChanged();
}
