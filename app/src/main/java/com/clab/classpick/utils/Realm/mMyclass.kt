package com.clab.classpick.utils.Realm

import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by kyun on 2017. 9. 12..
 */
@RealmClass
open class mMyclass : RealmModel {

    @PrimaryKey
    var cid: String? = null
    var color: Int = 0
}
