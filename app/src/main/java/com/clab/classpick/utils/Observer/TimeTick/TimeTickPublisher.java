package com.clab.classpick.utils.Observer.TimeTick;

import java.util.ArrayList;

/**
 * Created by kyun on 2017-04-09.
 */

public class TimeTickPublisher implements OnTimeTickPublish {

    private volatile static TimeTickPublisher instance;

    private ArrayList<OnTimeTickObserv> Observs;

    public TimeTickPublisher(){
        Observs = new ArrayList<>();
    }

    public static TimeTickPublisher getInstance (){
        if(instance == null) {
            synchronized (TimeTickPublisher.class) {
                if(instance == null) {
                    instance = new TimeTickPublisher();
                }
            }
        }
        return instance;
    }

    @Override
    public void addlistener(OnTimeTickObserv observ) {
        boolean isoverlap = false;
        for(OnTimeTickObserv o : Observs) {
            if(o == observ) {
                isoverlap = true;
                break;
            }
        }
        if(!isoverlap) Observs.add(observ);

    }

    @Override
    public void notifychange(boolean isDateChange) {
        for(OnTimeTickObserv observ: Observs)
            observ.OnTimeTick(isDateChange);
    }
}

interface OnTimeTickPublish {
    void addlistener(OnTimeTickObserv observ);
    void notifychange(boolean isDateChange);
}
