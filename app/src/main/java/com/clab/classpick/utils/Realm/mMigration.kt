package com.clab.classpick.utils.Realm

import io.realm.DynamicRealm
import io.realm.RealmMigration
import io.realm.RealmObjectSchema
import io.realm.RealmSchema

/**
 * Created by kyun on 2017. 9. 17..
 */

class mMigration : RealmMigration {

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        var oldVersion = oldVersion
        val schema = realm.schema

        if (oldVersion == 0L) {
            if (!schema.contains("mBuilding") && !schema.contains("mClassroom") && !schema.contains("mCustomtable")) {
                val mBuildingSchema = schema.create("mBuilding")
                val mClassroomSchema = schema.create("mClassroom")
                val mCustomtableSchema = schema.create("mCustomtable")

                mBuildingSchema.addField("bname", String::class.java).addField("lati", Float::class.javaPrimitiveType).addField("longti", Float::class.javaPrimitiveType)
                mClassroomSchema.addField("bname", String::class.java).addField("rid", String::class.java)
                mCustomtableSchema.addField("cid", String::class.java).addField("cname", String::class.java).
                        addField("rname", String::class.java).addField("cday", Int::class.java).addField("stime", String::class.java).
                        addField("etime", String::class.java)
            }
            oldVersion++
        }

        if (oldVersion == 1L) {
            if (!schema.contains("mBfavorite") && !schema.contains("mRfavorite")) {
                val mBfavoriteSchema = schema.create("mBfavorite")
                val mRfavoriteSchema = schema.create("mRfavorite")

                mBfavoriteSchema.addField("bname", String::class.java)
                mRfavoriteSchema.addField("bname", String::class.java).addField("rid", String::class.java)
            }
            oldVersion++
        }

    }
}
