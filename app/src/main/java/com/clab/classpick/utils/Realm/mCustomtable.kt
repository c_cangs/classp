package com.clab.classpick.utils.Realm

import io.realm.RealmModel
import io.realm.annotations.RealmClass

/**
 * Created by kyun on 2017. 9. 14..
 */

@RealmClass
open class mCustomtable : RealmModel {

    var cid: String? = null
        internal set
    var cname: String? = null
        internal set
    var rname: String? = null
        internal set
    var cday: Int = 0
        internal set
    var stime: String? = null
        internal set
    var etime: String? = null
        internal set

    operator fun set(cid: String, cname: String, rname: String, cday: Int, stime: String, etime: String) {
        this.cid = cid
        this.cname = cname
        this.rname = rname
        this.cday = cday
        this.stime = stime
        this.etime = etime
    }


}
