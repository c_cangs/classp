package com.clab.classpick.utils.Realm

import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import io.realm.annotations.RealmModule

/**
 * Created by kyun on 2017. 9. 12..
 */
@RealmClass
open class mClass : RealmModel {

    var cid: String? = null
        internal set
    var point: Int = 0
        internal set
    var type: String? = null
        internal set
    var department: String? = null
        internal set
    var cname: String? = null
        internal set
    var prof: String? = null
        internal set
    var grade: Int = 0
        internal set

    operator fun set(cid: String, point: Int, type: String, department: String, cname: String, prof: String, grade: Int) {
        this.cid = cid
        this.point = point
        this.type = type
        this.department = department
        this.cname = cname
        this.prof = prof
        this.grade = grade
    }

}
