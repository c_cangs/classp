package com.clab.classpick.utils

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.PowerManager
import android.preference.PreferenceManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.clab.classpick.NotiActivity
import com.clab.classpick.R
import com.clab.classpick.SplashActivity
import com.clab.classpick.utils.Singletons.mFunction
import java.util.*

/**
 * Created by kyun on 2017. 9. 29..
 */
class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val pm = context!!.getSystemService(Context.POWER_SERVICE) as PowerManager
        val wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"tag")
        wl.acquire()
        val b = PreferenceManager.getDefaultSharedPreferences(context)
        val e = b.edit()
        if (intent!!.getBooleanExtra("noti", false)) {
            val cid = intent.getStringExtra("cid")
            if(b.getString("boforenoti","") != cid) {
                val ti = intent.getStringExtra("name")
                val noti = NotificationCompat.Builder(context)
                        .setContentTitle(ti)
                        .setContentText(ti)
                        .setSmallIcon(R.drawable.icon)
                        .setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(if (Build.VERSION.SDK_INT > 24) NotificationManager.IMPORTANCE_HIGH else Notification.PRIORITY_HIGH)
                val stackBuilder = TaskStackBuilder.create(context)
                stackBuilder.addParentStack(SplashActivity::class.java)
                stackBuilder.addNextIntent(Intent(context, SplashActivity::class.java))
                val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                noti.setContentIntent(resultPendingIntent)
                val mNotificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                mNotificationManager.notify(0, noti.build())
//                e.putBoolean("setnoti", false)
                e.putString("beforenoti", cid)
//                e.putBoolean("setnoti", true)
                e.commit()
                var t = false
                if (Build.VERSION.SDK_INT > 19) {
                    if (!pm.isInteractive) t = true //context.startActivity(Intent(context, NotiActivity::class.java).putExtra("name", ti).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
                } else {
                    if (!pm.isScreenOn) t = true //context.startActivity(Intent(context, NotiActivity::class.java).putExtra("name", ti).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
                }
                context.startActivity(Intent(context, NotiActivity::class.java).putExtra("name", ti).putExtra("isScreen",t).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            }
        }
        wl.release()
//        else if (!b.getBoolean("setnoti", false)) {
//            val mfunction = mFunction.getInstance(context)
//
//            val setting = mfunction.nextAlarm()
//            val manager = context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
//            val pi = PendingIntent.getBroadcast(context, 0, Intent(context, this::class.java), PendingIntent.FLAG_CANCEL_CURRENT)
//            manager.cancel(pi)
//            pi.cancel()
//            if (setting.isNotEmpty()) {
//                val name = setting[0]
//                val time = setting[1].toLong()
//                val cid = setting[2]
//
//                val c = Calendar.getInstance()
//                c.timeInMillis = time
//                val t : String = "" + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE)
//
//                if(Build.VERSION.SDK_INT > 23) {
//                    Log.i("newalarmBroad","n - " + name + " t - " + t + " time - " + time)
//                    manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(context, 0,
//                            Intent(context, this::class.java).putExtra("name", name).putExtra("cid", cid).putExtra("noti", true), 0))
//                } else {
//                    manager.setExact(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(context, 0,
//                            Intent(context, this::class.java).putExtra("name", name).putExtra("cid", cid).putExtra("noti", true), 0))
//                }
//                e.putBoolean("setnoti", true)
//                e.commit()
//            }
//        }
    }
}