package com.clab.classpick.utils.Observer.Favorite;

import java.util.ArrayList;

/**
 * Created by kyun on 2017-03-23.
 */

public class FavoritePublisher implements OnFavoriteChangePublish {
    private volatile static FavoritePublisher instance;

    private ArrayList<OnFavoriteChangeRoomObserv> roomObservs;
    private ArrayList<OnFavoriteChangeBuildingObserv> buildingObservs;

    public FavoritePublisher(){
        roomObservs = new ArrayList<>();
        buildingObservs = new ArrayList<>();
    }

    public static FavoritePublisher getInstance (){
        if(instance == null) {
            synchronized (FavoritePublisher.class) {
                if(instance == null) {
                    instance = new FavoritePublisher();
                }
            }
        }
        return instance;
    }

    @Override
    public void addroom(OnFavoriteChangeRoomObserv observ) {
        boolean isoverlap = false;
        for(OnFavoriteChangeRoomObserv o : roomObservs) {
            if (o == observ) isoverlap= true;
        }
        if(!isoverlap) roomObservs.add(observ);
    }

    @Override
    public void deleteroom(OnFavoriteChangeBuildingObserv observ) {
        for(int i = 0; i < roomObservs.size(); i++) {
            if(roomObservs.get(i) == observ) roomObservs.remove(i);
        }
    }

    @Override
    public void addbuilding(OnFavoriteChangeBuildingObserv observ) {
        boolean isoverlap = false;
        for(OnFavoriteChangeBuildingObserv o : buildingObservs) {
            if(o == observ) isoverlap = true;
        }
        if(!isoverlap) buildingObservs.add(observ);
    }

    @Override
    public void deletebuilding(OnFavoriteChangeBuildingObserv observ) {
        for(int i = 0; i < buildingObservs.size(); i++) {
            if(buildingObservs.get(i) == observ) buildingObservs.remove(i);
        }
    }


    @Override
    public void changeRoomNotify() {
        for(OnFavoriteChangeRoomObserv observ : roomObservs){
            observ.FavoriteRoomChanged();
        }
    }

    @Override
    public void changeBuildingNotify() {
        for(OnFavoriteChangeBuildingObserv observ : buildingObservs) {
            observ.FavoriteBuildingChanged();
        }
    }
}

interface OnFavoriteChangePublish {
    void addroom(OnFavoriteChangeRoomObserv observ);
    void deleteroom(OnFavoriteChangeBuildingObserv observ);
    void addbuilding(OnFavoriteChangeBuildingObserv observ);
    void deletebuilding(OnFavoriteChangeBuildingObserv observ);
    void changeRoomNotify();
    void changeBuildingNotify();
}
