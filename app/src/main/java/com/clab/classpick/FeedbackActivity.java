package com.clab.classpick;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clab.classpick.utils.Singletons.Singleton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kyun on 2017. 9. 11..
 */

public class FeedbackActivity extends Activity {

    private Singleton singleton;

    private RelativeLayout topCon, rCon;

    private Button send;

    private InputMethodManager im;

    private int tCH;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nullv);
        if (singleton == null) singleton = Singleton.getInstance(this);
        tCH = 0;
        im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        LayoutInflater inflater = LayoutInflater.from(this);
        final View v = inflater.inflate(R.layout.activity_feedback, null);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                im.hideSoftInputFromWindow(view.getWindowToken(),0);
            }
        });
        addContentView(v, new LinearLayout.LayoutParams(singleton.getWidthpix(), singleton.getHeightpix() - singleton.getStatusBarHeightpix()));
        final Window mRootWindow = getWindow();
        v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int screenHeight = v.getRootView().getHeight();
                Rect r = new Rect();
                View view = mRootWindow.getDecorView();
                view.getWindowVisibleDisplayFrame(r);

                int keyBoardHeight = screenHeight - r.bottom;
                if (tCH <= 0) tCH = topCon.getHeight();
                if (keyBoardHeight > 100) {
                    topCon.animate().translationY(-tCH).withLayer()
                            .setInterpolator(AnimationUtils.loadInterpolator(FeedbackActivity.this, singleton.getAnimaccelerate())).setDuration(250).start();
                    rCon.animate().translationY(-tCH).withLayer()
                            .setInterpolator(AnimationUtils.loadInterpolator(FeedbackActivity.this, singleton.getAnimaccelerate())).setDuration(250).start();
                    send.animate().translationY(-tCH-send.getHeight()).withLayer()
                            .setInterpolator(AnimationUtils.loadInterpolator(FeedbackActivity.this, singleton.getAnimaccelerate())).setDuration(250).start();
                } else {
                    topCon.animate().translationY(0).withLayer()
                            .setInterpolator(AnimationUtils.loadInterpolator(FeedbackActivity.this, singleton.getAnimaccelerate())).setDuration(250).start();
                    rCon.animate().translationY(0).withLayer()
                            .setInterpolator(AnimationUtils.loadInterpolator(FeedbackActivity.this, singleton.getAnimaccelerate())).setDuration(250).start();
                    send.animate().translationY(0).withLayer()
                            .setInterpolator(AnimationUtils.loadInterpolator(FeedbackActivity.this, singleton.getAnimaccelerate())).setDuration(250).start();
                }
            }
        });

        View back = findViewById(R.id.feedback_back_view);
        RelativeLayout.LayoutParams backparam = (RelativeLayout.LayoutParams) back.getLayoutParams();
        backparam.height = singleton.ratiodp(false, 0.028);
        backparam.width = singleton.ratiodp(true, 0.028);
        backparam.leftMargin = singleton.ratiodp(true, 0.032);
        TextView tabtxt = findViewById(R.id.feedback_tab_txt);
        tabtxt.setTypeface(singleton.getBmjua());

        findViewById(R.id.feedback_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        topCon = findViewById(R.id.feedback_topcon);

        View logo = findViewById(R.id.feedback_logo);
        RelativeLayout.LayoutParams logoparam = (RelativeLayout.LayoutParams) logo.getLayoutParams();
        logoparam.width = singleton.ratiodp(true, 0.097);
        logoparam.height = singleton.ratiodp(false, 0.085);
        logoparam.topMargin = singleton.ratiodp(false, 0.032);
        TextView info = findViewById(R.id.feedback_info);
        info.setTypeface(singleton.getBmjua());
        ((RelativeLayout.LayoutParams) info.getLayoutParams()).topMargin = singleton.ratiodp(false, 0.158);

        rCon = findViewById(R.id.feedback_rcon);
        ((RelativeLayout.LayoutParams) rCon.getLayoutParams()).topMargin = singleton.ratiodp(false, 0.333);

        final EditText edit = findViewById(R.id.feedback_edit);
        edit.setTypeface(singleton.getNanumr());
        RelativeLayout.LayoutParams editparam = (RelativeLayout.LayoutParams) edit.getLayoutParams();
        editparam.width = singleton.ratiodp(true, 0.765);
        editparam.height = singleton.ratiodp(false, 0.29);
        ((TextView) findViewById(R.id.feedback_limit)).setTypeface(singleton.getNanumr());

        send = findViewById(R.id.feedback_send);
        RelativeLayout.LayoutParams sendparam = (RelativeLayout.LayoutParams) send.getLayoutParams();
        sendparam.height = singleton.ratiodp(false, 0.061);
        sendparam.width = singleton.ratiodp(true, 0.62);
        sendparam.bottomMargin = singleton.ratiodp(false, 0.038);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkContents(edit.getText().toString())) {
                    singleton.CustomDialog(FeedbackActivity.this).setTitle("전송").setMessage("내용을 전송할까요?").
                            setPositiveButton("전송", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    try {
                                        PackageInfo pain = getPackageManager().getPackageInfo(getPackageName(), 0);
                                        singleton.getRetroService().feedback(edit.getText().toString(), 0, pain.versionCode).enqueue(new Callback<ResponseBody>() {
                                            @Override
                                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                if (response.isSuccessful()) {
                                                    Toast.makeText(FeedbackActivity.this, "성공적으로 전송하였습니다!", Toast.LENGTH_SHORT).show();
                                                    finish();
                                                } else {
                                                    singleton.Errdialog(FeedbackActivity.this, false, "000").show();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                singleton.Errdialog(FeedbackActivity.this, false, "001").show();
                                            }
                                        });
                                    } catch (PackageManager.NameNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).setNegativeButton("취소", null).show();
                } else {
                    Toast.makeText(FeedbackActivity.this, "짧거나 너무 길어요 다시 작성해 주세요!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        AdView adView = findViewById(R.id.feedback_ad);
        adView.loadAd(new AdRequest.Builder().build());

    }

    private boolean checkContents(String s) {
        if(s.length() < 9 || s.length() > 500) {
            return false;
        } else return true;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.activity_slide_out_right_inner, R.anim.activity_slide_out_right);
    }
}
