package com.clab.classpick

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import com.clab.classpick.Favorite.FavoriteView
import com.clab.classpick.Schedule.TimeTable

/**
 * Created by kyun on 2017. 9. 18..
 */

class MainViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return TimeTable(0)
            1 -> return FavoriteView(0)
        }

        return null
    }

    override fun getCount(): Int {
        return 2
    }
}
