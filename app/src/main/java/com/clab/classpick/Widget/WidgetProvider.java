package com.clab.classpick.Widget;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.clab.classpick.R;
import com.clab.classpick.Schedule.TimeTableItem;
import com.clab.classpick.SplashActivity;
import com.clab.classpick.utils.Singletons.mFunction;
import com.clab.classpick.utils.Singletons.Singleton;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by kyun on 2017. 9. 10..
 */

public class WidgetProvider extends AppWidgetProvider {

    private int ht, wd;
    private Singleton singleton;
    private mFunction mfunction;
    private Context mContext;

    private Bitmap bitmap;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        mContext = context;
        singleton = Singleton.getInstance(context);
        mfunction = mFunction.getInstance(context);
        ht = 1;
        wd = 1;

        int N = appWidgetIds.length;

		/*int[] appWidgetIds holds ids of multiple instance of your widget
		 * meaning you are placing more than one widgets on your homescreen*/
        for (int i = 0; i < N; ++i) {
            do {
                appWidgetManager.updateAppWidget(appWidgetIds[i], new RemoteViews(mContext.getPackageName(), R.layout.widget_table));
                Bundle op = appWidgetManager.getAppWidgetOptions(appWidgetIds[i]);
                ht = op.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT);
                wd = op.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH);
            }
            while (wd <= 1 || ht <= 1);
            RemoteViews remoteViews = setViews();
            Intent intent = new Intent(context, SplashActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            remoteViews.setOnClickPendingIntent(R.id.widget_table_image,pendingIntent);
            appWidgetManager.updateAppWidget(appWidgetIds[i], remoteViews);
        }
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds,R.id.widget_table_image);
        bitmap.recycle();
    }


    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
        mContext = context;
        singleton = Singleton.getInstance(context);
        mfunction = mFunction.getInstance(context);
        ht = 1;
        wd = 1;

        do {
            ht = newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT);
            wd = newOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH);
        } while (ht <= 1 || wd <= 1);

        RemoteViews remoteViews = setViews();
        Intent intent = new Intent(context, SplashActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        remoteViews.setOnClickPendingIntent(R.id.widget_table_image,pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,R.id.widget_table_image);
        bitmap.recycle();
    }


    private RemoteViews setViews() {
        RemoteViews reV = new RemoteViews(mContext.getPackageName(), R.layout.widget_table);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        LinearLayout t = (LinearLayout) inflater.inflate(R.layout.widget_table_view,null);
        List<TimeTableItem> items = mfunction.getMyclassItem();
        Calendar c = Calendar.getInstance();
        int today = c.get(Calendar.DAY_OF_WEEK) - 1;
        LinearLayout timeline = t.findViewById(R.id.widget_table_times);
        int pa = singleton.ratiodp(true,0.009);
        for(int i = 8; i < 23; i++) {
            TextView tx = new TextView(mContext);
            tx.setText(Integer.toString(i));
            tx.setTypeface(singleton.getNanumr());
            tx.setTextSize(8);
            tx.setPadding(pa,0,0,0);
            timeline.addView(tx);
            LinearLayout.LayoutParams txparam = (LinearLayout.LayoutParams) tx.getLayoutParams();
            txparam.width = ViewGroup.LayoutParams.MATCH_PARENT;
            txparam.weight = 1;
        }
        for(int i = 1 ; i < 6 ; i ++) {
            TextView tv = t.findViewById(getId("widget_table_"+i));
            tv.setTypeface(singleton.getBmjua());
            if(i == today) {
                tv.setTextColor(singleton.getColorMain());
                t.findViewById(getId("widget_table_under_"+i)).setBackgroundColor(singleton.getColorMain());
            }
            RelativeLayout mainV = t.findViewById(getId("widget_table_con_"+i));
            TimeTableItem item = items.get(i);
            for (int j = 0; j < item.getCid().length; j++) {
                String title = item.getTitle()[j];
                String room = item.getRoom()[j];
                String stime = item.getStime()[j];
                String etime = item.getEtime()[j];
                int color = item.getColor()[j];

                LinearLayout itemV = (LinearLayout) inflater.inflate(R.layout.ritem_tableitem, null);
                mainV.addView(itemV);
                itemV.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                itemV.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;

                View contentsB = itemV.findViewById(R.id.table_contents_back);
                View conV = itemV.findViewById(R.id.table_contents_view);
                contentsB.setBackgroundColor(singleton.getTableColor(color));
                LinearLayout.LayoutParams topparam = (LinearLayout.LayoutParams) itemV.findViewById(R.id.table_top).getLayoutParams();
                LinearLayout.LayoutParams conparam = (LinearLayout.LayoutParams) conV.getLayoutParams();
                LinearLayout.LayoutParams bottomparam = (LinearLayout.LayoutParams) itemV.findViewById(R.id.table_bottom).getLayoutParams();

                float starttime = singleton.timeTofloat(stime);
                float endtime = singleton.timeTofloat(etime);
                int weightsum = 15;
                itemV.setWeightSum(weightsum);
                topparam.weight = starttime - 8;
                if (endtime < 23) {
                    bottomparam.weight = 23 - endtime;
                    conparam.weight = weightsum - (topparam.weight + bottomparam.weight);
                } else {
                    conparam.weight = weightsum - (topparam.weight);
                }

                if (endtime - starttime < 1.5 && title.length() > 8)
                    title = title.substring(0, 8) + "...";
                TextView contents = itemV.findViewById(R.id.table_contents);
                contents.setTypeface(singleton.getNanumbg());
                contents.setTextSize((float) 6);
                if (!room.equals(""))
                    contents.setText(title + "\n" + room + "\n" + stime + "~" + etime);
                else contents.setText(title + "\n" + stime + "~" + etime);
                contents.setPadding(pa,pa/3,pa,0);

                itemV.bringToFront();
            }
        }
        int wp = singleton.dpTopx(wd);
        int hp = singleton.dpTopx(ht);

        t.measure(View.MeasureSpec.makeMeasureSpec(wp, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(hp, View.MeasureSpec.EXACTLY));
        t.layout(0,0,t.getMeasuredWidth(),t.getMeasuredHeight());

        t.setDrawingCacheEnabled(true);
        //Bitmap bitmap = t.getDrawingCache();
        t.buildDrawingCache();
        bitmap = Bitmap.createBitmap(t.getMeasuredWidth(),t.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        t.draw(canvas);

        reV.setImageViewBitmap(R.id.widget_table_image,bitmap);

        return reV;

    }

    private int getId(String s) {
        return mContext.getResources().getIdentifier(s, "id", "com.clab.classpick");
    }
}
