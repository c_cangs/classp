package com.clab.classpick.Widget;


/**
 * Created by kyun on 2017. 9. 10..
 */

public class Widget44Item {
    private String title,rid,stime,etime;
    private int color;

    public String getTitle() {
        return title;
    }

    public String getRid() {
        return rid;
    }

    public String getStime() {
        return stime;
    }

    public String getEtime() {
        return etime;
    }

    public int getColor() {
        return color;
    }

    public Widget44Item(String title,String rid,String stime,String etime,int color) {
        this.title = title;
        this.rid = rid;
        this.stime = stime;
        this.etime = etime;
        this.color = color;

    }
}
