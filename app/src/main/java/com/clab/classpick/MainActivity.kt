package com.clab.classpick

import android.app.AlarmManager
import android.content.Intent
import android.preference.PreferenceManager
import android.support.v4.app.FragmentActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast

import com.clab.classpick.Schedule.TimeTable
import com.clab.classpick.utils.AlarmReceiver
import com.clab.classpick.utils.Singletons.Singleton
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : FragmentActivity() {

    private var singleton: Singleton? = null

    private var TableFragment: TimeTable? = null

    private var beforePress: Long = 0
    private var tabPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nullv)
        if (singleton == null) singleton = Singleton.getInstance(this)
        val inflater = LayoutInflater.from(this)
        val v = inflater.inflate(R.layout.activity_main, null)
        addContentView(v, LinearLayout.LayoutParams(singleton!!.widthpix, singleton!!.heightpix - singleton!!.statusBarHeightpix))

        if (PreferenceManager.getDefaultSharedPreferences(this).getInt(resources.getString(R.string.ucode), 0) == -3) {
            (main_tablayout.layoutParams as LinearLayout.LayoutParams).weight = 0f
            val adapter = MainNoSchoolPageAdapter(supportFragmentManager)
            main_viewpager!!.adapter = adapter
            main_viewpager!!.overScrollMode = View.OVER_SCROLL_NEVER
        } else {
            val adapter = MainViewPagerAdapter(supportFragmentManager)
            main_viewpager!!.adapter = adapter
            main_tablayout.setViewPager(main_viewpager)
            main_tablayout.setOnTabChangedListner { tabNum -> main_viewpager!!.setCurrentItem(tabNum, true) }
        }
        main_viewpager!!.offscreenPageLimit = 4
        main_viewpager!!.currentItem = 0
        main_viewpager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                tabPosition = position

            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })

        beforePress = System.currentTimeMillis() + 2000
        tabPosition = 0
    }


    override fun onBackPressed() {
        if (TableFragment == null) TableFragment = supportFragmentManager.findFragmentByTag("android:switcher:" + R.id.main_viewpager + ":" + 0) as TimeTable
        if (tabPosition == 0)
            TableFragment!!.onBackPressed()
        else {
            val nowT = System.currentTimeMillis()
            if (nowT - beforePress < 2000) {
                moveTaskToBack(true)
                finish()
                android.os.Process.killProcess(android.os.Process.myPid())
            } else {
                beforePress = nowT
                Toast.makeText(this, "뒤로 키를 한번 더 누르시면 종료됩니다.", Toast.LENGTH_SHORT).show()
            }
        }

    }


}
