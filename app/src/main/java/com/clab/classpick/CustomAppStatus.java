package com.clab.classpick;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import com.clab.classpick.Services.WithOnAppService;
import com.clab.classpick.utils.Observer.AppFrontEnd.AppFrontendPublisher;
import com.google.android.gms.ads.MobileAds;

/**
 * Created by kyun on 2017. 9. 6..
 */

public class CustomAppStatus extends Application {

    private AppStatus mAppStatus = AppStatus.FOREGROUND;

    private Intent timetick;

    @Override
    public void onCreate() {
        super.onCreate();
        MobileAds.initialize(this,"ca-app-pub-1655564333425112~1536640007");
        registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks());
        timetick = new Intent(this,WithOnAppService.class);
    }

    // Get app is foreground
    public AppStatus getAppStatus() {
        return mAppStatus;
    }

    // check if app is return foreground
    public boolean isReturnedForground() {
        return mAppStatus.ordinal() == AppStatus.RETURNED_TO_FOREGROUND.ordinal();
    }

    public boolean isForeground() {
        return mAppStatus.ordinal() == AppStatus.FOREGROUND.ordinal();
    }

    public boolean isBackground() {
        return mAppStatus.ordinal() == AppStatus.BACKGROUND.ordinal();
    }

    public enum AppStatus {
        BACKGROUND, // app is background
        RETURNED_TO_FOREGROUND, // app returned to foreground(or first launch)
        FOREGROUND; // app is foreground
    }

    public void DoAppDestory() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public class MyActivityLifecycleCallbacks implements ActivityLifecycleCallbacks {

        // running activity count
        private int running = 0;

        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityStarted(Activity activity) {
            if (++running == 1) {
                // running activity is 1,
                // app must be returned from background just now (or first launch)
                AppFrontendPublisher.getInstance().notifychange();
                mAppStatus = AppStatus.RETURNED_TO_FOREGROUND;
                if(timetick == null)
                    timetick = new Intent(CustomAppStatus.this,WithOnAppService.class);
                startService(timetick);
            } else if (running > 1) {
                // 2 or more running activities,
                // should be foreground already.
                mAppStatus = AppStatus.FOREGROUND;
            }
        }

        @Override
        public void onActivityResumed(Activity activity) {
        }

        @Override
        public void onActivityPaused(Activity activity) {
        }

        @Override
        public void onActivityStopped(Activity activity) {
            if (--running == 0) {
                // no active activity
                // app goes to background
                mAppStatus = AppStatus.BACKGROUND;
                if(timetick != null) stopService(timetick);
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
        }
    }

}
