package com.clab.classpick.Search;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * Created by kyun on 2017-03-04.
 */

public class SearchRecycleritem implements MultiItemEntity {

    public static final int buildingv = 1, roomv = 2, navv = 3;

    String building,room,time,name,nav;
    int itemtype,count;
    boolean isfree,isfav;

    public String getBuilding() {
        return building;
    }

    public String getRoom() {
        return room;
    }

    public String getTime() {
        return time;
    }

    public String getName() {
        return name;
    }

    public String getNav() {
        return nav;
    }

    public boolean getIsfree() {
        return isfree;
    }

    public boolean getIsfav() {
        return isfav;
    }

    public int getCount() {
        return count;
    }


    public SearchRecycleritem(int i, String n) {
        this.itemtype = i;
        this.nav = n;
    }

    public SearchRecycleritem(int i, String b, int c, boolean v) {
        this.itemtype = i;
        this.building = b;
        this.count = c;
        this.isfav = v;
    }


    public SearchRecycleritem(int i, String b, String r, String t, String n, boolean f, boolean v) {
        this.itemtype = i;
        this.building = b;
        this.room = r;
        this.time = t;
        this.name = n;
        this.isfree = f;
        this.isfav = v;
    }



    @Override
    public int getItemType() {
        return itemtype;
    }
}
