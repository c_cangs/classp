package com.clab.classpick.Search;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.clab.classpick.BuildingInfo.BuildingInfoActivity;
import com.clab.classpick.ClassRoomInfo.ClassRoomInfoActivity;
import com.clab.classpick.R;
import com.clab.classpick.utils.Singletons.Singleton;

import java.util.List;

/**
 * Created by kyun on 2017-03-04.
 */

public class SearchRecyclerAdapter extends BaseMultiItemQuickAdapter<SearchRecycleritem,BaseViewHolder> {

    private Context mContext;
    private Singleton singleton;

    private LinearLayout buildingitem, roomitem;

    private TextView navtxt,building,count,room,time,name;

    private RelativeLayout.LayoutParams buildingparam,navparam,classroomparam;

    private boolean isfreeview;


    public SearchRecyclerAdapter(Context context, List data, boolean isfreeview){
        super(data);
        this.mContext = context;
        this.isfreeview = isfreeview;
        if(singleton == null) singleton = Singleton.getInstance(context);
        addItemType(SearchRecycleritem.buildingv, R.layout.ritem_search_buildingitem);
        addItemType(SearchRecycleritem.roomv, R.layout.ritem_search_classroomitem);
        addItemType(SearchRecycleritem.navv, R.layout.ritem_search_nav);
        buildingparam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,singleton.ratiodp(false,0.081));
        buildingparam.bottomMargin = singleton.ratiodp(false,0.002);
        navparam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,singleton.ratiodp(false,0.043));
        classroomparam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,singleton.ratiodp(false,0.101));
        classroomparam.bottomMargin = singleton.ratiodp(false,0.002);

    }

    @Override
    protected void convert(BaseViewHolder helper, final SearchRecycleritem item) {
        switch (helper.getItemViewType()) {
            case  SearchRecycleritem.buildingv :
                buildingitem = helper.getView(R.id.search_buildingitem);
                buildingitem.setLayoutParams(buildingparam);
                buildingitem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mContext.startActivity(new Intent(mContext, BuildingInfoActivity.class).putExtra("bname",item.getBuilding()).putExtra("isfav",item.getIsfav()));
                        ((Activity) mContext).overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_in_right_inner);
                    }
                });
                building = helper.getView(R.id.search_buildingitem_building);
                building.setText(item.getBuilding());
                building.setTypeface(singleton.getBmjua());
                count = helper.getView(R.id.search_buildingitem_count);
                count.setText(Integer.toString(item.getCount()));
                count.setTypeface(singleton.getBmjua());
                helper.setTypeface(R.id.search_buildingitem_free,singleton.getBmjua());
                break;

            case SearchRecycleritem.roomv :
                roomitem = helper.getView(R.id.search_classitem_view);
                roomitem.setLayoutParams(classroomparam);
                roomitem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mContext.startActivity(new Intent(mContext, ClassRoomInfoActivity.class).putExtra("isfree", item.getIsfree())
                                .putExtra("isfav", item.getIsfav()).putExtra("bname", item.getBuilding()).putExtra("rid", item.getRoom()));
                        ((Activity) mContext).overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_in_right_inner);
                    }
                });
                building = helper.getView(R.id.search_classitem_building);
                room = helper.getView(R.id.search_classitme_room);
                time = helper.getView(R.id.search_classtiem_time);
                name = helper.getView(R.id.search_classitem_classname);
                building.setTypeface(singleton.getNanumb());
                room.setTypeface(singleton.getNanumb());
                time.setTypeface(singleton.getNanumb());
                name.setTypeface(singleton.getNanumb());
                if (item.getIsfree()) {
                    helper.setBackgroundRes(R.id.search_classitem_status, R.drawable.free_card);
                    building.setText(item.getBuilding());
                    room.setText(item.getRoom()+"호");
                    time.setText("~"+item.getTime());
                    name.setVisibility(View.GONE);
                } else {
                    if(!isfreeview) {
                        helper.setBackgroundRes(R.id.search_classitem_status, R.drawable.in_class_card);
                        building.setText(item.getBuilding());
                        room.setText(item.getRoom()+"호");
                        time.setText("~"+item.getTime());
                        name.setText(item.getName());
                    } else roomitem.setVisibility(View.GONE);
                }
                break;

            case SearchRecycleritem.navv :
                helper.getView(R.id.search_navview).setLayoutParams(navparam);
                navtxt = helper.getView(R.id.search_navtxt);
                navtxt.setPadding(singleton.ratiodp(true,0.031),0,0,0);
                navtxt.setTypeface(singleton.getBmjua());
                navtxt.setText(item.getNav());
                break;
        }
    }
}
