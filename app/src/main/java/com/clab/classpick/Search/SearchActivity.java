package com.clab.classpick.Search;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clab.classpick.R;
import com.clab.classpick.utils.Singletons.Singleton;
import com.clab.classpick.utils.Singletons.mFunction;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.List;

import static com.chad.library.adapter.base.BaseQuickAdapter.ALPHAIN;

/**
 * Created by kyun on 2017-02-17.
 */

public class SearchActivity extends Activity {

    private Singleton singleton;
    private mFunction mfunction;

    private RecyclerView searchrecycler;
    private SearchRecyclerAdapter recyclerAdapter;

    private List<SearchRecycleritem> items;

    private boolean isfilterfree = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        if(singleton == null) singleton = Singleton.getInstance(this);
        if(mfunction == null) mfunction = mFunction.getInstance(this);

        final LinearLayout filterview = findViewById(R.id.search_filter);
        final LinearLayout searchresult = findViewById(R.id.search_resultview);
        final InputMethodManager mInputMethodManager = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);

        filterview.setVisibility(View.GONE);

        final ImageView sorry = findViewById(R.id.search_sorry);
        RelativeLayout.LayoutParams sorryparam = (RelativeLayout.LayoutParams) sorry.getLayoutParams();
        sorryparam.width = singleton.ratiodp(true,0.484);
        sorryparam.height = singleton.ratiodp(false,0.12);
        sorryparam.topMargin = singleton.ratiodp(false,0.291);
        sorry.setLayoutParams(sorryparam);
        sorry.setVisibility(View.GONE);


        ImageButton backbtn = findViewById(R.id.search_backbtn);
        LinearLayout.LayoutParams backbtnparam = (LinearLayout.LayoutParams) backbtn.getLayoutParams();
        backbtnparam.height = singleton.ratiodp(false,0.0296);
        backbtn.setLayoutParams(backbtnparam);
        findViewById(R.id.search_backbtn_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        final AutoCompleteTextView searchedit = findViewById(R.id.search_edittext);
        searchedit.setTypeface(singleton.getNanumr());
        searchedit.setThreshold(0);
        searchedit.setDropDownHeight(singleton.ratiodp(false,0.624));
        searchedit.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, mfunction.searchitemlist()));
        searchedit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                searchedit.setText((String)adapterView.getItemAtPosition(i));
                searchedit.dismissDropDown();
                mInputMethodManager.hideSoftInputFromWindow(searchedit.getWindowToken(), 0);
                items = mfunction.searchitems((String)adapterView.getItemAtPosition(i));
                if(items.size() < 3) {
                    sorry.setVisibility(View.VISIBLE);
                    filterview.setVisibility(View.GONE);
                    searchresult.setVisibility(View.INVISIBLE);
                } else {
                    sorry.setVisibility(View.GONE);
                    filterview.setVisibility(View.VISIBLE);
                    searchresult.setVisibility(View.VISIBLE);
                    setview();
                }

            }
        });
        searchedit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                switch (i) {
                    default:
                        searchedit.dismissDropDown();
                        mInputMethodManager.hideSoftInputFromWindow(searchedit.getWindowToken(), 0);
                        items = mfunction.searchitems(searchedit.getText().toString());
                        if(items.size() < 3 ) {
                            sorry.setVisibility(View.VISIBLE);
                            filterview.setVisibility(View.GONE);
                            searchresult.setVisibility(View.INVISIBLE);
                        } else {
                            sorry.setVisibility(View.GONE);
                            filterview.setVisibility(View.VISIBLE);
                            searchresult.setVisibility(View.VISIBLE);
                            setview();
                        }
                        break;
                }
                return true;
            }
        });

        ImageButton searchbtn = findViewById(R.id.search_btn);
        RelativeLayout.LayoutParams searchbtnparam = (RelativeLayout.LayoutParams) searchbtn.getLayoutParams();
        searchbtnparam.width = singleton.ratiodp(true,0.0583);
        searchbtnparam.height = singleton.ratiodp(false,0.0328);
        searchbtnparam.rightMargin = singleton.ratiodp(true,0.0277);
        searchbtn.setLayoutParams(searchbtnparam);
        findViewById(R.id.search_btn_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchedit.dismissDropDown();
                mInputMethodManager.hideSoftInputFromWindow(searchedit.getWindowToken(), 0);
                items = mfunction.searchitems(searchedit.getText().toString());
                if(items.size() < 3) {
                    sorry.setVisibility(View.VISIBLE);
                    filterview.setVisibility(View.GONE);
                    searchresult.setVisibility(View.INVISIBLE);
                } else {
                    sorry.setVisibility(View.GONE);
                    filterview.setVisibility(View.VISIBLE);
                    searchresult.setVisibility(View.VISIBLE);
                    setview();
                }
            }
        });

        TextView cancle = findViewById(R.id.search_cancle);
        cancle.setTypeface(singleton.getBmjua());
        findViewById(R.id.search_cancle_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchedit.clearListSelection();
                searchedit.setText("");
                mInputMethodManager.hideSoftInputFromWindow(searchedit.getWindowToken(), 0);

            }
        });

        final TextView freetxt = findViewById(R.id.search_free_text);
        final TextView alltxt = findViewById(R.id.search_all_text);

        freetxt.setTypeface(singleton.getBmjua());
        alltxt.setTypeface(singleton.getBmjua());

        final RelativeLayout filterfree = findViewById(R.id.search_free);
        final RelativeLayout filterall = findViewById(R.id.search_all);

        filterfree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isfilterfree) {
                    filterfree.setBackgroundResource(R.drawable.right_on);
                    filterall.setBackgroundResource(R.drawable.left_off);
                    freetxt.setTextColor(getResources().getColor(R.color.white));
                    alltxt.setTextColor(getResources().getColor(R.color.mainColor));
                    isfilterfree = true;
                    setview();
                }
            }
        });


        filterall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isfilterfree) {
                    filterfree.setBackgroundResource(R.drawable.right_off);
                    filterall.setBackgroundResource(R.drawable.left_on);
                    freetxt.setTextColor(getResources().getColor(R.color.mainColor));
                    alltxt.setTextColor(getResources().getColor(R.color.white));
                    isfilterfree = false;
                    setview();
                }
            }
        });

        searchrecycler = findViewById(R.id.search_recycler);
        searchrecycler.setNestedScrollingEnabled(false);
        searchrecycler.setItemViewCacheSize(10);
        searchrecycler.setDrawingCacheEnabled(true);
        searchrecycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        searchrecycler.setLayoutManager(new LinearLayoutManager(this));

        AdView mAdiVew = findViewById(R.id.search_adView);
        mAdiVew.loadAd(new AdRequest.Builder().build());

    }

    private void setview() {
        recyclerAdapter = new SearchRecyclerAdapter(this,items,isfilterfree);
        recyclerAdapter.openLoadAnimation(ALPHAIN);
        recyclerAdapter.setNotDoAnimationCount(3);
        recyclerAdapter.isFirstOnly(false);
        searchrecycler.setAdapter(recyclerAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(singleton == null) singleton = Singleton.getInstance(this);
        if(mfunction == null) mfunction = mFunction.getInstance(this);

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.activity_slide_out_right_inner, R.anim.activity_slide_out_right);
    }
}
