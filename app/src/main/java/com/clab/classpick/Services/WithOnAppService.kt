package com.clab.classpick.Services

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import com.clab.classpick.utils.AlarmReceiver

import com.clab.classpick.utils.Observer.TimeTick.TimeTickPublisher


/**
 * Created by kyun on 2017. 8. 1..
 */

class WithOnAppService : Service() {

    private var timeBoadcast: BroadcastReceiver? = null

    private var tickPublisher: TimeTickPublisher? = null

    override fun onCreate() {
        super.onCreate()
        if (tickPublisher == null) tickPublisher = TimeTickPublisher.getInstance()
        timeBoadcast = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == Intent.ACTION_DATE_CHANGED) {
                    tickPublisher!!.notifychange(true)
                } else if (intent.action == Intent.ACTION_TIME_TICK) {
                    //                        int m = Calendar.getInstance().get(Calendar.MINUTE);
                    //                        if (m == 0) TimeTickPublisher.getInstance().notifychange();
                    //Toast.makeText(context,"tick",Toast.LENGTH_SHORT).show();
                    tickPublisher!!.notifychange(false)
                }
            }
        }
        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_TIME_TICK)
        intentFilter.addAction(Intent.ACTION_DATE_CHANGED)
        registerReceiver(timeBoadcast, intentFilter)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(timeBoadcast)
    }
}