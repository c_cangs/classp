package com.clab.classpick.ClassRoomInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clab.classpick.R;
import com.clab.classpick.utils.Observer.AppFrontEnd.AppFrontendPublisher;
import com.clab.classpick.utils.Observer.AppFrontEnd.OnAppFrontendObserv;
import com.clab.classpick.utils.Observer.Favorite.FavoritePublisher;
import com.clab.classpick.utils.Observer.Favorite.OnFavoriteChangeRoomObserv;
import com.clab.classpick.utils.Observer.TimeTick.OnTimeTickObserv;
import com.clab.classpick.utils.Observer.TimeTick.TimeTickPublisher;
import com.clab.classpick.utils.Realm.mClass;
import com.clab.classpick.utils.Realm.mClasstable;
import com.clab.classpick.utils.Singletons.Singleton;
import com.clab.classpick.utils.Singletons.mFunction;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Calendar;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by kyun on 2017-04-11.
 */

//todo 파랑점 만들기
public class ClassRoomInfoActivity extends Activity implements OnFavoriteChangeRoomObserv,OnTimeTickObserv,OnAppFrontendObserv {

    private Singleton singleton;
    private mFunction mfunction;

    private AlertDialog.Builder dialog;

    private boolean isfav,beforenow,isinfodown;

    private String sbname,srid,beforeitem;

    private int infoHeight;

    private Button favorite;

    private LinearLayout info;

    private RelativeLayout.LayoutParams infoparam;

    private TextView infoname, infotime, infoprof;

    private View beforeview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classroominfo);
        TimeTickPublisher.getInstance().addlistener(this);
        AppFrontendPublisher.getInstance().addlistener(this);
        if(singleton == null) singleton = Singleton.getInstance(this);
        if(mfunction == null) mfunction = mFunction.getInstance(this);
        dialog = singleton.CustomDialog(this);

        boolean isfree = getIntent().getBooleanExtra("isfree",true);
        isfav = getIntent().getBooleanExtra("isfav",false);
        sbname = getIntent().getStringExtra("bname");
        srid = getIntent().getStringExtra("rid");

        beforenow = false;
        isinfodown = true;
        beforeitem = "";
        infoHeight = 0;

        findViewById(R.id.classroominfo_background).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//배경 클릭시 인포 내림
                if(!beforeitem.equals("") && !isinfodown) {
                    if(!beforenow)
                        beforeview.setBackgroundResource(R.drawable.table_content_blue);
                    else
                        beforeview.setBackgroundResource(R.drawable.table_content_yello);
                    isinfodown = true;
                    info.animate().translationY(infoHeight).withLayer()
                            .setInterpolator(AnimationUtils.loadInterpolator(ClassRoomInfoActivity.this,singleton.getAnimaccelerate())).setDuration(250).start();
                }
            }
        });

        ImageView status = findViewById(R.id.classroominfo_status);
        TextView bname = findViewById(R.id.classroominfo_bname);
        TextView rid = findViewById(R.id.classroominfo_rid);
        favorite = findViewById(R.id.classroominfo_favorite);
//        Button loca = findViewById(R.id.classroominfo_loca);
//        Button share = findViewById(R.id.classroominfo_share);
        Button back = findViewById(R.id.classroominfo_back);
        LinearLayout.LayoutParams backparam = (LinearLayout.LayoutParams) back.getLayoutParams();
        backparam.height = singleton.ratiodp(false,0.0296);
        findViewById(R.id.classroominfo_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView) findViewById(R.id.classroominfo_tabtxt)).setTypeface(singleton.getBmjua());

        if(isfree) status.setBackgroundResource(R.drawable.free_card);
        else status.setBackgroundResource(R.drawable.in_class_card);

        if(isfav) favorite.setBackgroundResource(R.drawable.favorite_on);
        else favorite.setBackgroundResource(R.drawable.favorite_off);

        ((RelativeLayout.LayoutParams) bname.getLayoutParams()).topMargin = singleton.ratiodp(false,0.01);
        bname.setTypeface(singleton.getBmjua());
        bname.setText(sbname);

        rid.setTypeface(singleton.getBmjua());
        rid.setText(srid);

        RelativeLayout.LayoutParams favoriteparam = (RelativeLayout.LayoutParams) favorite.getLayoutParams();
        favoriteparam.width = singleton.ratiodp(true,0.069);
        favoriteparam.height = singleton.ratiodp(false,0.037);

//        RelativeLayout.LayoutParams locaparam = (RelativeLayout.LayoutParams) loca.getLayoutParams();
//        locaparam.leftMargin = singleton.ratiodp(true,0.039);
//        locaparam.width = singleton.ratiodp(true,0.048);
//        locaparam.height = singleton.ratiodp(false,0.037);
//
//        RelativeLayout.LayoutParams shareparam = (RelativeLayout.LayoutParams) share.getLayoutParams();
//        shareparam.leftMargin = singleton.ratiodp(true,0.039);
//        shareparam.width = singleton.ratiodp(true,0.052);
//        shareparam.height = singleton.ratiodp(false,0.033);

        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isfav) {
                    dialog.setTitle("즐겨찾기 삭제").setMessage("즐겨찾기에 삭제하시겠습니까?").setPositiveButton("네", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mfunction.FavoriteUpdate(ClassRoomInfoActivity.this, false, sbname, srid);
                        }
                    }).setNegativeButton("아니요",null).show();
                } else {
                    dialog.setTitle("즐겨찾기 추가").setMessage("즐겨찾기에 추가하시겠습니까?").setPositiveButton("네", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mfunction.FavoriteUpdate(ClassRoomInfoActivity.this, true, sbname, srid);
                        }
                    }).setNegativeButton("아니요",null).show();
                }
            }
        });


        info = findViewById(R.id.cmi_info);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isinfodown) {
                    if(beforenow)
                        beforeview.setBackgroundResource(R.drawable.table_content_yello_select);
                    else
                        beforeview.setBackgroundResource(R.drawable.table_content_blue_select);
                    info.animate().translationY(infoHeight).withLayer()
                            .setInterpolator(AnimationUtils.loadInterpolator(ClassRoomInfoActivity.this, singleton.getAnimaccelerate())).setDuration(250).start();
                    isinfodown = true;
                }
            }
        });
        infoname = findViewById(R.id.cmi_info_name);
        infotime = findViewById(R.id.cmi_info_time);
        infoprof = findViewById(R.id.cmi_info_prof);
        infoname.setTypeface(singleton.getNanumb());
        infotime.setTypeface(singleton.getNanumb());
        infoprof.setTypeface(singleton.getNanumb());
        infoparam = (RelativeLayout.LayoutParams) info.getLayoutParams();
        infoparam.width = singleton.ratiodp(true,0.916);
        infoparam.height = infoHeight = singleton.ratiodp(false,0.172);
        info.setLayoutParams(infoparam);
        info.setY(infoHeight);

        setView();

        AdView mAdiVew = findViewById(R.id.classroominfo_adView);
        mAdiVew.loadAd(new AdRequest.Builder().build());
    }

    private void setView() {
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
        LinearLayout cons =  findViewById(R.id.cmi_con);
        cons.removeAllViews();
        for(int i = 1; i < 6;i++) {
            View v = SetContents(i);
            cons.addView(v);
            LinearLayout.LayoutParams vparam = (LinearLayout.LayoutParams) v.getLayoutParams();
            vparam.weight = 1;
            vparam.width = 0;
            vparam.height = ViewGroup.LayoutParams.MATCH_PARENT;
            TextView tx  = findViewById(getResources().getIdentifier("id/cmi_txt_" + Integer.toString(i), "id", "com.clab.classpick"));
            tx.setTypeface(singleton.getNanumb());
            if(i == day) {
                findViewById(getResources().getIdentifier("id/cmi_txtb_" + Integer.toString(i), "id", "com.clab.classpick")).setBackgroundColor(singleton.getColorMain());
                tx.setTextColor(singleton.getColorWhite());
            }
        }
        RelativeLayout.LayoutParams timelistparam = (RelativeLayout.LayoutParams) ((LinearLayout)findViewById(R.id.cmi_time_list)).getLayoutParams();
        timelistparam.leftMargin = singleton.ratiodp(true,0.012);
        for(int i = 9; i < 23 ; i++)
            ((TextView) findViewById(getResources().getIdentifier("id/cmi_time_text_" + Integer.toString(i), "id", "com.clab.classpick"))).setTypeface(singleton.getNanumb());

    }

    private View SetContents (final int day) {
        Calendar c = Calendar.getInstance();
        final RelativeLayout v  = (RelativeLayout) getLayoutInflater().inflate(R.layout.ritem_timetable,null);

        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mClasstable> tableraw = realm.where(mClasstable.class).equalTo("bname",sbname).equalTo("rid",srid).equalTo("cday",day).findAll();
        final float nowtime = c.get(Calendar.HOUR_OF_DAY) + (float)c.get(Calendar.MINUTE)/60;
        final int today = c.get(Calendar.DAY_OF_WEEK) - 1;

        for(mClasstable m : tableraw) {
            LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_table_item, null);
            v.addView(layout);
            layout.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            layout.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

            LinearLayout middle = layout.findViewById(R.id.table_item_middle);
            middle.getLayoutParams().width = singleton.ratiodp(true, 0.08);
            final ImageView image = middle.findViewById(R.id.table_item_contents);
            image.getLayoutParams().width = singleton.ratiodp(true, 0.028);
            image.bringToFront();

            LinearLayout.LayoutParams middleparam = (LinearLayout.LayoutParams) middle.getLayoutParams();
            LinearLayout.LayoutParams topparam = (LinearLayout.LayoutParams) layout.findViewById(R.id.table_item_top).getLayoutParams();
            LinearLayout.LayoutParams bottomparam = (LinearLayout.LayoutParams) layout.findViewById(R.id.table_item_bottom).getLayoutParams();

            String cid = m.getCid();
            mClass mc = realm.where(mClass.class).equalTo("cid",cid).findFirst();
            final String cname = mc.getCname();
            final String prof = mc.getProf();
            String stime = mfunction.getUnivscheduleS()[m.getSperiod()][0];
            String etime = mfunction.getUnivscheduleS()[m.getEperiod()][1];
            final String alltime = stime + " ~ " + etime;

            final String thisitem = cid+day;

            final float starttime = mfunction.getUnivscheduleF()[m.getSperiod()][0];
            final float endtime = mfunction.getUnivscheduleF()[m.getEperiod()][1];
            int weightsum = 14;

            layout.setWeightSum(weightsum);
            topparam.weight = starttime - 9;
            bottomparam.weight = 23 - endtime;
            middleparam.weight = weightsum - (topparam.weight + bottomparam.weight);

            if (today == day && starttime <= nowtime && endtime >= nowtime) {
                //지금 시간일때
                if(beforeitem.equals("")) {
                    image.setBackgroundResource(R.drawable.table_content_yello_select);
                    beforenow = true;
                    beforeitem = thisitem;
                    beforeview = image;
                    setInfo(cname, alltime, prof);
                } else if(beforeitem.equals(thisitem) && !isinfodown) {
                    image.setBackgroundResource(R.drawable.table_content_yello_select);
                    beforeview = image;
                }
                else {
                    image.setBackgroundResource(R.drawable.table_content_yello);
                }
            } else if(beforeitem.equals(thisitem) && !isinfodown) {
                image.setBackgroundResource(R.drawable.table_content_blue_select);
                beforeview = image;
                beforenow = false;
                setInfo(cname,alltime,prof);
            }
            else {
                image.setBackgroundResource(R.drawable.table_content_blue);
            }

            middle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (beforeitem.equals("")) {
                        image.setBackgroundResource(R.drawable.table_content_blue_select);
                        beforeitem = thisitem;
                        beforeview = image;
                        setInfo(cname, alltime, prof);
                    } else if (!beforeitem.equals(thisitem)) {
                        if (beforenow)
                            beforeview.setBackgroundResource(R.drawable.table_content_yello);
                        else
                            beforeview.setBackgroundResource(R.drawable.table_content_blue);
                        if (today == day && starttime <= nowtime && endtime >= nowtime) {
                            image.setBackgroundResource(R.drawable.table_content_yello_select);
                            beforenow = true;
                        } else {
                            image.setBackgroundResource(R.drawable.table_content_blue_select);
                            beforenow = false;
                        }
                        beforeitem = thisitem;
                        beforeview = image;
                        info.setTranslationY(infoHeight);
                        setInfo(cname, alltime, prof);
                    } else if(isinfodown) {
                        if (today == day && starttime <= nowtime && endtime >= nowtime) {
                            image.setBackgroundResource(R.drawable.table_content_yello_select);
                            beforenow = true;
                        } else {
                            image.setBackgroundResource(R.drawable.table_content_blue_select);
                            beforenow = false;
                        }
                        isinfodown = false;
                        info.animate().translationY(0).withLayer()
                                .setInterpolator(AnimationUtils.loadInterpolator(ClassRoomInfoActivity.this,singleton.getAnimaccelerate())).setDuration(250).start();
                    }
                }
            });
        }

        if(day == today && nowtime < 23 && nowtime >= 9) { //
            LinearLayout layout = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_table_item, null);
            v.addView(layout);
            RelativeLayout.LayoutParams dotparam = (RelativeLayout.LayoutParams) layout.getLayoutParams();
            dotparam.width = ViewGroup.LayoutParams.MATCH_PARENT;
            dotparam.height = ViewGroup.LayoutParams.MATCH_PARENT;
            View image = layout.findViewById(R.id.table_item_middle);
            image.setBackgroundResource(R.drawable.table_dot);
            image.getLayoutParams().height = singleton.ratiodp(false,0.01578);
            image.getLayoutParams().width = singleton.ratiodp(true,0.028);
            LinearLayout.LayoutParams topparam = (LinearLayout.LayoutParams) layout.findViewById(R.id.table_item_top).getLayoutParams();
            LinearLayout.LayoutParams bottomparam = (LinearLayout.LayoutParams) layout.findViewById(R.id.table_item_bottom).getLayoutParams();
            topparam.weight = topW(nowtime);
            bottomparam.weight = 14 - topparam.weight;
        }

        return v;
    }


    private float topW(float T) {
        return T - (9 - (float) ((T - 9) * (0.4/14)));
    }

    private void setInfo (String name, String time, String prof) {
        infoname.setText(name);
        infotime.setText(time);
        infoprof.setText(prof + " 교수");
        isinfodown = false;
        if (beforenow) {
            infoname.setTextColor(singleton.getColorDarkGray());
            infotime.setTextColor(singleton.getColorDarkGray());
            infoprof.setTextColor(singleton.getColorDarkGray());
            info.setBackgroundResource(R.drawable.tableinfo_yello);
        }
        else {
            infoname.setTextColor(singleton.getColorWhite());
            infotime.setTextColor(singleton.getColorWhite());
            infoprof.setTextColor(singleton.getColorWhite());
            info.setBackgroundResource(R.drawable.tableinfo_blue);
        }
        info.animate().translationY(0).withLayer()
                .setInterpolator(AnimationUtils.loadInterpolator(this, singleton.getAnimaccelerate())).setDuration(250).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(singleton == null) singleton = Singleton.getInstance(this);
        if(mfunction == null) mfunction = mFunction.getInstance(this);
        dialog = singleton.CustomDialog(this);
        FavoritePublisher.getInstance().addroom(this);
    }

    @Override
    public void onBackPressed() {
        if(!beforeitem.equals("") && !isinfodown) {
            if(!beforenow)
                beforeview.setBackgroundResource(R.drawable.table_content_blue);
            else
                beforeview.setBackgroundResource(R.drawable.table_content_yello);
            isinfodown = true;
            info.animate().translationY(infoHeight).withLayer()
                    .setInterpolator(AnimationUtils.loadInterpolator(ClassRoomInfoActivity.this,singleton.getAnimaccelerate())).setDuration(250).start();
        } else {
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.activity_slide_out_right_inner, R.anim.activity_slide_out_right);
    }

    @Override
    public void FavoriteRoomChanged() {
        if(isfav) {
            isfav = false;
            favorite.setBackgroundResource(R.drawable.favorite_off);
        } else {
            isfav = true;
            favorite.setBackgroundResource(R.drawable.favorite_on);
        }
    }

    @Override
    public void OnTimeTick(boolean isDateChange) {
        if(Calendar.getInstance().get(Calendar.MINUTE)%5 == 0)
            setView();
    }

    @Override
    public void OnAppFrontend() {
        setView();
    }
}
