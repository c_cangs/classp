package com.clab.classpick.Schedule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clab.classpick.FeedbackActivity;
import com.clab.classpick.R;
import com.clab.classpick.Widget.WidgetProvider;
import com.clab.classpick.utils.AlarmReceiver;
import com.clab.classpick.utils.Observer.AppFrontEnd.AppFrontendPublisher;
import com.clab.classpick.utils.Observer.AppFrontEnd.OnAppFrontendObserv;
import com.clab.classpick.utils.Observer.TimeTick.OnTimeTickObserv;
import com.clab.classpick.utils.Observer.TimeTick.TimeTickPublisher;
import com.clab.classpick.utils.Realm.mClass;
import com.clab.classpick.utils.Realm.mCustomtable;
import com.clab.classpick.utils.Singletons.mFunction;
import com.clab.classpick.utils.Singletons.Singleton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by kyun on 2017. 9. 7..
 */

@SuppressLint("ValidFragment")
public class TimeTable extends Fragment implements OnTimeTickObserv,OnAppFrontendObserv {

    private Singleton singleton;
    private mFunction mfunction;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private LayoutInflater inflater;

    private AlertDialog loading;

    private RelativeLayout ContentV;
    private View addV;
    private EditText input;
    private RecyclerView TableContents;
    private TimeTableRecyclerAdapter timeTableRecyclerAdapter;

    private RelativeLayout newitemV;
    private LinearLayout searchV;
    private RelativeLayout customV;
    private RelativeLayout customInputV;
    private RecyclerView itemList;
    private ClassListRecyclerAdapter classListRecyclerAdapter;
    private TextView customName,customRoom,customSave;
    private InputMethodManager im;
    private List<RelativeLayout> npVs;
    private int d,sh,sm,eh,em;

    private String[] cids,points,types,departements,cnames,profs,grades;
    private String PositionCid;
    private long beforePress;
    private boolean isInputing,isAlting,isOnkeyboard,isOnItem,isSearch,isFrontend,isAlterCustom,isNoSchool;
    private int newVHeight,infoVHeight,adHeight,tabHeight;

    private RelativeLayout itemInfo;
    private TextView Info1,Info2, Info3;

    public TimeTable() {}

    public TimeTable(int i) {}

    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(singleton == null) singleton = Singleton.getInstance(mContext);
        if(mfunction == null) mfunction = mFunction.getInstance(mContext);
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = preferences.edit();
        TimeTickPublisher.getInstance().addlistener(this);
        AppFrontendPublisher.getInstance().addlistener(this);
        loading = singleton.Loading(mContext);
        isInputing = false;
        isAlting = false;
        isOnkeyboard = false;
        isOnItem = false;
        isSearch = true;
        isFrontend = false;
        isAlterCustom = false;
        if(preferences.getInt(getResources().getString(R.string.ucode),0) == -3) {
            isNoSchool = true;
            isSearch = false;
        }
        else isNoSchool = false;
        Realm realm = Realm.getInstance(singleton.getmConfig());
        RealmResults<mClass> classes = realm.where(mClass.class).findAll();
        int size = classes.size();
        cids = new String[size];
        points = new String[size];
        types = new String[size];
        departements = new String[size];
        cnames = new String[size];
        profs = new String[size];
        grades = new String[size];
        for(int i = 0 ; i < size ; i ++) {
            mClass m = classes.get(i);
            cids[i] = m.getCid();
            points[i] = m.getPoint() + "학점";
            types[i] = m.getType();
            departements[i] = m.getDepartment();
            cnames[i] = m.getCname();
            profs[i] = m.getProf();
            switch (m.getGrade()) {
                case 0: grades[i] = "전체학년"; break;
                case 1: grades[i] = "1학년"; break;
                case 2: grades[i] = "2학년"; break;
                case 3: grades[i] = "3학년"; break;
                case 4: grades[i] = "4학년"; break;
            }
        }
        beforePress = System.currentTimeMillis()+2000;
        newVHeight = singleton.ratiodp(false,0.375);
        infoVHeight = singleton.ratiodp(false,0.175);
        adHeight = 0;
        tabHeight = 0;
        PositionCid = "";
        npVs = new ArrayList<>();
        d = 1;
        sh = 8;
        sm = 0;
        eh = 8;
        em = 0;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ContentV = (RelativeLayout) inflater.inflate(R.layout.fragment_timetable,null);
        this.inflater = inflater;
        if(!PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(mContext.getResources().getString(R.string.first),false)) {
            final View firstV = inflater.inflate(R.layout.layout_first_use,null);
            ((RelativeLayout)((Activity)mContext).findViewById(R.id.main)).addView(firstV);
            firstV.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
            firstV.bringToFront();
            firstV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.animate().alpha(0).setDuration(250).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                            editor.putBoolean(mContext.getResources().getString(R.string.first),true);
                            editor.commit();
                            ((RelativeLayout)((Activity)mContext).findViewById(R.id.main)).removeView(firstV);
                        }
                    }).start();
                }
            });
            View plus = firstV.findViewById(R.id.first_add);
            RelativeLayout.LayoutParams plusparam = (RelativeLayout.LayoutParams) plus.getLayoutParams();
            plusparam.height = singleton.ratiodp(false,0.024);
            plusparam.width = singleton.ratiodp(true,0.043);
            plusparam.rightMargin = singleton.ratiodp(true,0.04);
            plusparam.topMargin = singleton.ratiodp(false,0.021);
            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    firstV.animate().alpha(0).setDuration(250).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                            editor.putBoolean(mContext.getResources().getString(R.string.first),true);
                            editor.commit();
                            ((RelativeLayout)((Activity)mContext).findViewById(R.id.main)).removeView(firstV);
                            ControlnewitemV();
                        }
                    }).start();
                }
            });
            View direct = firstV.findViewById(R.id.first_direct);
            RelativeLayout.LayoutParams directparam = (RelativeLayout.LayoutParams) direct.getLayoutParams();
            directparam.height = singleton.ratiodp(false,0.076);
            directparam.width = singleton.ratiodp(true,0.139);
            directparam.rightMargin = singleton.ratiodp(true,0.09);
            View info = firstV.findViewById(R.id.first_info);
            RelativeLayout.LayoutParams infoparam = (RelativeLayout.LayoutParams) info.getLayoutParams();
            infoparam.height = singleton.ratiodp(false,0.078);
            infoparam.width = singleton.ratiodp(true,0.358);
            infoparam.rightMargin = singleton.ratiodp(true,0.071);
            Button start = firstV.findViewById(R.id.first_start);
            RelativeLayout.LayoutParams startparam = (RelativeLayout.LayoutParams) start.getLayoutParams();
            startparam.width = singleton.ratiodp(true,0.653);
            startparam.height = singleton.ratiodp(false,0.08);
            startparam.bottomMargin = singleton.ratiodp(false,0.124);
            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    firstV.animate().alpha(0).setDuration(250).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                            editor.putBoolean(mContext.getResources().getString(R.string.first),true);
                            editor.commit();
                            ((RelativeLayout)((Activity)mContext).findViewById(R.id.main)).removeView(firstV);
                            ControlnewitemV();
                        }
                    }).start();
                }
            });
        }
        final Window mRootWindow = ((Activity)mContext).getWindow();
        ContentV.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(){
            public void onGlobalLayout(){
                int screenHeight = ContentV.getRootView().getHeight();
                Rect r = new Rect();
                View view = mRootWindow.getDecorView();
                view.getWindowVisibleDisplayFrame(r);

                int keyBoardHeight = screenHeight - r.bottom;
                if(keyBoardHeight > 100) isOnkeyboard = true;
                else isOnkeyboard = false;

                if(adHeight == 0) adHeight = ContentV.findViewById(R.id.timetable_adView).getHeight();
                if(tabHeight == 0) tabHeight = singleton.getHeightpix() - singleton.getStatusBarHeightpix() - ContentV.getHeight();

                if(isInputing) {
                    if(isOnkeyboard) {
                        newitemV.animate().translationY(-keyBoardHeight + adHeight + tabHeight).withLayer()
                                .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).setDuration(250).start();
                    } else {
                        newitemV.animate().translationY(0).withLayer()
                                .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).setDuration(250).start();
                    }
                }
            }
        });
        im = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);

        View logo = ContentV.findViewById(R.id.timetable_logo);
        RelativeLayout.LayoutParams logoparam = (RelativeLayout.LayoutParams) logo.getLayoutParams();
        logoparam.width = singleton.ratiodp(true,0.3875);
        logoparam.height = singleton.ratiodp(false,0.0351);
        addV = ContentV.findViewById(R.id.timetable_add_view);
        RelativeLayout.LayoutParams addparam = (RelativeLayout.LayoutParams) addV.getLayoutParams();
        addparam.width = singleton.ratiodp(true,0.043);
        addparam.height = singleton.ratiodp(false,0.024);
        addparam.rightMargin = singleton.ratiodp(true,0.04);
        View feedback = ContentV.findViewById(R.id.timetable_feeedback_view);
        RelativeLayout.LayoutParams feedbackparam = (RelativeLayout.LayoutParams) feedback.getLayoutParams();
        feedbackparam.width = singleton.ratiodp(true,0.057);
        feedbackparam.height = singleton.ratiodp(false,0.041);
        feedbackparam.leftMargin = singleton.ratiodp(true,0.04);

        ContentV.findViewById(R.id.timetable_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ControlnewitemV();
            }
        });
        ContentV.findViewById(R.id.timetable_feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, FeedbackActivity.class));
                ((Activity)mContext).overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_in_right_inner);
            }
        });

        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_WEEK) - 1;

        for(int i = 1; i < 6 ; i ++) {
            TextView tx = ContentV.findViewById(mContext.getResources().getIdentifier("id/timetable_" + Integer.toString(i), "id", "com.clab.classpick"));
            tx.setTypeface(singleton.getBmjua());
            if(day == i) {
                tx.setTextColor(singleton.getColorMain());
                ContentV.findViewById(mContext.getResources().getIdentifier("id/timetable_under_" + Integer.toString(i), "id", "com.clab.classpick")).setBackgroundColor(singleton.getColorMain());
            }
        }

        TableContents = ContentV.findViewById(R.id.timetable_contents);
        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        TableContents.setLayoutManager(manager);

        itemInfo = ContentV.findViewById(R.id.timetable_table_info);
        ViewGroup.LayoutParams itemInfoparam = itemInfo.getLayoutParams();
        itemInfoparam.width = singleton.ratiodp(true,0.92);
        itemInfoparam.height = infoVHeight;
        itemInfo.setY(infoVHeight);
        itemInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        View downArrow = itemInfo.findViewById(R.id.light_blue_card_down);
        downArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeInfoV();
            }
        });
        ViewGroup.LayoutParams downArrowparam = downArrow.getLayoutParams();
        downArrowparam.width = singleton.ratiodp(true,0.05);
        downArrowparam.height = singleton.ratiodp(false,0.016);
        TextView delete = itemInfo.findViewById(R.id.light_blue_card_delete);
        delete.setTypeface(singleton.getBmjua());
        RelativeLayout.LayoutParams deleteparam = (RelativeLayout.LayoutParams) delete.getLayoutParams();
        deleteparam.rightMargin = singleton.ratiodp(true,0.0587);
        deleteparam.topMargin = singleton.ratiodp(false,0.017);
        itemInfo.findViewById(R.id.light_blue_card_delete_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //선택뷰삭제
                singleton.CustomDialog(mContext).setTitle("일정 삭제").setMessage("일정을 삭제하시겠습니까?")
                        .setPositiveButton("삭제", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mfunction.AlterMyclass(mContext,PositionCid);
                        classListRecyclerAdapter.change(PositionCid,false);
                        setTalbeView();
                        closeInfoV();
                    }
                }).setNegativeButton("취소",null).show();
            }
        });
        TextView alter = itemInfo.findViewById(R.id.light_blue_card_alter);
        alter.setTypeface(singleton.getBmjua());
        RelativeLayout.LayoutParams alterparam = (RelativeLayout.LayoutParams) alter.getLayoutParams();
        alterparam.rightMargin = singleton.ratiodp(true,0.044);

        itemInfo.findViewById(R.id.light_blue_card_alter_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Realm.getInstance(singleton.getmConfig()).where(mCustomtable.class).equalTo("cid", PositionCid).findAll().size() == 0) {
                    if(!isAlting) {
                        loading.show();
                        isAlting = true;
                        isSearch = true;
                        ControlnewitemV();
                        classListRecyclerAdapter = new ClassListRecyclerAdapter(mContext, setClassItems(""));
                        itemList.setAdapter(classListRecyclerAdapter);
                        input.setText("");
                        for (int i = 0; i < classListRecyclerAdapter.getData().size(); i++) {
                            ClassListItem m = classListRecyclerAdapter.getItem(i);
                            if (m.getCid().equals(PositionCid)) {
                                final int fi = i;
                                if (i > classListRecyclerAdapter.getData().size() - 20) {
                                    itemList.scrollToPosition(i - classListRecyclerAdapter.getData().size() + 1);
                                    new Handler() {
                                        @Override
                                        public void handleMessage(Message msg) {
                                            super.handleMessage(msg);
                                            itemList.smoothScrollToPosition(fi);
                                        }
                                    }.sendEmptyMessageDelayed(0, 100);
                                } else {
                                    itemList.scrollToPosition(i + 20);
                                    new Handler() {
                                        @Override
                                        public void handleMessage(Message msg) {
                                            super.handleMessage(msg);
                                            itemList.smoothScrollToPosition(fi);
                                        }
                                    }.sendEmptyMessageDelayed(0, 100);
                                }
                                break;
                            }
                        }
                    }
                } else {
                    isSearch = false;
                    isAlterCustom = true;
                    customSave.setText("수정");
                    ControlnewitemV();
                    List<TimeTableItem> ms = timeTableRecyclerAdapter.getData();
                    boolean p = false;
                    for (TimeTableItem m : ms) {
                        if (p) {
                            for (int i = 0; i < m.getCid().length; i++) {
                                if (m.getCid()[i].equals(PositionCid)) {
                                    RelativeLayout nv = npVs.get(0);
                                    float stime = singleton.timeTofloat(m.getStime()[i]);
                                    float etime = singleton.timeTofloat(m.getEtime()[i]);
                                    d = m.getDay();
                                    int shour = sh = (int) stime;
                                    int smin = sm = (int) (60 * (stime - shour));
                                    int ehour = eh = (int) etime;
                                    int emin = em = (int) (60 * (etime - ehour));
                                    ((NumberPicker) nv.findViewById(R.id.timetable_custom_time_day)).setValue(m.getDay());
                                    ((NumberPicker) nv.findViewById(R.id.timetable_custom_time_shour)).setValue(shour);
                                    ((NumberPicker) nv.findViewById(R.id.timetable_custom_time_smin)).setValue(smin);
                                    ((NumberPicker) nv.findViewById(R.id.timetable_custom_time_ehour)).setValue(ehour);
                                    ((NumberPicker) nv.findViewById(R.id.timetable_custom_time_emin)).setValue(emin);
                                    ((EditText) customInputV.findViewById(R.id.timetable_custom_name)).setText(m.getTitle()[i]);
                                    ((EditText) customInputV.findViewById(R.id.timetable_custom_place)).setText(m.getRoom()[i]);
                                }
                            }
                        }
                        p = true;
                    }
                }
            }
        });
        Info1 = itemInfo.findViewById(R.id.light_blue_card_contents1);
        Info1.setTypeface(singleton.getNanumb());
        RelativeLayout.LayoutParams Info1param = (RelativeLayout.LayoutParams) Info1.getLayoutParams();
        Info1param.leftMargin = singleton.ratiodp(true,0.0587);
        Info2 = itemInfo.findViewById(R.id.light_blue_card_contents2);
        Info2.setTypeface(singleton.getNanumb());
        RelativeLayout.LayoutParams Info2param = (RelativeLayout.LayoutParams) Info2.getLayoutParams();
        Info2param.topMargin = singleton.ratiodp(false,0.005);
        Info3 = itemInfo.findViewById(R.id.light_blue_card_contents3);
        Info3.setTypeface(singleton.getNanumb());
        RelativeLayout.LayoutParams Info3param = (RelativeLayout.LayoutParams) Info3.getLayoutParams();
        Info3param.bottomMargin = singleton.ratiodp(false,0.03);

        //새 스케줄 입력
        newitemV = ContentV.findViewById(R.id.timetable_newitem);
        newitemV.getLayoutParams().height = newVHeight;
        newitemV.setY(newVHeight);
        //직접입력 시작
        searchV = newitemV.findViewById(R.id.timetable_newitem_search_view);

        TextView direct = searchV.findViewById(R.id.timetable_newitem_direct);
        direct.setTypeface(singleton.getBmjua());
        direct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    searchV.setVisibility(View.GONE);
                    customV.setVisibility(View.VISIBLE);
                    isSearch = false;
                    if(!isAlterCustom&&customSave.getText().toString().equals("수정")) customSave.setText("저장");
            }
        });
        TextView close = searchV.findViewById(R.id.timetable_newitem_close);
        close.setTypeface(singleton.getBmjua());
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClosenewitemV();
            }
        });

        input = searchV.findViewById(R.id.timetable_newitem_input);
        input.getLayoutParams().height = singleton.ratiodp(false,0.0375);
        input.setTypeface(singleton.getNanumr());
        input.requestFocus();
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                classListRecyclerAdapter = new ClassListRecyclerAdapter(mContext, setClassItems(charSequence));
                itemList.setAdapter(classListRecyclerAdapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        itemList = searchV.findViewById(R.id.timetable_newitem_items);
        itemList.setLayoutManager(new LinearLayoutManager(mContext));
        classListRecyclerAdapter = new ClassListRecyclerAdapter(mContext, setClassItems(""));
        itemList.setAdapter(classListRecyclerAdapter);
        itemList.setItemViewCacheSize(10);
        itemList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE: //The RecyclerView is not scrolling
                        if(isAlting) {
                            loading.dismiss();
                            isAlting = false;
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING: //Scrolling now
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING: //Scroll Settling
                        break;
                }
            }
        });
        //직접입력 끝

        customV = newitemV.findViewById(R.id.timetable_newitem_custom_view);
        LinearLayout customTopV = customV.findViewById(R.id.timetable_custom_top);
        ((RelativeLayout.LayoutParams)customTopV.getLayoutParams()).height = singleton.ratiodp(false,0.057);

        TextView changeSearch = customTopV.findViewById(R.id.timetable_custom_search);
        if(isNoSchool) changeSearch.setVisibility(View.GONE);
        else {
            changeSearch.setTypeface(singleton.getBmjua());
            changeSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    searchV.setVisibility(View.VISIBLE);
                    customV.setVisibility(View.GONE);
                    isSearch = true;
                    isAlterCustom = false;
                }
            });
        }
        customSave = customTopV.findViewById(R.id.timetable_custom_save);
        customSave.setTypeface(singleton.getBmjua());
        customSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveCustom();
            }
        });
        TextView customClose = customTopV.findViewById(R.id.timetable_custom_close);
        customClose.setTypeface(singleton.getBmjua());
        customClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClosenewitemV();
            }
        });
        customInputV = customV.findViewById(R.id.timetable_custom_input);
        customInputV.getLayoutParams().height = singleton.ratiodp(false,0.31);
        customName = customInputV.findViewById(R.id.timetable_custom_name);
        customName.setTypeface(singleton.getNanumb());
        ((LinearLayout.LayoutParams)customName.getLayoutParams()).width = singleton.ratiodp(true,0.765);
        ((RelativeLayout.LayoutParams)customInputV.findViewById(R.id.timetable_custom_name_view).getLayoutParams()).bottomMargin = singleton.ratiodp(false,0.016);
        customRoom = customInputV.findViewById(R.id.timetable_custom_place);
        customRoom.setTypeface(singleton.getNanumb());
        ((LinearLayout.LayoutParams)customRoom.getLayoutParams()).width = singleton.ratiodp(true,0.765);
        ((RelativeLayout.LayoutParams)customInputV.findViewById(R.id.timetable_custom_place_view).getLayoutParams()).bottomMargin = singleton.ratiodp(false,0.016);

        customInputV.addView(setCustomWeel());
        RelativeLayout.LayoutParams wheelparam = (RelativeLayout.LayoutParams) npVs.get(0).getLayoutParams();
        wheelparam.height = singleton.ratiodp(false,0.137);
        wheelparam.addRule(RelativeLayout.ABOVE,R.id.timetable_custom_name_view);
        wheelparam.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        AdView mAdiVew =  ContentV.findViewById(R.id.timetable_adView);
        mAdiVew.loadAd(new AdRequest.Builder().build());

        return ContentV;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFrontend) {
            TimeTickPublisher.getInstance().notifychange(true);
            isFrontend = false;
        }
        else setTalbeView();
    }

    public void onBackPressed() {
        long nowT = System.currentTimeMillis();
        if(isOnItem) {
            closeInfoV();
        } else if(isInputing) {
            ClosenewitemV();
        } else if(nowT - beforePress < 2000) {
            ((Activity)mContext).moveTaskToBack(true);
            ((Activity)mContext).finish();
            android.os.Process.killProcess(android.os.Process.myPid());
        } else {
            beforePress = nowT;
            Toast.makeText(mContext,"뒤로 키를 한번 더 누르시면 종료됩니다.",Toast.LENGTH_SHORT).show();
        }
    }

    private void setTalbeView() {
        List<TimeTableItem> items = mfunction.getMyclassItem();
        for(int i = 1; i < 6; i++) {
            items.get(i).setListener(new TableListener() {
                @Override
                public void onSelect(String cid, int day) {
                    if(day != -1) {
                        //누른거
                        if(!PositionCid.equals(cid)) {
                            PositionCid = cid;
                            setInfoV(day);
                        }
                        if(!isInputing) showInfoV();
                    } else if(day == -1) closeInfoV();
                    if(!isAlting) ClosenewitemV();
                }
            });
        }
        Calendar c = Calendar.getInstance();
        timeTableRecyclerAdapter = new TimeTableRecyclerAdapter(mContext,items,c.get(Calendar.DAY_OF_WEEK)-1);
        TableContents.setAdapter(timeTableRecyclerAdapter);
        updateWidget();
//        resetNoti();
    }

    private List<String[]> Search(CharSequence s) {
        List<String[]> cidlist = new ArrayList<>();
        for (int i = 0; i < cids.length; i++) {
            if (cids[i].contains(s) || points[i].contains(s) || types[i].contains(s) || departements[i].contains(s) ||
                    cnames[i].contains(s) || profs[i].contains(s) || grades[i].contains(s)) {
                cidlist.add(new String[]{cids[i], cnames[i], grades[i], types[i], points[i], profs[i], departements[i]});
            }
        }

        return cidlist;
    }

    private List<ClassListItem> setClassItems(CharSequence s) {
        List<ClassListItem> list;
        if(s.equals("")) list = mfunction.getAllitem();
        else list = mfunction.getNewItems(Search(s));
        for(ClassListItem l : list) {
            l.setListener(new TableListener() {
                @Override
                public void onSelect(String cid, int p) {
                    int r = mfunction.AlterMyclass(mContext,cid);
                    if(r != -1) {
                        setTalbeView();
                        if(r == 0) Toast.makeText(mContext,"일정 추가 성공.",Toast.LENGTH_SHORT).show();
                        else if(r == 1) Toast.makeText(mContext,"일정 삭제 성공.",Toast.LENGTH_SHORT).show();
                    }
                    else Toast.makeText(mContext,"다른 일정과 겹쳐서 추가할 수 없습니다.",Toast.LENGTH_SHORT).show();
                }
            });
        }
        return list;
    }

    private void ControlnewitemV() {
        if(isInputing) {
            newitemV.animate().translationY(newVHeight).withLayer()
                    .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).setDuration(250).start();
            addV.animate().rotation(0).setDuration(250)
                    .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).start();
            isInputing = false;
            isAlterCustom = false;
            isAlting = false;
            im.hideSoftInputFromWindow(input.getWindowToken(),0);
        } else {
            if(isSearch) {
                searchV.setVisibility(View.VISIBLE);
                customV.setVisibility(View.GONE);
            } else {
                searchV.setVisibility(View.GONE);
                customV.setVisibility(View.VISIBLE);
            }
            newitemV.animate().translationY(0).withLayer()
                    .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).setDuration(250).start();
            addV.animate().rotation(45).setDuration(250)
                    .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).start();
            //im.showSoftInput(input,InputMethodManager.SHOW_IMPLICIT);
            closeInfoV();
            isInputing = true;
            if(!isAlterCustom&&customSave.getText().toString().equals("수정")) customSave.setText("저장");
//            classListRecyclerAdapter = new ClassListRecyclerAdapter(mContext, setClassItems(""));
//            itemList.setAdapter(classListRecyclerAdapter);
        }
    }

    private void ClosenewitemV() {
        if(isInputing) {
            newitemV.animate().translationY(newVHeight).withLayer()
                    .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).setDuration(250).start();
            addV.animate().rotation(0).setDuration(250)
                    .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).start();
            isInputing = false;
            isAlterCustom = false;
            isAlting = false;
            im.hideSoftInputFromWindow(input.getWindowToken(),0);
        }
    }

    private RelativeLayout setCustomWeel() {
        RelativeLayout cus = (RelativeLayout) inflater.inflate(R.layout.layout_timetable_custom_time,null);
        String[] hour = new String[16];
        String[] min = new String[60];
        for(int i = 0 ; i < 16 ; i++) {
            int t = i+8;
            if(t < 10) hour[i] = "0"+t;
            else hour[i] = Integer.toString(t);
        }
        for(int i = 0; i < 60; i ++) {
            if(i < 10) min[i] = "0"+i;
            else min[i] = Integer.toString(i);
        }

        NumberPicker day = cus.findViewById(R.id.timetable_custom_time_day);
        day.setTypeface(singleton.getBmjua());
        day.setDisplayedValues(new String[]{"월","화","수","목","금"});
        ((RelativeLayout.LayoutParams)day.getLayoutParams()).leftMargin = singleton.ratiodp(true,0.07);
        NumberPicker shour = cus.findViewById(R.id.timetable_custom_time_shour);
        shour.setTypeface(singleton.getBmjua());
        shour.setDisplayedValues(hour);
        ((RelativeLayout.LayoutParams)shour.getLayoutParams()).leftMargin = singleton.ratiodp(true,0.25);
        TextView sdot = cus.findViewById(R.id.timetable_custom_time_sdot);
        sdot.setTypeface(singleton.getBmjua());
        ((RelativeLayout.LayoutParams)sdot.getLayoutParams()).leftMargin = singleton.ratiodp(true,0.41);
        NumberPicker smin = cus.findViewById(R.id.timetable_custom_time_smin);
        smin.setTypeface(singleton.getBmjua());
        smin.setDisplayedValues(min);
        ((RelativeLayout.LayoutParams)smin.getLayoutParams()).leftMargin = singleton.ratiodp(true,0.41);
        TextView dash = cus.findViewById(R.id.timetable_custom_time_dash);
        dash.setTypeface(singleton.getBmjua());
        ((RelativeLayout.LayoutParams)dash.getLayoutParams()).leftMargin = singleton.ratiodp(true,0.56);
        NumberPicker ehour = cus.findViewById(R.id.timetable_custom_time_ehour);
        ehour.setTypeface(singleton.getBmjua());
        ehour.setDisplayedValues(hour);
        ((RelativeLayout.LayoutParams)ehour.getLayoutParams()).leftMargin = singleton.ratiodp(true,0.6);
        TextView edot = cus.findViewById(R.id.timetable_custom_time_edot);
        edot.setTypeface(singleton.getBmjua());
        ((RelativeLayout.LayoutParams)edot.getLayoutParams()).leftMargin = singleton.ratiodp(true,0.76);
        NumberPicker emin = cus.findViewById(R.id.timetable_custom_time_emin);
        emin.setTypeface(singleton.getBmjua());
        emin.setDisplayedValues(min);
        ((RelativeLayout.LayoutParams)emin.getLayoutParams()).leftMargin = singleton.ratiodp(true,0.76);
        day.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                d = newVal;
            }
        });
        shour.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                sh = newVal;
            }
        });
        smin.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                sm = newVal;
            }
        });
        ehour.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                eh = newVal;
            }
        });
        emin.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                em = newVal;
            }
        });

        npVs.add(cus);
        return cus;
    }

    private void saveCustom() {
        List<Float[]> settings = new ArrayList<>();
        for (RelativeLayout cus : npVs) {
            float stime = sh + (float) sm / 60;
            float etime = eh + (float) em / 60;
            if (stime > etime || etime - stime < 0.5) {
                Toast.makeText(mContext, "시간이 잘못 설정되었습니다.\n종료시간은 시작시간보다 최소 30분 이상으로 해주세요", Toast.LENGTH_SHORT).show();
            } else if (customName.getText().toString().length() == 0) {
                Toast.makeText(mContext, "이름은 반드시 입력해 주세요.", Toast.LENGTH_SHORT).show();
            } else {
                settings.add(new Float[]{(float) d, stime, etime});
            }
        }
        if (settings.size() > 0) {
            if (!isAlterCustom) {
                if (mfunction.insertCustom(customName.getText().toString(), customRoom.getText().toString(), "", settings)) {
                    Toast.makeText(mContext, "일정 추가 성공.", Toast.LENGTH_SHORT).show();
                    setTalbeView();
                } else
                    Toast.makeText(mContext, "다른 일정과 겹쳐서 추가할 수 없습니다.", Toast.LENGTH_SHORT).show();
            } else {
                if (mfunction.insertCustom(customName.getText().toString(), customRoom.getText().toString(), PositionCid, settings)) {
                    mfunction.AlterMyclass(mContext, PositionCid);
                    int n = preferences.getInt("custom",0) - 1;
                    PositionCid =  "c" + n;
                    Toast.makeText(mContext, "일정이 수정되었습니다.", Toast.LENGTH_SHORT).show();
                    setTalbeView();
                } else
                    Toast.makeText(mContext, "다른 일정과 겹쳐서 추가할 수 없습니다.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setInfoV(int day){
        String[] contents = mfunction.getClassInfos(PositionCid,day);
        Info1.setText(contents[0]);
        Info2.setText(contents[1]);
        Info3.setText(contents[2]);
        itemInfo.setTranslationY(infoVHeight);
    }

    private void showInfoV() {
        isOnItem = true;
        itemInfo.animate().translationY(0).withLayer()
                .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).setDuration(250).start();
    }

    private void closeInfoV() {
        isOnItem = false;
        itemInfo.animate().translationY(infoVHeight).withLayer()
                .setInterpolator(AnimationUtils.loadInterpolator(mContext, singleton.getAnimaccelerate())).setDuration(250).start();
    }

    private void updateWidget() {
        Intent intent = new Intent(mContext,WidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        int[] ids = AppWidgetManager.getInstance(mContext.getApplicationContext()).getAppWidgetIds(new ComponentName(mContext, WidgetProvider.class));
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
        mContext.sendBroadcast(intent);
    }

    private void resetNoti() {
//        AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
//        Intent i = new Intent(mContext, AlarmReceiver.class);
//        PendingIntent pi = PendingIntent.getBroadcast(mContext,0,i,PendingIntent.FLAG_CANCEL_CURRENT);
//        am.cancel(pi);
//        pi.cancel();
//        editor.putBoolean("setnoti", false).commit();
//        mContext.sendBroadcast(i);
        String[] setting = mfunction.nextAlarm();
        AlarmManager manager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        PendingIntent pi = PendingIntent.getBroadcast(mContext, 0, new Intent(mContext, AlarmReceiver.class), PendingIntent.FLAG_CANCEL_CURRENT);
        manager.cancel(pi);
        pi.cancel();
        if (setting.length > 0) {
            String name = setting[0];
            long time = Long.parseLong(setting[1]);
            String cid = setting[2];

            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(time);
            String t = "" + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);

            if(Build.VERSION.SDK_INT > 23) {
                Log.i("newalarmTable","n - " + name + " t - " + t + " time - " + time);
                manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(mContext, 0,
                        new Intent(mContext, AlarmReceiver.class).putExtra("name", name).putExtra("cid", cid).putExtra("noti", true), 0));
            } else {
                manager.setExact(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(mContext, 0,
                        new Intent(mContext, AlarmReceiver.class).putExtra("name", name).putExtra("cid", cid).putExtra("noti", true), 0));
            }
        }
    }

    @Override
    public void OnTimeTick(boolean isDateChange) {
        if(isDateChange) {
            setTalbeView();
            Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DAY_OF_WEEK) - 1;
            for(int i = 1; i < 6 ; i ++) {
                TextView tx = ContentV.findViewById(mContext.getResources().getIdentifier("id/timetable_" + Integer.toString(i), "id", "com.clab.classpick"));
                tx.setTypeface(singleton.getBmjua());
                if(day == i) {
                    tx.setTextColor(singleton.getColorMain());
                    ContentV.findViewById(mContext.getResources().getIdentifier("id/timetable_under_" + Integer.toString(i), "id", "com.clab.classpick")).setBackgroundColor(singleton.getColorMain());
                } else {
                    tx.setTextColor(singleton.getColorDarkGray());
                    ContentV.findViewById(mContext.getResources().getIdentifier("id/timetable_under_" + Integer.toString(i), "id", "com.clab.classpick")).setBackground(null);
                }
            }
        } else {
            timeTableRecyclerAdapter.Tick();
        }
    }

    @Override
    public void OnAppFrontend() {
        isFrontend = true;
    }

}
