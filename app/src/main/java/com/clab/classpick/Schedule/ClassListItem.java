package com.clab.classpick.Schedule;

/**
 * Created by kyun on 2017. 9. 8..
 */

public class ClassListItem {
    private String cid,title,grade,type,point,prof,department;
    private String[] period,rid;
    private TableListener listener;

    public String getCid() {
        return cid;
    }

    public String getTitle() {
        return title;
    }

    public String getGrade() {
        return grade;
    }

    public String getType() {
        return type;
    }

    public String getPoint() {
        return point;
    }

    public String getProf() {
        return prof;
    }

    public String getDepartment() {
        return department;
    }

    public String[] getPeriod() {
        return period;
    }

    public String[] getRid() {
        return rid;
    }

    public TableListener getListener() {
        return listener;
    }

    public void setListener(TableListener listener) {
        this.listener = listener;
    }

    public ClassListItem (String cid,String title,String grade, String type, String point, String prof,String department, String[] period, String[] rid) {
        this.cid = cid;
        this.title = title;
        this.grade = grade;
        this.type = type;
        this.point =point;
        this.prof = prof;
        this.department = department;
        this.period = period;
        this.rid = rid;
    }

}
