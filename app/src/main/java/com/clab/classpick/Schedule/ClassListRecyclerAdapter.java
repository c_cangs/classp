package com.clab.classpick.Schedule;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.clab.classpick.R;
import com.clab.classpick.utils.Realm.mMyclass;
import com.clab.classpick.utils.Singletons.Singleton;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by kyun on 2017. 9. 8..
 */

public class ClassListRecyclerAdapter extends BaseQuickAdapter<ClassListItem,BaseViewHolder> {

    private Singleton singleton;

    private int ih;

    private List<onoff> onoffList;

    private List<String> cids;


    public ClassListRecyclerAdapter(Context context, List<ClassListItem> list) {
        super(R.layout.ritem_classlist,list);
        if(singleton == null) singleton = Singleton.getInstance(context);
        ih = singleton.ratiodp(false,0.129);
        onoffList = new ArrayList<>();
        cids = new ArrayList<>();
    }

    public void change(String cid, boolean on) {
        if(cids.contains(cid)) {
            int p = cids.indexOf(cid);
            if(onoffList.get(p) != null) onoffList.get(p).onoff(on);
        }
    }

    @Override
    protected void convert(BaseViewHolder helper, ClassListItem item) {
        final View main = helper.getConvertView();
        final int po = helper.getAdapterPosition();
        main.setBackgroundColor(singleton.getColorWhite());

        main.getLayoutParams().height = ih;
        final TableListener listener = item.getListener();
        final String cid = item.getCid();

        int pad = singleton.ratiodp(true,0.033);
        TextView title = helper.getView(R.id.classlist_title);
        title.setText(item.getTitle());
        title.setTypeface(singleton.getNanumb());
        title.setPadding(pad,0,0,0);
        TextView middle = helper.getView(R.id.classlist_middle);
        middle.setTypeface(singleton.getNanumb());
        middle.setPadding(pad,0,0,0);
        if(item.getPeriod().length > 1) {
            String s = "";
            String n = "";
            for(int i = 0; i < item.getPeriod().length; i++) {
                if(i == 0) s = item.getPeriod()[i];
                else s += " | " + item.getPeriod()[i];
                if(i == 0) n = item.getRid()[i];
                else n += " | " + item.getRid()[i];
            }
            middle.setText(s+"\n"+n);
        }
        else if(item.getPeriod().length == 1) middle.setText(item.getPeriod()[0]+"\n"+item.getRid()[0]);
        TextView cidt = helper.getView(R.id.classlist_cid_department);
        cidt.setText(item.getDepartment() + " | " + item.getCid());
        cidt.setTypeface(singleton.getNanumb());
        cidt.setPadding(pad,0,0,0);
        TextView right = helper.getView(R.id.classlist_right);
        right.setText(item.getGrade()+"\n"+item.getType()+"\n"+item.getPoint()+"\n"+item.getProf());
        if(Realm.getInstance(singleton.getmConfig()).where(mMyclass.class).equalTo("cid",cid).findAll().size() > 0) main.setBackgroundColor(Color.rgb(238,238,238));

        cids.add(cid);
        onoffList.add(new onoff() {
            @Override
            public void onoff(boolean on) {
                if(on) main.setBackgroundColor(Color.rgb(238,238,238));
                else main.setBackgroundColor(singleton.getColorWhite());
            }
        });

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSelect(cid,po);
                if(Realm.getInstance(singleton.getmConfig()).where(mMyclass.class).equalTo("cid",cid).findAll().size() > 0) {
                    if (((ColorDrawable) main.getBackground()).getColor() == singleton.getColorWhite())
                        main.setBackgroundColor(Color.rgb(238, 238, 238));
                }
                else main.setBackgroundColor(singleton.getColorWhite());
            }
        });
    }

    private interface onoff {
        void onoff(boolean on);
    }


}
