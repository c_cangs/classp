package com.clab.classpick.Schedule;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.clab.classpick.R;
import com.clab.classpick.utils.Singletons.Singleton;

import java.util.Calendar;
import java.util.List;

/**
 * Created by kyun on 2017. 9. 7..
 */

public class TimeTableRecyclerAdapter extends BaseQuickAdapter<TimeTableItem,BaseViewHolder> {

    private Singleton singleton;
    private Context mContext;

    private LayoutInflater inflater;

    private TimeTick timetickBar,timetickText;

    private int today;

    private LinearLayout nowtimeBarView, nowtimeTextView;

    public TimeTableRecyclerAdapter(Context context, List<TimeTableItem> list, int today) {
        super(R.layout.ritem_timetable,list);
        mContext = context;
        this.today = today;
        if(singleton == null) singleton = Singleton.getInstance(context);
        inflater = LayoutInflater.from(context);
    }

    public void Tick() {
        if(timetickBar != null) timetickBar.Tick();
        if(timetickText != null) timetickText.Tick();
    }


    @Override
    protected void convert(BaseViewHolder helper, TimeTableItem item) {
        RelativeLayout mainV = (RelativeLayout) helper.getConvertView();
        int pa = singleton.ratiodp(true,0.009);
        if(helper.getLayoutPosition() == 0) { //시간 글씨
            mainV.getLayoutParams().width = singleton.weightdp(true,5.243,0.243);
            mainV.removeAllViews();
            LinearLayout timeline = new LinearLayout(mContext);
            timeline.setOrientation(LinearLayout.VERTICAL);
            mainV.addView(timeline);
            timeline.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            timeline.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
            for(int i = 8; i < 23; i++) {
                TextView tx = new TextView(mContext);
                tx.setText(Integer.toString(i));
                tx.setTypeface(singleton.getNanumr());
                tx.setTextSize(8);
                tx.setPadding(pa,0,0,0);
                timeline.addView(tx);
                LinearLayout.LayoutParams txparam = (LinearLayout.LayoutParams) tx.getLayoutParams();
                txparam.width = ViewGroup.LayoutParams.MATCH_PARENT;
                txparam.weight = 1;
            }
        } else {
            final TableListener listener = item.getListener();
            mainV.getLayoutParams().width = singleton.weightdp(true,5.243,1);
            mainV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onSelect("",-1);
                }
            });
            final int day = item.getDay();
            for(int i = 0; i < item.getCid().length; i++) {
                final String cid = item.getCid()[i];
                String title = item.getTitle()[i];
                String room = item.getRoom()[i];
                String stime = item.getStime()[i];
                String etime = item.getEtime()[i];
                int color = item.getColor()[i];

                LinearLayout itemV = (LinearLayout) inflater.inflate(R.layout.ritem_tableitem,null);
                mainV.addView(itemV);
                itemV.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                View contentsB = itemV.findViewById(R.id.table_contents_back);
                View conV = itemV.findViewById(R.id.table_contents_view);
                contentsB.setBackgroundColor(singleton.getTableColor(color));
                LinearLayout.LayoutParams topparam = (LinearLayout.LayoutParams) itemV.findViewById(R.id.table_top).getLayoutParams();
                LinearLayout.LayoutParams conparam = (LinearLayout.LayoutParams) conV.getLayoutParams();
                LinearLayout.LayoutParams bottomparam = (LinearLayout.LayoutParams) itemV.findViewById(R.id.table_bottom).getLayoutParams();

                float starttime = singleton.timeTofloat(stime);
                float endtime = singleton.timeTofloat(etime);
                int weightsum = 15;
                itemV.setWeightSum(weightsum);
                topparam.weight = starttime - 8;
                if(endtime < 23) {
                    bottomparam.weight = 23 - endtime;
                    conparam.weight = weightsum - (topparam.weight + bottomparam.weight);
                } else {
                    conparam.weight = weightsum - (topparam.weight);
                }

                if(endtime - starttime < 1.5 && title.length() > 8) title = title.substring(0,8) + "...";
                TextView contents = itemV.findViewById(R.id.table_contents);
                contents.setTypeface(singleton.getNanumbg());
                if(!room.equals("")) contents.setText(title + "\n" + room + "\n" + stime + "~" + etime);
                else contents.setText(title + "\n" + stime + "~" + etime);
                contents.setPadding(pa,pa/3,pa,0);

                conV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onSelect(cid,day);
                    }
                });
                mainV.bringToFront();
            }
            if(day == today) {
                final RelativeLayout fmainV = mainV;
                nowtimeBarView = (LinearLayout) inflater.inflate(R.layout.ritem_nowtime_bar,null);
                fmainV.addView(nowtimeBarView);
                timetickBar = new TimeTick() {
                    @Override
                    public void Tick() {
                        Calendar c = Calendar.getInstance();
                        int nowhour = c.get(Calendar.HOUR_OF_DAY);
                        fmainV.removeView(nowtimeBarView);
                        if(nowhour > 7 && nowhour < 23) {
                            nowtimeBarView = (LinearLayout) inflater.inflate(R.layout.ritem_nowtime_bar, null);
                            fmainV.addView(nowtimeBarView);
                            RelativeLayout.LayoutParams nowtimeparam = (RelativeLayout.LayoutParams) nowtimeBarView.getLayoutParams();
                            nowtimeparam.height = ViewGroup.LayoutParams.MATCH_PARENT;
                            LinearLayout.LayoutParams topparam = (LinearLayout.LayoutParams) nowtimeBarView.findViewById(R.id.nowtime_bar_top).getLayoutParams();
                            LinearLayout.LayoutParams bottomparam = (LinearLayout.LayoutParams) nowtimeBarView.findViewById(R.id.nowtime_bar_bottom).getLayoutParams();
                            String ntime = nowhour + ":" + c.get(Calendar.MINUTE);
                            float nowtime = singleton.timeTofloat(ntime);
                            nowtimeBarView.setWeightSum(15);
                            topparam.weight = nowtime - 8;
                            bottomparam.weight = 15 - topparam.weight;
                        }
                    }
                };

                timetickBar.Tick();
            } else if((today -1 == day && today < 6 && today > 0) || (today == 1 && day == 2)) {
                final RelativeLayout fmainV = mainV;
                nowtimeTextView = (LinearLayout) inflater.inflate(R.layout.ritem_nowtime_text,null);
                fmainV.addView(nowtimeTextView);
                timetickText = new TimeTick() {
                    @Override
                    public void Tick() {
                        Calendar c = Calendar.getInstance();
                        int nowhour = c.get(Calendar.HOUR_OF_DAY);
                        fmainV.removeView(nowtimeTextView);
                        if(nowhour > 7 && nowhour < 23) {
                            nowtimeTextView = (LinearLayout) inflater.inflate(R.layout.ritem_nowtime_text, null);
                            fmainV.addView(nowtimeTextView);
                            RelativeLayout.LayoutParams nowtimeparam = (RelativeLayout.LayoutParams) nowtimeTextView.getLayoutParams();
                            nowtimeparam.height = ViewGroup.LayoutParams.MATCH_PARENT;
                            LinearLayout.LayoutParams topparam = (LinearLayout.LayoutParams) nowtimeTextView.findViewById(R.id.nowtime_text_top).getLayoutParams();
                            LinearLayout.LayoutParams bottomparam = (LinearLayout.LayoutParams) nowtimeTextView.findViewById(R.id.nowtime_text_bottom).getLayoutParams();
                            int min = c.get(Calendar.MINUTE);
                            String ntime = nowhour + ":" + min;
                            float nowtime = singleton.timeTofloat(ntime);
                            nowtimeTextView.setWeightSum(15);
                            topparam.weight = nowtime+textW(nowtime) - 8;
                            bottomparam.weight = 15 - topparam.weight;
                            String h,m;
                            if(nowhour < 10) h = "0"+nowhour;
                            else h = Integer.toString(nowhour);
                            if(min < 10) m = "0"+min;
                            else m = Integer.toString(min);
                            TextView nowtimetext = nowtimeTextView.findViewById(R.id.nowtime_text);
                            nowtimetext.setTypeface(singleton.getNanumbg());
                            nowtimetext.setText(h+":"+m);
                            if(today - 1 == day) nowtimetext.setGravity(Gravity.RIGHT);
                            else if(today == 1 && day == 2) nowtimetext.setGravity(Gravity.LEFT);
                        }
                    }
                };
                timetickText.Tick();
            }
        }
    }

    private float textW(float nowtime) {
        float r = nowtime - 13;
        if(r < 0) return r*(float)0.014;
        else return r/100;
    }

    private interface TimeTick {
        void Tick();
    }
}
