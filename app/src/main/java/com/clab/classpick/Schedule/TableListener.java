package com.clab.classpick.Schedule;

/**
 * Created by kyun on 2017. 9. 7..
 */

public interface TableListener {
    void onSelect(String cid, int p);
}
