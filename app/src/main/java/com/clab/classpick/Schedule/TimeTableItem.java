package com.clab.classpick.Schedule;

/**
 * Created by kyun on 2017. 9. 7..
 */

public class TimeTableItem {

    private String[] cid, title, room, stime, etime;
    private Integer[] color;
    private int day;
    private TableListener listener;

    public String[] getCid() {
        return cid;
    }

    public String[] getTitle() {
        return title;
    }

    public String[] getRoom() {
        return room;
    }

    public String[] getStime() {
        return stime;
    }

    public String[] getEtime() {
        return etime;
    }

    public Integer[] getColor() {
        return color;
    }

    public int getDay() {
        return day;
    }

    public TableListener getListener() {
        return listener;
    }

    public void setListener(TableListener listener) {
        this.listener = listener;
    }

    public TimeTableItem(String[] cid, String[] title, String[] room, String[] stime, String[] etime, Integer[] color,int day) {
        this.cid = cid;
        this.title = title;
        this.room = room;
        this.stime = stime;
        this.etime = etime;
        this.color = color;
        this.day = day;
    }

}
