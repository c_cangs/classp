package com.clab.classpick

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.*
import android.util.Log
import android.view.WindowManager
import com.clab.classpick.utils.AlarmReceiver
import com.clab.classpick.utils.Singletons.mFunction
import kotlinx.android.synthetic.main.activity_noti.*
import java.util.*

/**
 * Created by kyun on 2017. 9. 30..
 */

class NotiActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("noti","notiA")
        setContentView(R.layout.activity_noti)
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON or WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
        WindowManager.LayoutParams.FLAG_FULLSCREEN)
        if(intent.getBooleanExtra("isScreen",false)) {
            noti_dissmiss.setOnClickListener { finish() }
            noti_background.setOnClickListener { finish() }
            noti_contents.setText(intent.getStringExtra("name"))
            object : Handler() {
                override fun handleMessage(msg: Message?) {
                    super.handleMessage(msg)
                    finish()
                }
            }.sendEmptyMessageDelayed(0, 8000)
        }
        else {
            setContentView(R.layout.activity_nullv)
            finish()
        }
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        finish()
    }

    override fun finish() {
        val mfunction = mFunction.getInstance(this@NotiActivity)
        val setting = mfunction.nextAlarm()
        if (setting.isNotEmpty()) {
            val name = setting[0]
            val time = setting[1].toLong()
            val cid = setting[2]

            val c = Calendar.getInstance()
            c.timeInMillis = time
            val t : String = "" + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE)
            val manager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pi = PendingIntent.getBroadcast(this@NotiActivity, 0, Intent(this@NotiActivity, AlarmReceiver::class.java), PendingIntent.FLAG_CANCEL_CURRENT)
            manager.cancel(pi)
            pi.cancel()
            if(Build.VERSION.SDK_INT > 23) {
                Log.i("newalarmActi","n - " + name + " t - " + t + " time - " + time)
                manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(this@NotiActivity, 0,
                        Intent(this@NotiActivity, AlarmReceiver::class.java).putExtra("name", name).putExtra("cid", cid).putExtra("noti", true), 0))
            } else {
                manager.setExact(AlarmManager.RTC_WAKEUP, time, PendingIntent.getBroadcast(this@NotiActivity, 0,
                        Intent(this@NotiActivity, AlarmReceiver::class.java).putExtra("name", name).putExtra("cid", cid).putExtra("noti", true), 0))
            }
        }
        if(intent.getBooleanExtra("isScreen",false)) moveTaskToBack(true)
        else {
            val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            var s = ""
            if (Build.VERSION.SDK_INT > 20) {
                if(am.runningAppProcesses.size > 1) s = am.runningAppProcesses.get(1).processName
                else am.runningAppProcesses.get(0).processName
                Log.i("topA",s)
                if (s != "com.clab.classpick") {
                    moveTaskToBack(true)
                }
            } else {
                if(am.getRunningTasks(2).size > 1) s = am.getRunningTasks(2).get(1).topActivity.packageName
                else s = am.getRunningTasks(2).get(0).topActivity.packageName
                Log.i("topA",s)
                if (s != "com.clab.classpick") {
                    moveTaskToBack(true)
                }
            }
        }
        super.finish()
    }

}
