package com.clab.classpick.Favorite.Building;

/**
 * Created by kyun on 2017-02-15.
 */

public class FavoriteBuildingItem {

    private String building;
    private int roomcount;
    public String getBuilding() {
        return building;
    }
    public int getRoomcount() {
        return roomcount;
    }

    public FavoriteBuildingItem(String b, int r) {
        this.building = b;
        this.roomcount = r;
    }
}
