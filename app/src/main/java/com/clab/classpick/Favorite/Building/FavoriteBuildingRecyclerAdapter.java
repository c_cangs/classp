package com.clab.classpick.Favorite.Building;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.DialogPreference;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.clab.classpick.BuildingInfo.BuildingInfoActivity;
import com.clab.classpick.R;
import com.clab.classpick.utils.Singletons.Singleton;
import com.clab.classpick.utils.Singletons.mFunction;

import java.util.List;

/**
 * Created by kyun on 2017-02-14.
 */

public class FavoriteBuildingRecyclerAdapter extends BaseQuickAdapter<FavoriteBuildingItem,BaseViewHolder> {

    private Context mContext;
    private Singleton singleton;
    private RelativeLayout.LayoutParams layoutParams, deleteparam;

    private AlertDialog.Builder dialog;

    private TextView building, count;
    private ImageButton delete;


    public FavoriteBuildingRecyclerAdapter (Context context, List<FavoriteBuildingItem> items){
        super(R.layout.layout_favorite_buildingitem,items);
        this.mContext = context;
        if(singleton == null) singleton = Singleton.getInstance(context);

        layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, singleton.ratiodp(false,0.113));
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        layoutParams.topMargin = singleton.ratiodp(false,0.005);
        deleteparam = new RelativeLayout.LayoutParams(singleton.ratiodp(true,0.068), singleton.ratiodp(false,0.0382));
        deleteparam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        deleteparam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        dialog = singleton.CustomDialog(mContext).setTitle("즐겨찾기 삭제").setMessage("즐겨찾기를 삭제하시겠습니까?");
    }

    @Override
    protected void convert(BaseViewHolder helper, final FavoriteBuildingItem item) {
        LinearLayout background = helper.getView(R.id.fav_background);
        background.setLayoutParams(layoutParams);
        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, BuildingInfoActivity.class).putExtra("bname",item.getBuilding()).putExtra("isfav",true));
                ((Activity) mContext).overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_in_right_inner);
            }
        });
        building = helper.getView(R.id.fbuildingitem_building);
        building.setTypeface(singleton.getNanumeb());
        building.setText(item.getBuilding());
        count = helper.getView(R.id.fbuildingitem_count);
        count.setTypeface(singleton.getBmjua());
        count.setText(Integer.toString(item.getRoomcount()));
        delete = helper.getView(R.id.fbuildingitem_delete);
        delete.setLayoutParams(deleteparam);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setPositiveButton("네", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mFunction.getInstance(mContext).FavoriteUpdate(mContext,false,item.getBuilding(),null);
                    }
                }).setNegativeButton("아니요",null).show();
            }
        });
        helper.setTypeface(R.id.fbuildingitem_freenav,singleton.getBmjua());
    }
}
