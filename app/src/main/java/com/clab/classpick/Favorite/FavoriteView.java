package com.clab.classpick.Favorite;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clab.classpick.Favorite.Building.FavoriteBuildingRecyclerAdapter;
import com.clab.classpick.Favorite.Room.FavoriteRoomRecyclerAdapter;
import com.clab.classpick.R;
import com.clab.classpick.Search.SearchActivity;
import com.clab.classpick.utils.Observer.AppFrontEnd.AppFrontendPublisher;
import com.clab.classpick.utils.Observer.AppFrontEnd.OnAppFrontendObserv;
import com.clab.classpick.utils.Observer.Favorite.FavoritePublisher;
import com.clab.classpick.utils.Observer.Favorite.OnFavoriteChangeBuildingObserv;
import com.clab.classpick.utils.Observer.Favorite.OnFavoriteChangeRoomObserv;
import com.clab.classpick.utils.Observer.TimeTick.OnTimeTickObserv;
import com.clab.classpick.utils.Observer.TimeTick.TimeTickPublisher;
import com.clab.classpick.utils.Singletons.Singleton;
import com.clab.classpick.utils.Singletons.mFunction;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Calendar;

import static com.chad.library.adapter.base.BaseQuickAdapter.ALPHAIN;

/**
 * Created by kyun on 2017-02-14.
 */

@SuppressLint("ValidFragment")
public class FavoriteView extends Fragment implements OnFavoriteChangeRoomObserv,OnFavoriteChangeBuildingObserv,OnAppFrontendObserv,OnTimeTickObserv {

    private Singleton singleton;
    private mFunction mfunction;

    private Context mContext;

    private boolean isclassroom, isFrontend;

    private RecyclerView favoriteview;

    public FavoriteView() {}

    public FavoriteView(int i) {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(singleton == null) singleton = Singleton.getInstance(getContext());
        if(mfunction == null) mfunction = mFunction.getInstance(getContext());
        AppFrontendPublisher.getInstance().addlistener(this);
        FavoritePublisher.getInstance().addroom(this);
        FavoritePublisher.getInstance().addbuilding(this);
        TimeTickPublisher.getInstance().addlistener(this);
        isclassroom = true;
        isFrontend = false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_favorite, null);
        v.findViewById(R.id.favorite_topshadow).bringToFront();

        ((TextView) v.findViewById(R.id.favorite_top_text)).setTypeface(singleton.getBmjua());

        v.findViewById(R.id.favorite_search_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, SearchActivity.class));
                ((Activity)mContext).overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_in_right_inner);
            }
        });

        favoriteview = v.findViewById(R.id.favorite_recycler);
        favoriteview.setItemViewCacheSize(10);
        favoriteview.setDrawingCacheEnabled(true);
        favoriteview.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

//        ((LinearLayout.LayoutParams) v.findViewById(R.id.favorite_recycler_left).getLayoutParams()).weight = 1;
//        ((LinearLayout.LayoutParams) v.findViewById(R.id.favorite_recycler_right).getLayoutParams()).weight = 1;
//        ((LinearLayout.LayoutParams) favoriteview.getLayoutParams()).weight = 16;

        setAniView();

        final RelativeLayout classroombtn = v.findViewById(R.id.favorite_classroom_btn);
        final RelativeLayout buildingbtn = v.findViewById(R.id.favorite_building_btn);

        final TextView classroomtxt = v.findViewById(R.id.favorite_classroom_text);
        final TextView buildingtxt = v.findViewById(R.id.favorite_building_text);

        classroomtxt.setTypeface(singleton.getBmjua());
        buildingtxt.setTypeface(singleton.getBmjua());

        classroombtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isclassroom) {
                    isclassroom = true;
                    setAniView();
                    classroombtn.setBackgroundResource(R.drawable.left_on);
                    buildingbtn.setBackgroundResource(R.drawable.right_off);
                    classroomtxt.setTextColor(getResources().getColor(R.color.white));
                    buildingtxt.setTextColor(getResources().getColor(R.color.mainColor));
                }
            }
        });

        buildingbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isclassroom) {
                    isclassroom = false;
                    setAniView();
                    classroombtn.setBackgroundResource(R.drawable.left_off);
                    buildingbtn.setBackgroundResource(R.drawable.right_on);
                    classroomtxt.setTextColor(getResources().getColor(R.color.mainColor));
                    buildingtxt.setTextColor(getResources().getColor(R.color.white));
                }
            }
        });

        AdView mAdiVew =  v.findViewById(R.id.favorite_adView);
        mAdiVew.loadAd(new AdRequest.Builder().build());

        v.findViewById(R.id.favorite_search_btn).bringToFront();


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFrontend) {
            setAniView();
            isFrontend = false;
        }
    }

    private void setAniView() {
        if(isclassroom) {
            favoriteview.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
            FavoriteRoomRecyclerAdapter recyclerAdapter = new FavoriteRoomRecyclerAdapter(mContext, mfunction.favoriteRoomSet());
            recyclerAdapter.openLoadAnimation(ALPHAIN);
            recyclerAdapter.setNotDoAnimationCount(3);
            recyclerAdapter.isFirstOnly(false);
            favoriteview.setAdapter(recyclerAdapter);
        } else {
            favoriteview.setLayoutManager(new LinearLayoutManager(mContext));
            FavoriteBuildingRecyclerAdapter recyclerAdapter = new FavoriteBuildingRecyclerAdapter(mContext, mfunction.favoriteBuildingSet());
            recyclerAdapter.openLoadAnimation(ALPHAIN);
            recyclerAdapter.setNotDoAnimationCount(3);
            recyclerAdapter.isFirstOnly(false);
            favoriteview.setAdapter(recyclerAdapter);
        }
    }

    private void setNoAniView() {
        if(isclassroom) {
            favoriteview.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
            FavoriteRoomRecyclerAdapter recyclerAdapter = new FavoriteRoomRecyclerAdapter(mContext, mfunction.favoriteRoomSet());
            favoriteview.setAdapter(recyclerAdapter);
        } else {
            favoriteview.setLayoutManager(new LinearLayoutManager(mContext));
            FavoriteBuildingRecyclerAdapter recyclerAdapter = new FavoriteBuildingRecyclerAdapter(mContext, mfunction.favoriteBuildingSet());
            favoriteview.setAdapter(recyclerAdapter);
        }
    }

    @Override
    public void OnAppFrontend() {
        isFrontend = true;
    }

    @Override
    public void FavoriteRoomChanged() {
        setAniView();
    }

    @Override
    public void FavoriteBuildingChanged() {
        setAniView();
    }

    @Override
    public void OnTimeTick(boolean isDateChange) {
        if(Calendar.getInstance().get(Calendar.MINUTE)%5 == 1) setNoAniView();
    }
}
