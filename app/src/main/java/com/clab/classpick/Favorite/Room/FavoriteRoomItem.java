package com.clab.classpick.Favorite.Room;

/**
 * Created by kyun on 2017-02-14.
 */

public class FavoriteRoomItem {

    private boolean isfree;
    private String building, room, time, classname;
    public boolean getisfree() {
        return isfree;
    }
    public String getBuilding() {
        return building;
    }
    public String getRoom() {
        return room;
    }
    public String getTime() {
        return time;
    }
    public String getClassname() {
        return classname;
    }

    public FavoriteRoomItem(boolean f, String b, String r, String t, String c) {
        this.isfree = f;
        this.building = b;
        this.room = r;
        this.time = t;
        this.classname = c;
    }
}
