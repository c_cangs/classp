package com.clab.classpick.Favorite.Room;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.clab.classpick.ClassRoomInfo.ClassRoomInfoActivity;
import com.clab.classpick.R;
import com.clab.classpick.utils.Singletons.Singleton;
import com.clab.classpick.utils.Singletons.mFunction;

import java.util.List;

/**
 * Created by kyun on 2017-02-14.
 */

public class FavoriteRoomRecyclerAdapter extends BaseQuickAdapter<FavoriteRoomItem,BaseViewHolder> {

    private Context mContext;
    private Singleton singleton;

    private TextView isfree, building, room, sclass, time;
    private ImageButton itemdelete;
    private RelativeLayout.LayoutParams isfreeparam, inclassparam, itemdeleteparam;

    public FavoriteRoomRecyclerAdapter(Context context, List<FavoriteRoomItem> items) {
        super(R.layout.layout_favorite_classitem, items);
        this.mContext = context;
        if (singleton == null) singleton = Singleton.getInstance(context);
        isfreeparam = new RelativeLayout.LayoutParams(singleton.ratiodp(true, 0.428), singleton.ratiodp(false, 0.143));
        isfreeparam.addRule(RelativeLayout.CENTER_IN_PARENT);
        isfreeparam.topMargin = singleton.ratiodp(false,0.005);
//        isfreeparam.leftMargin = singleton.ratiodp(true,0.002);
//        isfreeparam.rightMargin = singleton.ratiodp(true,0.002);
        inclassparam = new RelativeLayout.LayoutParams(singleton.ratiodp(true, 0.428), singleton.ratiodp(false, 0.179));
        inclassparam.addRule(RelativeLayout.CENTER_IN_PARENT);
        inclassparam.topMargin = singleton.ratiodp(false,0.005);
//        inclassparam.leftMargin = singleton.ratiodp(true,0.002);
//        inclassparam.rightMargin = singleton.ratiodp(true,0.002);
        itemdeleteparam = new RelativeLayout.LayoutParams(singleton.ratiodp(true, 0.068), singleton.ratiodp(false, 0.0382));
        itemdeleteparam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        itemdeleteparam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final FavoriteRoomItem item) {
        LinearLayout background = helper.getView(R.id.fclassitem_background);
        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, ClassRoomInfoActivity.class).putExtra("isfree", item.getisfree())
                        .putExtra("isfav", true).putExtra("bname", item.getBuilding()).putExtra("rid", item.getRoom()));
                ((Activity) mContext).overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_in_right_inner);
            }
        });
        isfree = helper.getView(R.id.fclassitem_isfree);
        building = helper.getView(R.id.fclassitem_building);
        room = helper.getView(R.id.fclassitem_room);
        sclass = helper.getView(R.id.fclassitem_class);
        time = helper.getView(R.id.fclassitem_time);
        itemdelete = helper.getView(R.id.fclassitem_delete);
        itemdelete.setLayoutParams(itemdeleteparam);
        itemdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                singleton.CustomDialog(mContext).setTitle("즐겨찾기 삭제").setMessage("즐겨찾기를 삭제하시겠습니까?").setPositiveButton("네", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mFunction.getInstance(mContext).FavoriteUpdate(mContext,false,item.getBuilding(),item.getRoom());
                    }
                }).setNegativeButton("아니요",null).show();
            }
        });
        if (item.getisfree()) {
//            if ((helper.getLayoutPosition() + 3) % 4 == 0 || (helper.getLayoutPosition() + 2) % 4 == 0) {
//                isfreeparam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//            } else {
//                isfreeparam.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//            }
            background.setBackgroundResource(R.drawable.fav_class_free);
            background.setLayoutParams(isfreeparam);
            isfree.setText("FREE");
            isfree.setTextColor(singleton.getColorYello());
            isfree.setTypeface(singleton.getBmjua());
            building.setText(item.getBuilding());
            building.setTextColor(singleton.getColorDarkGray());
            building.setTypeface(singleton.getNanumr());
            sclass.setVisibility(View.GONE);
            room.setText(item.getRoom()+"호");
            room.setTextColor(singleton.getColorDarkGray());
            room.setTypeface(singleton.getNanumr());
            time.setText("~"+item.getTime());
            time.setTextColor(singleton.getColorDarkGray());
            time.setTypeface(singleton.getNanumr());
        } else {
//            if ((helper.getLayoutPosition() + 3) % 4 == 0 || (helper.getLayoutPosition() + 2) % 4 == 0) {
//                inclassparam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//            } else {
//                inclassparam.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
//            }
            background.setBackgroundResource(R.drawable.fav_class_incalss);
            background.setLayoutParams(inclassparam);
            isfree.setText("IN CLASS");
            isfree.setTextColor(singleton.getColorMain());
            isfree.setTypeface(singleton.getBmjua());
            building.setText(item.getBuilding());
            building.setTextColor(singleton.getColorDarkGray());
            building.setTypeface(singleton.getNanumr());
            room.setText(item.getRoom()+"호");
            room.setTextColor(singleton.getColorDarkGray());
            room.setTypeface(singleton.getNanumr());
            sclass.setVisibility(View.VISIBLE);
            sclass.setText(item.getClassname());
            sclass.setTextColor(singleton.getColorDarkGray());
            sclass.setTypeface(singleton.getNanumr());
            time.setText("~"+item.getTime());
            time.setTextColor(singleton.getColorDarkGray());
            time.setTypeface(singleton.getNanumr());
        }
    }
}
