package com.clab.classpick.BuildingInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clab.classpick.R;
import com.clab.classpick.utils.Observer.Favorite.FavoritePublisher;
import com.clab.classpick.utils.Observer.Favorite.OnFavoriteChangeBuildingObserv;
import com.clab.classpick.utils.Observer.Favorite.OnFavoriteChangeRoomObserv;
import com.clab.classpick.utils.Observer.TimeTick.OnTimeTickObserv;
import com.clab.classpick.utils.Observer.TimeTick.TimeTickPublisher;
import com.clab.classpick.utils.Singletons.Singleton;
import com.clab.classpick.utils.Singletons.mFunction;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Calendar;

import static com.chad.library.adapter.base.BaseQuickAdapter.ALPHAIN;

/**
 * Created by kyun on 2017-03-06.
 */

public class BuildingInfoActivity extends Activity implements OnFavoriteChangeRoomObserv,OnFavoriteChangeBuildingObserv,OnTimeTickObserv {

    private Singleton singleton;
    private mFunction mfunction;

    private AlertDialog.Builder dialog;

    private String bname;
    private int classcount[];
    private boolean isfreeview = true,isinclassview = false, isfav;

    private LinearLayout inclass, isfree;
    private RelativeLayout icons;
    private TextView tab, building, freetxt, freecount, inclasstxt, inclasscount, nav;
    private Button back, fav, loca, share;

    private RecyclerView recyclerView;
    private BuildinginfoRecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buildinginfo);
        if(singleton == null) singleton = Singleton.getInstance(this);
        if(mfunction == null) mfunction = mFunction.getInstance(this);
        dialog = singleton.CustomDialog(this);
        bname = getIntent().getStringExtra("bname");
        isfav = getIntent().getBooleanExtra("isfav",false);
        isfree = findViewById(R.id.buildinginfo_isfree);
        final LinearLayout.LayoutParams isfreeparam = (LinearLayout.LayoutParams) isfree.getLayoutParams();
        isfreeparam.width = singleton.ratiodp(true,0.346);
        isfree.setLayoutParams(isfreeparam);
        isfree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isfreeview) {
                    isfree.setBackgroundResource(R.drawable.buildinginfo_infow);
                    freetxt.setTextColor(singleton.getColorYello());
                    freecount.setTextColor(singleton.getColorYello());
//                    inclass.setBackgroundResource(R.drawable.buildinginfo_infopp);
//                    inclasstxt.setTextColor(Color.parseColor("#FFFFFF"));
//                    inclasscount.setTextColor(Color.parseColor("#FFFFFF"));
                    isfreeview = false;
                    setview();
                } else {
                    isfree.setBackgroundResource(R.drawable.buildinginfo_infoy);
                    freetxt.setTextColor(Color.parseColor("#FFFFFF"));
                    freecount.setTextColor(Color.parseColor("#FFFFFF"));
                    inclass.setBackgroundResource(R.drawable.buildinginfo_infow);
                    inclasstxt.setTextColor(singleton.getColorMain());
                    inclasscount.setTextColor(singleton.getColorMain());
                    isfreeview = true;
                    isinclassview = false;
                    setview();
                }
            }
        });

        inclass = findViewById(R.id.buildinginfo_inclass);
        LinearLayout.LayoutParams inclassparam = (LinearLayout.LayoutParams) inclass.getLayoutParams();
        inclassparam.width = singleton.ratiodp(true,0.346);
        inclass.setLayoutParams(inclassparam);
        inclass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isinclassview) {
                    inclass.setBackgroundResource(R.drawable.buildinginfo_infow);
                    inclasstxt.setTextColor(singleton.getColorinclass());
                    inclasscount.setTextColor(singleton.getColorinclass());
                    isinclassview = false;
                    setview();
                } else {
//                    isfree.setBackgroundResource(R.drawable.buildinginfo_infop);
//                    freetxt.setTextColor(Color.parseColor("#FFFFFF"));
//                    freecount.setTextColor(Color.parseColor("#FFFFFF"));
                    isfree.setBackgroundResource(R.drawable.buildinginfo_infow);
                    freetxt.setTextColor(singleton.getColorYello());
                    freecount.setTextColor(singleton.getColorYello());
                    inclass.setBackgroundResource(R.drawable.buildinginfo_infob);
                    inclasstxt.setTextColor(Color.parseColor("#FFFFFF"));
                    inclasscount.setTextColor(Color.parseColor("#FFFFFF"));
                    isinclassview = true;
                    isfreeview = false;
                    setview();
                }
            }
        });

        icons = findViewById(R.id.buildinginfo_icons);
        LinearLayout.LayoutParams iconsparam = (LinearLayout.LayoutParams) icons.getLayoutParams();
        iconsparam.width = singleton.ratiodp(true,0.303);
        icons.setLayoutParams(iconsparam);
        tab = findViewById(R.id.buildinginfo_tabtxt);
        building = findViewById(R.id.buildinginfo_buildingtxt);
        freetxt = findViewById(R.id.buildinginfo_freetxt);
        freecount = findViewById(R.id.buildinginfo_freecount);
        inclasstxt = findViewById(R.id.buildinginfo_inclasstxt);
        inclasscount = findViewById(R.id.buildinginfo_inclasscount);
        nav = findViewById(R.id.buildinginfo_navtxt);
        nav.setTypeface(singleton.getBmjua());
        inclasscount.setTypeface(singleton.getBmjua());
        inclasstxt.setTypeface(singleton.getBmjua());
        freecount.setTypeface(singleton.getBmjua());
        freetxt.setTypeface(singleton.getBmjua());
        building.setTypeface(singleton.getBmjua());
        tab.setTypeface(singleton.getBmjua());
        back = findViewById(R.id.buildinginfo_back);
        RelativeLayout.LayoutParams backparam= (RelativeLayout.LayoutParams) back.getLayoutParams();
        backparam.height = singleton.ratiodp(false,0.0296);
        backparam.width = singleton.ratiodp(true,0.028);
        backparam.leftMargin = singleton.ratiodp(true,0.0361);
        findViewById(R.id.buildinginfo_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        fav = findViewById(R.id.buildinginfo_favorite);
        fav.getLayoutParams().width = singleton.ratiodp(true,0.072);
        if(isfav) fav.setBackgroundResource(R.drawable.favorite_on);
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isfav) {
                    dialog.setTitle("즐겨찾기 삭제").setMessage("즐겨찾기에 삭제하시겠습니까?").setPositiveButton("네", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mfunction.FavoriteUpdate(BuildingInfoActivity.this, false, bname, null);
                        }
                    }).setNegativeButton("아니요",null).show();
                } else {
                    dialog.setTitle("즐겨찾기 추가").setMessage("즐겨찾기에 추가하시겠습니까?").setPositiveButton("네", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mfunction.FavoriteUpdate(BuildingInfoActivity.this, true, bname, null);
                        }
                    }).setNegativeButton("아니요",null).show();
                }
            }
        });


        classcount = mfunction.countBuildingRoom(bname);

        building.setText(bname);
        freecount.setText(Integer.toString(classcount[0]));
        inclasscount.setText(Integer.toString(classcount[1]));

        recyclerView = findViewById(R.id.buildinginfo_recycler);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemViewCacheSize(10);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        setview();

        AdView mAdiVew = findViewById(R.id.buildinginfo_adView);
        mAdiVew.loadAd(new AdRequest.Builder().build());
    }

    private void setview() {
        recyclerAdapter = new BuildinginfoRecyclerAdapter(this, mfunction.buildingInfoSet(isfreeview,isinclassview,bname));
        recyclerAdapter.openLoadAnimation(ALPHAIN);
        recyclerAdapter.setNotDoAnimationCount(3);
        recyclerAdapter.isFirstOnly(false);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(singleton == null) singleton = Singleton.getInstance(this);
        if(mfunction == null) mfunction = mFunction.getInstance(this);
        dialog = singleton.CustomDialog(this);
        FavoritePublisher.getInstance().addbuilding(this);
        FavoritePublisher.getInstance().addroom(this);
        TimeTickPublisher.getInstance().addlistener(this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.activity_slide_out_right_inner, R.anim.activity_slide_out_right);
    }

    @Override
    public void FavoriteRoomChanged() {
        setview();
    }

    @Override
    public void FavoriteBuildingChanged() {
        if(isfav) {
            fav.setBackgroundResource(R.drawable.favorite_off);
            isfav = false;
        }
        else {
            fav.setBackgroundResource(R.drawable.favorite_on);
            isfav = true;
        }
    }


    @Override
    public void OnTimeTick(boolean isDateChange) {
        if(Calendar.getInstance().get(Calendar.MINUTE)%5 == 0) setview();
    }
}
