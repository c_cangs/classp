package com.clab.classpick.BuildingInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.clab.classpick.ClassRoomInfo.ClassRoomInfoActivity;
import com.clab.classpick.R;
import com.clab.classpick.utils.Singletons.Singleton;
import com.clab.classpick.utils.Singletons.mFunction;

import java.util.List;

/**
 * Created by kyun on 2017-03-06.
 */

public class BuildinginfoRecyclerAdapter extends BaseQuickAdapter<BuildinginfoItem,BaseViewHolder> {

    private Context mContext;

    private Singleton singleton;
    private mFunction mfunction;

    private AlertDialog.Builder dialog;

    private RelativeLayout.LayoutParams mainparam, favparam;

    private TextView building,room,time,name;
    private ImageButton fav;

    public BuildinginfoRecyclerAdapter(Context context, List<BuildinginfoItem> item) {
        super(R.layout.layout_buildinginfo_classroomitem,item);
        this.mContext = context;
        if(singleton == null) singleton = Singleton.getInstance(context);
        if(mfunction == null) mfunction = mFunction.getInstance(context);
        mainparam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,singleton.ratiodp(false,0.101));
        favparam = new RelativeLayout.LayoutParams(singleton.ratiodp(true,0.06),singleton.ratiodp(false,0.0323));
        favparam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        favparam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        dialog = singleton.CustomDialog(context);
    }


    @Override
    protected void convert(BaseViewHolder helper, final BuildinginfoItem item) {
        LinearLayout mainv = helper.getView(R.id.buildinginfo_classitem_view);
        mainv.setLayoutParams(mainparam);
        mainv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext, ClassRoomInfoActivity.class).putExtra("isfree", item.getIsfree())
                        .putExtra("isfav", item.getIsfavorite()).putExtra("bname", item.getBuilding()).putExtra("rid", item.getRoom()));
                ((Activity) mContext).overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_in_right_inner);
            }
        });
        fav = helper.getView(R.id.buildinginfo_classitem_favorite);
        fav.setLayoutParams(favparam);
        building = helper.getView(R.id.buildinginfo_classitem_building);
        room = helper.getView(R.id.buildinginfo_classitme_room);
        time = helper.getView(R.id.buildinginfo_classtiem_time);
        name = helper.getView(R.id.buildinginfo_classitem_classname);
        building.setTypeface(singleton.getNanumb());
        room.setTypeface(singleton.getNanumb());
        time.setTypeface(singleton.getNanumb());
        name.setTypeface(singleton.getNanumb());
        if(item.getIsfree()) {
            helper.setBackgroundRes(R.id.buildinginfo_classitem_status, R.drawable.free_card);
            building.setText(item.getBuilding());
            room.setText(item.getRoom());
            time.setText(item.getTime());
            name.setVisibility(View.GONE);
        } else {
            helper.setBackgroundRes(R.id.buildinginfo_classitem_status, R.drawable.in_class_card);
            building.setText(item.getBuilding());
            room.setText(item.getRoom());
            time.setText(item.getTime());
            name.setText(item.getName());
        }
        if(item.getIsfavorite()){
            fav.setBackgroundResource(R.drawable.favorite_on);
            fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.setTitle("즐겨찾기 삭제").setMessage("즐겨찾기에 삭제하시겠습니까?").setPositiveButton("네", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mfunction.FavoriteUpdate(mContext,false,item.getBuilding(),item.getRoom());
                        }
                    }).setNegativeButton("아니요",null).show();
                }
            });
        } else {
            fav.setBackgroundResource(R.drawable.favorite_off);
            fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.setTitle("즐겨찾기 추가").setMessage("즐겨찾기에 추가하시겠습니까?").setPositiveButton("네", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mfunction.FavoriteUpdate(mContext,false,item.getBuilding(),item.getRoom());
                        }
                    }).setNegativeButton("아니요",null).show();
                }
            });
        }

    }
}
