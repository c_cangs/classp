package com.clab.classpick.BuildingInfo;

/**
 * Created by kyun on 2017-03-06.
 */

public class BuildinginfoItem {

    boolean isfree,isfavorite;
    String building,room,time,name;

    public boolean getIsfree() {
        return isfree;
    }

    public boolean getIsfavorite() {
        return isfavorite;
    }

    public String getBuilding() {
        return building;
    }

    public String getRoom(){
        return  room;
    }

    public String getTime() {
        return time;
    }

    public String getName() {
        return name;
    }

    public BuildinginfoItem(boolean isfree, boolean isfavorite, String b, String r, String t, String n){
        this.isfree = isfree;
        this.isfavorite = isfavorite;
        this.building = b;
        this.room = r;
        this.time = t;
        this.name = n;
    }

}
